package net.nilosplace.Forex.client.models;

import java.io.Serializable;
import java.util.HashMap;

public class Portfolio implements Serializable {

	private static final long serialVersionUID = -3539073156463671126L;
	
	// Instrument, Strategy
	private HashMap<String, HashMap<Integer, Position>> positions = new HashMap<String, HashMap<Integer, Position>>();
	private double capital;

	public Portfolio(double initialCapital) {
		this.capital = initialCapital;
	}

	public Position getPosition(String instrument, int strategyId) {
		HashMap<Integer, Position> instrumentPositions = positions.get(instrument);
		if(instrumentPositions == null) {
			instrumentPositions = new HashMap<Integer, Position>();
			positions.put(instrument, instrumentPositions);
		}
		
		Position p = instrumentPositions.get(strategyId);
		if(p == null) {
			p = new Position();
			instrumentPositions.put(strategyId, p);
		}
		return p;
	}

	public HashMap<String, HashMap<Integer, Position>> getPositions() {
		return positions;
	}
	public void setPositions(HashMap<String, HashMap<Integer, Position>> positions) {
		this.positions = positions;
	}
	public double getCapital() {
		return capital;
	}
	public void setCapital(double capital) {
		this.capital = capital;
	}
	public void updateCapital(double profit) {
		this.capital += profit;
	}
}
