package net.nilosplace.Forex.client.strategies;

import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.enums.StrategyState;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.exceptions.DataUnavailableException;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.Strategy;
import net.nilosplace.Forex.client.models.StrategyConfig;

import org.apache.log4j.Logger;

public class RSISwingStrategy extends Strategy {

	protected Logger log = Logger.getLogger(getClass());
	protected RSISwingStrategyConfig config;
	
	public RSISwingStrategy(DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue, StrategyConfig config) {
		super(dataManager, eventQueue);
		this.config = (RSISwingStrategyConfig)config;
	}
	
	/*
	 * Basic Strategy
	 * The main idea behind this strategy is that we want to buy the market as it has pull back
	 * extremes. We also want our trades to be short term and exit our trades as the Relative
	 * Strength Index crosses above 40.
	 * 
	 * In this strategy, we're trying to take advantage of a move to the downside that has gone
	 * too far and too fast. Sellers have panicked pushing prices down and there is no one at
	 * least in the short term left to sell. We are also saying with our rules that the return
	 * distribution is more normal above the 200 day exponential moving average
	 * 
	 * Buy Signal 1:
	 * 		Condition 1: (Close > EMA(200))
	 * 		Condition 2: (RSI < 30)
	 * 
	 * Buy Signal 2:
	 * 		Condition 1: Close < EMA(200)
	 * 		Condition 2: Close < Open
	 * 		Condition 3: RSI < 25
	 * 		Condition 4: (Open - Close) > (.7 * (High - Low))
	 * 
	 * Exit:
	 * 		Condition: RSI > 40
	 * 
	 */

	@Override
	public void handleMarketEvent(MarketEvent event) {
		
		DataBar current = getLatestBar(config.getDataType());
		if(current == null) return;
//		double open = current.getOpenAsk();
//		double close = current.getCloseAsk();
//		double high = current.getHighAsk();
//		double low = current.getLowAsk();

		DataBar ema200, rsi;
		try {
			ema200 = ema(config.getDataType(), config.getEmaLength());
			rsi = rsi(config.getDataType(), config.getRsiLength());
		} catch (TALibDataException e) {
			return;
		} catch (DataUnavailableException e) {
			return;
		}
		
		if(state == StrategyState.OUT) {

			log.info("Tick: " + event.getTick());
//			log.info("Open: " + open + " High: " + high + " Low: " + low + " Close: " + close + " EMA(200): " + ema200 + " RSI(5): " + rsi);
//			
//			log.info("Buy condition 1: " + (close > ema200 && rsi < 30));
//			log.info("Buy condition 1: (" + close + " > " + ema200 + "): " + (close > ema200));
//			log.info("Buy condition 1: (" + rsi + " < " + 30 + "): " + (rsi < 30));
//			log.info("Buy condition 1: " + (close > ema200 && rsi < 30));
			
			// Buy condition 1
			if(current.getCloseAsk() > ema200.getCloseAsk() && rsi.getCloseAsk() < 30) {
				if(state == StrategyState.OUT) {
					log.info("Buy Current: " + current.getCloseAsk());
					//log.info("Buy condition 1: " + (close > ema200 && rsi < 30));
					//log.info("Buy condition 1: (" + close + " > " + ema200 + "): " + (close > ema200));
					//log.info("Buy condition 1: (" + rsi + " < " + 30 + "): " + (rsi < 30));
					//log.info("Buy condition 1: " + (close > ema200 && rsi < 30));
					signal(event.getTick().getTimeStamp(), SignalType.LONG, 1.0);
					return;
				}
			}

			//log.info("Buy condition 2: (" + close + " < " + ema200 + "): " + (close < ema200));
			//log.info("Buy condition 2: (" + close + " < " + open + "): " + (close < open));
			//log.info("Buy condition 2: (" + rsi + " < " + 25 + "): " + (rsi < 25));
			//log.info("Buy condition 2: (" + (open - close) + " > " + (.7 * (high - low)) + "): " + ((open - close) > (.7 * (high - low))));
			//log.info("Buy condition 2: " + ((close < ema200) && close < open && rsi < 25 && ((open - close) > (.7 * (high - low)))));
			
			// Buy Condition 2
			if(
					(current.getCloseAsk() < ema200.getCloseAsk()) &&
					current.getCloseAsk() < current.getOpenAsk() && 
					rsi.getCloseAsk() < 25 &&
					((current.getOpenAsk() - current.getCloseAsk()) > (.7 * (current.getHighAsk() - current.getLowAsk())))
			) {
				if(state == StrategyState.OUT) {
					log.info("Buy Current: " + current.getCloseAsk());
					//log.info("Buy condition 2: (" + close + " < " + ema200 + "): " + (close < ema200));
					//log.info("Buy condition 2: (" + close + " < " + open + "): " + (close < open));
					//log.info("Buy condition 2: (" + rsi + " < " + 25 + "): " + (rsi < 25));
					//log.info("Buy condition 2: (" + (open - close) + " > " + (.7 * (high - low)) + "): " + ((open - close) > (.7 * (high - low))));
					log.info("Buy condition 2: " + ((current.getCloseAsk() < ema200.getCloseAsk()) && current.getCloseAsk() < current.getOpenAsk() && rsi.getCloseAsk() < 25 && ((current.getOpenAsk() - current.getCloseAsk()) > (.7 * (current.getHighAsk() - current.getLowAsk())))));
					signal(event.getTick().getTimeStamp(), SignalType.LONG, 1.0);
					return;
				}
			}
		} else {
			if(rsi.getCloseBid() > 40) {
				log.info("Exit Current: " + current.getCloseBid());
				log.info("Exit Condition: (" + rsi.getCloseBid() + " > " + 40 + "): " + (rsi.getCloseBid() > 40));
				signal(event.getTick().getTimeStamp(), SignalType.EXIT, 1.0);
				return;
			}
		}
	}

	
}
