package net.nilosplace.Forex.client.models;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.Direction;

import org.apache.commons.lang3.time.DateUtils;

import com.oanda.fxtrade.api.CandlePoint;
import com.oanda.fxtrade.api.FXHistoryPoint;

public class DataBar implements Serializable {

	private static final long serialVersionUID = 456020886767410995L;
	//private Logger log = Logger.getLogger(getClass());
	private double openBid;
	private double openAsk;
	private double highBid;
	private double highAsk;
	private double lowBid;
	private double lowAsk;
	private double closeBid;
	private double closeAsk;
	private double volume;
	private Boolean complete;
	private String instrument;
	private Date time;
	private int barSize;
	private double pipSize;
	private DecimalFormat df = new DecimalFormat("#.00000");
	
	public DataBar() {
		this.time = new Date();
	}
	
	public DataBar(DataBar candle) {
		this.openBid = candle.getOpenBid();
		this.openAsk = candle.getOpenAsk();
		this.highBid = candle.getHighBid();
		this.highAsk = candle.getHighAsk();
		this.lowBid = candle.getLowBid();
		this.lowAsk = candle.getLowAsk();
		this.closeBid = candle.getCloseBid();
		this.closeAsk = candle.getCloseAsk();
		
		this.volume = candle.getVolume();
		this.complete = candle.getComplete();
		this.instrument = candle.getInstrument();
		this.time = candle.getTime();
		this.pipSize = candle.getPipSize();
		this.barSize = (int)((highBid - lowBid) / pipSize);
	}

	public DataBar(DataTick dataTick) {
		this.openBid = this.closeBid = this.highBid = this.lowBid = dataTick.getBid();
		this.openAsk = this.closeAsk = this.highAsk = this.lowAsk = dataTick.getAsk();
		this.instrument = dataTick.getInstrument();
		this.time = dataTick.getTimeStamp();
	}

	public DataBar(FXHistoryPoint point) {
		this.openBid = point.getOpen().getBid();
		this.openAsk = point.getOpen().getAsk();
		this.highBid = point.getMax().getBid();
		this.highAsk = point.getMax().getAsk();
		this.lowBid = point.getMin().getBid();
		this.lowAsk = point.getMin().getAsk();
		this.closeBid = point.getClose().getBid();
		this.closeAsk = point.getClose().getAsk();
		this.time = new Date(point.getTimestamp() * 1000);
	}

	public DataBar(CandlePoint point) {
		this.openBid = point.getOpen();
		this.openAsk = point.getOpen();
		this.highBid = point.getMax();
		this.highAsk = point.getMax();
		this.lowBid = point.getMin();
		this.lowAsk = point.getMin();
		this.closeBid = point.getClose();
		this.closeAsk = point.getClose();
		this.time = new Date(point.getTimestamp() * 1000);
	}

	public DataBar(double openAsk, double highAsk, double lowAsk, double closeAsk, double openBid, double highBid, double lowBid, double closeBid, double pipsize) {
		this.openAsk = openAsk;
		this.openBid = openBid;
		this.highAsk = highAsk;
		this.highBid = highBid;
		this.lowAsk = lowAsk;
		this.lowBid = lowBid;
		this.closeAsk = closeAsk;
		this.closeBid = closeBid;
		this.pipSize = pipsize;
	}

	public double getOpenBid() {
		return openBid;
	}
	public void setOpenBid(double openBid) {
		this.openBid = openBid;
	}
	public double getOpenAsk() {
		return openAsk;
	}
	public void setOpenAsk(double openAsk) {
		this.openAsk = openAsk;
	}
	public double getHighBid() {
		return highBid;
	}
	public void setHighBid(double highBid) {
		this.highBid = highBid;
	}
	public double getHighAsk() {
		return highAsk;
	}
	public void setHighAsk(double highAsk) {
		this.highAsk = highAsk;
	}
	public double getLowBid() {
		return lowBid;
	}
	public void setLowBid(double lowBid) {
		this.lowBid = lowBid;
	}
	public double getLowAsk() {
		return lowAsk;
	}
	public void setLowAsk(double lowAsk) {
		this.lowAsk = lowAsk;
	}
	public double getCloseBid() {
		return closeBid;
	}
	public void setCloseBid(double closeBid) {
		this.closeBid = closeBid;
	}
	public double getCloseAsk() {
		return closeAsk;
	}
	public void setCloseAsk(double closeAsk) {
		this.closeAsk = closeAsk;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public Boolean getComplete() {
		return complete;
	}
	public void setComplete(Boolean complete) {
		this.complete = complete;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public double getPipSize() {
		return pipSize;
	}
	public void setPipSize(double pipSize) {
		this.pipSize = pipSize;
	}
	public int getBarSize() {
		return barSize;
	}
	public void setBarSize(int barSize) {
		this.barSize = barSize;
	}
	
	public Date toBar(DataType type) {
		switch(type) {
			case TICK:
				return time;
			case MINUTE1:
				return toMin1Bar();
			case MINUTE5:
				return toMin5Bar();
			case MINUTE15:
				return toMin15Bar();
			case MINUTE30:
				return toMin30Bar();
			case HOUR1:
			case MINUTE60:
				return toHour1Bar();
			case HOUR3:
			case MINUTE180:
				return toHour3Bar();
			case HOUR4:
			case MINUTE240:
				return toHour4Bar();
			case DAY:
				return toDay1Bar();
			case WEEK:
				return toWeek1Bar();
			case MONTH:
				return toMonth1Bar();
		}
		return null;
	}
	
	
	public Date toMin1Bar() {
		return DateUtils.truncate(time, Calendar.MINUTE);
	}
	public Date toMin5Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.MINUTE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.MINUTE);
		int mod = unroundedMinutes % 5;
		calendar.add(Calendar.MINUTE, -mod);
		return calendar.getTime();
	}
	
	public Date toMin15Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.MINUTE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.MINUTE);
		int mod = unroundedMinutes % 15;
		calendar.add(Calendar.MINUTE, -mod);
		return calendar.getTime();
	}
	
	public Date toMin30Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.MINUTE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.MINUTE);
		int mod = unroundedMinutes % 30;
		calendar.add(Calendar.MINUTE, -mod);
		return calendar.getTime();
	}
	
	public Date toHour1Bar() {
		return DateUtils.truncate(time, Calendar.HOUR);
	}
	
	public Date toHour3Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.HOUR);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.HOUR);
		int mod = unroundedMinutes % 3;
		calendar.add(Calendar.HOUR, -mod);
		return calendar.getTime();
	}
	
	public Date toHour4Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.HOUR);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.HOUR);
		int mod = unroundedMinutes % 4;
		calendar.add(Calendar.HOUR, -mod);
		return calendar.getTime();
	}
	
	public Date toDay1Bar() {
		return DateUtils.truncate(time, Calendar.DAY_OF_MONTH);
	}
	
	public Date toWeek1Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.DAY_OF_MONTH);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		calendar.add(Calendar.DAY_OF_WEEK, -unroundedMinutes);
		return calendar.getTime();
	}
	
	public Date toMonth1Bar() {
		// TODO this seems wrong
		return DateUtils.truncate(time, Calendar.MONTH);
	}
	
	public void setCandle(DataType type, DataTick t) {
		this.time = t.getTimeStamp();
		this.instrument = t.getInstrument();
		this.highAsk = t.getAsk();
		this.lowAsk = t.getAsk();
		this.closeAsk = t.getAsk();
		this.openAsk = t.getAsk();
		this.highBid = t.getBid();
		this.lowBid = t.getBid();
		this.closeBid = t.getBid();
		this.openBid = t.getBid();
	}
	

	public void updateCandle(CandlePoint point) {
		if(highAsk < point.getMax()) highAsk = point.getMax();
		if(highBid < point.getMax()) highBid = point.getMax();
		
		if(lowAsk > point.getMin()) lowAsk = point.getMin();
		if(lowBid > point.getMin()) lowBid = point.getMin();
		
		closeAsk = point.getClose();
		closeBid = point.getClose();
	}
	
	public void updateCandle(FXHistoryPoint point) {
		if(highAsk < point.getMax().getAsk()) highAsk = point.getMax().getAsk();
		if(highBid < point.getMax().getBid()) highBid = point.getMax().getBid();
		
		if(lowAsk > point.getMin().getAsk()) lowAsk = point.getMin().getAsk();
		if(lowBid > point.getMin().getBid()) lowBid = point.getMin().getBid();
		
		closeAsk = point.getClose().getAsk();
		closeBid = point.getClose().getBid();
	}
	
	public String toString() {
		return "OB: " + df.format(openBid) + " OA: " + df.format(openAsk) + " HB: " + df.format(highBid) + " HA: " + df.format(highAsk) + " LB: " + df.format(lowBid) + " LA: " + df.format(lowAsk) + " CB: " + df.format(closeBid) + " CA: " + df.format(closeAsk) + " volume: " + volume + " complete: " + complete + " instrument: " + instrument + " time: " + time;
	}
	
	public Direction getDir() {
		if(openBid < closeBid || openAsk < closeAsk) return Direction.UP;
		else return Direction.DOWN;
	}
	
	public String cvsString() {
		return df.format(openBid) + "," + df.format(highBid) + "," + df.format(lowBid) + "," + df.format(closeBid);
	}

	
//	public String toString() {
//		DecimalFormat df = new DecimalFormat("#.00000");
//		return "O: " + df.format(openBid) + " H: " + df.format(highPrice) + " L: " + df.format(lowPrice) + " C: " + df.format(closePrice) + " BS: " + df.format(barSize) + " PS: " + df.format(pipSize) + " D: " + getDir();
//	}

}
