package net.nilosplace.Forex.client.models;


public class StrategyConfig {
	
	private Class strategyClass;
	private String strategyClassName;
	
	public StrategyConfig(Class strategyClass, String strategyClassName) {
		this.strategyClass = strategyClass;
		this.strategyClassName = strategyClassName;
	}

	public Class getStrategyClass() {
		return strategyClass;
	}
	public void setStrategyClass(Class strategyClass) {
		this.strategyClass = strategyClass;
	}
	public String getStrategyClassName() {
		return strategyClassName;
	}
	public void setStrategyClassName(String strategyClassName) {
		this.strategyClassName = strategyClassName;
	}
}
