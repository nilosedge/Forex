package net.nilosplace.Forex.client.exceptions;

import net.nilosplace.Forex.client.enums.DataType;

public class DataUnavailableException extends Exception {

	private static final long	serialVersionUID	= 3873695134614642984L;

	public DataUnavailableException(DataType type, int dataSize, int windowSize, String message) {
		super(type + ": " + message + " DataSize: " + dataSize + " Window Size: " + windowSize);
	}

}
