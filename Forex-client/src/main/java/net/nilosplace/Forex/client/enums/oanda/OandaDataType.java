package net.nilosplace.Forex.client.enums.oanda;

import net.nilosplace.Forex.client.enums.DataType;

import com.oanda.fxtrade.api.FXClient;

public enum OandaDataType {
	SYMBOL("Symbol", 0),
	
	MINUTE1("1", FXClient.INTERVAL_1_MIN),
	MINUTE5("5", FXClient.INTERVAL_5_MIN),
	MINUTE15("15", FXClient.INTERVAL_15_MIN),
	MINUTE30("30", FXClient.INTERVAL_30_MIN),
	MINUTE60("60", FXClient.INTERVAL_1_HOUR),
	MINUTE240("240", FXClient.INTERVAL_3_HOUR),
	DAY("D.", FXClient.INTERVAL_1_DAY),
	WEEK("W.", FXClient.INTERVAL_1_DAY),
	DAILYAVERAGE("Daily Average", 0),
	YESTERDAYHL("Yesterday", 0),
	WEEKHL("Week", 0),
	YESTERDAYHIGH("High", 0),
	YESTERDAYLOW("Low", 0),
	WEEKHIGH("High", 0),
	WEEKLOW("Low", 0),
	;

	private String columnName;
	private long dataType;


	private OandaDataType(String columnName, long dataType) {
		this.columnName = columnName;
		this.dataType = dataType;
	}

	public String getColumnName() {
		return columnName;
	}
	
	public long getDataType() {
		return dataType;
	}
	
	public String toString() {
		return columnName;
	}

	public static long lookup(DataType dataType) {

		switch(dataType) {
			case MINUTE1:
				return FXClient.INTERVAL_1_MIN;
			case MINUTE5:
				return FXClient.INTERVAL_5_MIN;
			case MINUTE15:
				return FXClient.INTERVAL_15_MIN;
			case MINUTE30:
				return FXClient.INTERVAL_30_MIN;
			case MINUTE60:
			case HOUR1:
				return FXClient.INTERVAL_1_HOUR;
			case MINUTE180:
			case HOUR3:
				return FXClient.INTERVAL_3_HOUR;
			case DAY:
				return FXClient.INTERVAL_1_DAY;
			
			// TODO fix the next ones to match oanda's
			case HOUR4:
			case MINUTE240:
				return FXClient.INTERVAL_3_HOUR;
			case MONTH:
				return FXClient.INTERVAL_1_DAY;
			case WEEK:
				return FXClient.INTERVAL_1_DAY;
			
			case TICK:
				return FXClient.INTERVAL_5_SEC;
			default:
				return 0;
		}
	}
}
