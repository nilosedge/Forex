package net.nilosplace.Forex.client.models;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.enums.StrategyState;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.events.SignalEvent;
import net.nilosplace.Forex.client.exceptions.DataUnavailableException;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.util.LimitedQueue;

public abstract class Strategy {

	protected StrategyState state = StrategyState.OUT;
	protected String instrument = "";
	private int id;
	private ConcurrentLinkedQueue<Event> eventQueue;
	protected DataManager dataManager;
	private Logger log = Logger.getLogger(getClass());
	
	public Strategy(DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue) {
		this.dataManager = dataManager;
		this.eventQueue = eventQueue;
	}
	
	public abstract void handleMarketEvent(MarketEvent event);
	
	protected void signal(Date date, SignalType type, double strength) {
		//log.info("Signal: D: " + date + " T: " + type + " S: " + strength);
		if(type == SignalType.LONG) {
			state = StrategyState.LONG;
		} else if(type == SignalType.EXIT) {
			state = StrategyState.OUT;
		} else if(type == SignalType.SHORT) {
			state = StrategyState.SHORT;
		}

		SignalEvent signal = new SignalEvent(this, instrument, type, strength);
		eventQueue.add(signal);
	}
	
	protected DataBar rsi(DataType dataType, int window) throws TALibDataException, DataUnavailableException {
		return dataManager.getRsi(dataType, instrument, window);
	}
	protected DataBar sma(DataType dataType, int window) throws TALibDataException, DataUnavailableException {
		return dataManager.getSma(dataType, instrument, window);
	}
	protected DataBar ema(DataType dataType, int window) throws TALibDataException, DataUnavailableException {
		return dataManager.getEma(dataType, instrument, window);
	}
	
	protected Direction emaup(DataType dataType, int window) throws TALibDataException, DataUnavailableException {
		return dataManager.emaDir(dataType, instrument, window);
	}
	protected LimitedQueue<DataBar> getHeikenAshiBars(DataType dataType, int amount) {
		return dataManager.getHeikenAshiBars(dataType, instrument, amount);
	}
	protected LimitedQueue<DataBar> getHeikenAshiRenkoBars(DataType dataType, int barSize, int amount) {
		return dataManager.getHeikenAshiRenkoBars(dataType, instrument, barSize, amount);
	}
	
//	protected int getPriceCount(CacheType type) {
//		return cm.getBarSize(source, type, instrument);
//	}
	
	protected DataBar getLatestBar(DataType dataType) {
		return dataManager.getCurrentPrice(dataType, instrument);
	}
	
	// TODO get rid of this
	public enum OandaOrderType {
		limit,
		stop,
		marketIfTouched,
		market,
		;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
}
