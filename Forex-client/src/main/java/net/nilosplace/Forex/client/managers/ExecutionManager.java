package net.nilosplace.Forex.client.managers;

import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.OrderEvent;
import net.nilosplace.Forex.client.models.ForexExitOrder;
import net.nilosplace.Forex.client.models.ForexLimitOrder;
import net.nilosplace.Forex.client.models.ForexMarketOrder;

public abstract class ExecutionManager extends Manager {
	
	protected abstract void handleMarketOrder(ForexMarketOrder marketOrder);
	protected abstract void handleLimitOrder(ForexLimitOrder limitOrder);
	protected abstract void handleExitOrder(ForexExitOrder exitOrder);
	
	public void handle(Event e) {
		
		OrderEvent o = (OrderEvent)e;
		
		if(o.getOrder() instanceof ForexMarketOrder) {
			handleMarketOrder((ForexMarketOrder)o.getOrder());
		} else if(o.getOrder() instanceof ForexLimitOrder) {
			handleLimitOrder((ForexLimitOrder)o.getOrder());
		} else if(o.getOrder() instanceof ForexExitOrder) {
			handleExitOrder((ForexExitOrder)o.getOrder());
		}
	}
}
