package net.nilosplace.Forex.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import net.nilosplace.Forex.client.models.Program;
import net.nilosplace.Forex.client.programs.BackTesterProgram;

import org.apache.log4j.PropertyConfigurator;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		//BasicConfigurator.configure();
		PropertyConfigurator.configure("log4j.properties");
		
		Properties programConfig = new Properties();
		if(args.length > 0) {
			programConfig.load(new FileInputStream(new File(args[0])));
		} else {
			programConfig.load(new FileInputStream(new File("config.properties")));
		}
		
		//Program p = new OandaPracticeProgram(programConfig);
		//p.start();
		
		Program p = new BackTesterProgram(programConfig);
		p.start();

	}

}
