package net.nilosplace.Forex.client.models;

public abstract class ForexOrder {

	protected int strategyId;

	public int getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}
	public ForexOrder(int strategyId) {
		this.strategyId = strategyId;
	}
	
	public abstract String toString();
}
