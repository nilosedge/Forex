package net.nilosplace.Forex.client.UI.panels;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import net.nilosplace.Forex.client.UI.components.PriceComponent;

public class FloatLimitStrategyPanel extends JPanel implements Runnable {

	private static final long serialVersionUID = -6242768430795224186L;
	private BufferedImage buffer;
	private PriceComponent price;

	public FloatLimitStrategyPanel(int w, int h, PriceComponent price) {
		buffer = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
		this.price = price;
	}

	protected void paintComponent(Graphics g) {
		Graphics2D b = buffer.createGraphics();
		b.setBackground(Color.black);
		b.fillRect(0,0,getWidth(),getHeight());
		price.redraw(b);
		b.dispose();

		//Graphics2D g = (Graphics2D)c.getGraphics();
		g.drawImage(buffer,0,0,this);
		Toolkit.getDefaultToolkit().sync();
		g.dispose();
	}

	public void run() {
		while(true) {
			try {
				repaint();
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
