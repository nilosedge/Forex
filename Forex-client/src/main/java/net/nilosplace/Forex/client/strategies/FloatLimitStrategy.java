package net.nilosplace.Forex.client.strategies;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import net.nilosplace.Forex.client.UI.components.PriceComponent;
import net.nilosplace.Forex.client.UI.frames.FloatLimitStrategyFrame;
import net.nilosplace.Forex.client.enums.StrategyState;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.DataTick;
import net.nilosplace.Forex.client.models.Strategy;
import net.nilosplace.Forex.client.models.StrategyConfig;

public class FloatLimitStrategy extends Strategy {

	protected FloatLimitStrategyConfig config;
	protected DataManager dataManager;
	private double price = 0;
	private DataBar lastBar = new DataBar();
	private double longPrice = 0;
	private double shortPrice = 0;
	private double lastLongPrice = 0;
	private double lastShortPrice = 0;
	private FloatLimitStrategyFrame frame;
	private PriceComponent priceC = new PriceComponent();
	protected Logger log = Logger.getLogger(getClass());
	
	public FloatLimitStrategy(DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue, StrategyConfig config) {
		super(dataManager, eventQueue);
		this.dataManager = dataManager;
		this.config = (FloatLimitStrategyConfig)config;
		this.frame = new FloatLimitStrategyFrame("DataType: " + this.config.dataType + " " + "Pips: " + this.config.pips, 300, 400, priceC);
	}

	@Override
	public void handleMarketEvent(MarketEvent event) {
		DataTick t = event.getTick();
		
		if(t.getAsk() < price) {
			price = t.getAsk();
		}
		if(t.getBid() > price) {
			price = t.getBid();
		}
		DataBar bar = dataManager.getCurrentPrice(config.dataType, instrument);
		
		if(state == StrategyState.OUT) {
		
			if(longPrice < t.getBid() && lastLongPrice != longPrice) {
				log.info("This should generate a long trade: " + longPrice + " T: " + event.getTick().getTimeStamp());
				lastLongPrice = longPrice;
			}
			if(shortPrice > t.getAsk() && lastShortPrice != shortPrice) {
				log.info("This should generate a short trade: " + shortPrice + " T: " + event.getTick().getTimeStamp());
				lastShortPrice = shortPrice;
			}
			//System.out.println("Bar: " + bar.getTime() + " TB: " + bar.toBar(config.dataType));
			//System.out.println("LBar: " + lastBar.getTime() + " TB: " + lastBar.toBar(config.dataType));
			if(bar.toBar(config.dataType).equals(lastBar.toBar(config.dataType))) {
				//log.info("Time Equal");
			} else {
				lastBar = bar;
				//log.info("Bar: " + lastBar.toBar(config.dataType));
				//log.info("Update Order");
				longPrice = bar.getCloseBid() + (config.pips * 0.01);
				shortPrice = bar.getCloseAsk() - (config.pips * 0.01);
			}
		}
		
		priceC.updateNumbers(longPrice, shortPrice, bar, event.getTick(), config.pips, 0.01);
		
		//log.info("Price: " + price + " L: " + longPrice + " S: " + shortPrice);

		//signal(t.getTimeStamp(), SignalType.LONG, 1);
	}

}
