package net.nilosplace.Forex.client.models;

public class ForexMarketOrder extends ForexOrder {

	private String instrument;
	private int units = 0;
	private double price = 0;

	public ForexMarketOrder(int strategyId, String instrument, int units, double price) {
		super(strategyId);
		this.instrument = instrument;
		this.units = units;
		this.price = price;
	}
	
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public int getUnits() {
		return units;
	}
	public void setUnits(int units) {
		this.units = units;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Id: " + strategyId + " I: " + instrument + " U: " + units + " P: " + price;
	}
}
