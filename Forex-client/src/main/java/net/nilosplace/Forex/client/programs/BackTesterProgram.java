package net.nilosplace.Forex.client.programs;

import java.util.Properties;

import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.FillEvent;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.events.OrderEvent;
import net.nilosplace.Forex.client.events.SignalEvent;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.managers.ExecutionManager;
import net.nilosplace.Forex.client.managers.PortfolioManager;
import net.nilosplace.Forex.client.managers.StrategyManager;
import net.nilosplace.Forex.client.managers.data.CVSDataManager;
import net.nilosplace.Forex.client.managers.execution.CVSExecutionManager;
import net.nilosplace.Forex.client.managers.strategies.CVSStrategyManager;
import net.nilosplace.Forex.client.models.Program;

import org.apache.log4j.Logger;

public class BackTesterProgram extends Program {

	private Logger log = Logger.getLogger(getClass());
	DataManager dataManager;
	StrategyManager strategyManager;
	PortfolioManager portfolioManager;
	ExecutionManager executionManager;
	
	public BackTesterProgram(Properties programConfig) {
		super(programConfig);
		
		dataManager = new CVSDataManager(programConfig, eventQueue);
		strategyManager = new CVSStrategyManager(programConfig, dataManager, eventQueue);
		portfolioManager = new PortfolioManager(programConfig, dataManager, eventQueue);
		executionManager = new CVSExecutionManager(programConfig, dataManager, eventQueue);
	}
	
	public void run() {
		
		for(String instrument: instrumentList) {
			log.info("Run Back Test for Instrument: " + instrument);
			int counter = 0;

			while(((CVSDataManager)dataManager).getNextBar(instrument)) {
	
				while(!eventQueue.isEmpty()) {
					
					Event e = eventQueue.poll();
					
					if(e instanceof MarketEvent) {
						strategyManager.handle(e);
					} else if(e instanceof SignalEvent) {
						portfolioManager.handle(e);
					} else if(e instanceof OrderEvent) {
						executionManager.handle(e);
					} else if(e instanceof FillEvent) {
						portfolioManager.handle(e);
					}
					
				}
				try {
					Thread.sleep(150);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				counter++;
			}
			log.info("Read: " + counter + " ticks form: " + instrument);
		}
		
	}

}
