package net.nilosplace.Forex.client.managers.strategies;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.managers.StrategyManager;
import net.nilosplace.Forex.client.models.StrategyConfig;
import net.nilosplace.Forex.client.strategies.FloatLimitStrategyConfig;
import net.nilosplace.Forex.client.strategies.MovingAverageCrossStrategyConfig;
import net.nilosplace.Forex.client.strategies.RSIHighLowStrategyConfig;
import net.nilosplace.Forex.client.strategies.RSISwingStrategyConfig;

public class OandaStrategyManager extends StrategyManager {

	ArrayList<StrategyConfig> configs = new ArrayList<StrategyConfig>();
	
	public OandaStrategyManager(Properties programConfig, DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue) {
		super(programConfig, dataManager, eventQueue);

//		@Override
//		protected void rateEvent(MarketEvent event) {
//			runStrategy(DataType.MINUTE1, short_window, long_window, event.getTick());
//		}
		
		//configs.add(new MovingAverageCrossStrategyConfig(DataType.MINUTE1, 100, 400));
		//configs.add(new MovingAverageCrossStrategyConfig(DataType.MINUTE5, 100, 400));
		//configs.add(new MovingAverageCrossStrategyConfig(DataType.MINUTE15, 50, 200));
		
		//configs.add(new RSIHighLowStrategyConfig());
		
		//configs.add(new RSISwingStrategyConfig(DataType.MINUTE1, 100, 400));
		//configs.add(new RSISwingStrategyConfig(DataType.MINUTE5, 100, 400));
		//configs.add(new RSISwingStrategyConfig(DataType.MINUTE15, 50, 200));
		
		configs.add(new FloatLimitStrategyConfig(DataType.MINUTE1, 5));
		configs.add(new FloatLimitStrategyConfig(DataType.MINUTE5, 10));
		
		//classes.add(OandaRSIHighLowStrategy.class);
		
		initStrategies(configs);
	}

}
