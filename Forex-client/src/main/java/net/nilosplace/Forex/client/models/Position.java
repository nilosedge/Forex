package net.nilosplace.Forex.client.models;

import java.io.Serializable;


public class Position implements Serializable {

	private static final long serialVersionUID = -4718560600209442569L;
	private int units = 0;
	private double price = 0;

	public int getUnits() {
		return units;
	}
//	public void setUnits(int units) {
//		this.units = units;
//	}
	public double getPrice() {
		return price;
	}
//	public void setPrice(double price) {
//		this.price = price;
//	}
	public void updateFill(int inunits, double inprice) {
		if((units + inunits) != 0) {
			price = ((units * price) + (inunits * inprice)) / (units + inunits);
		} else {
			price = 0;
		}
		units += inunits;
	}
	
	public String toString() {
		return "U: " + units + " P: " + price;
	}
}
