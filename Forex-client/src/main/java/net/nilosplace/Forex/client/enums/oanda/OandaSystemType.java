package net.nilosplace.Forex.client.enums.oanda;


public enum OandaSystemType {
	
	PRODUCTION(
		"oanda_production_username",
		"oanda_production_password",
		"oanda_production_account_id"
	),
	PRACTICE(
		"oanda_practice_username",
		"oanda_practice_password",
		"oanda_practice_account_id"
	),
	SANDBOX(
		"oanda_sandbox_username",
		"oanda_sandbox_password",
		"oanda_sandbox_account_id"
	),
	;
	
	private String accountId;
	private String username;
	private String password;


	private OandaSystemType(String username, String password, String accountId) {
		this.username = username;
		this.password = password;
		this.accountId = accountId;
	}

	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getAccountId() {
		return accountId;
	}
}
