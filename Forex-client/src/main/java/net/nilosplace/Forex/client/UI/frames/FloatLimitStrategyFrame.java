package net.nilosplace.Forex.client.UI.frames;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import net.nilosplace.Forex.client.UI.components.PriceComponent;
import net.nilosplace.Forex.client.UI.panels.FloatLimitStrategyPanel;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.DataTick;

public class FloatLimitStrategyFrame extends JFrame {

	private static final long serialVersionUID = 9055720283655656062L;
	private FloatLimitStrategyPanel panel;
	
	public FloatLimitStrategyFrame(String name, int w, int h, PriceComponent price) {
		super(name);
		setSize(w, h);
		panel = new FloatLimitStrategyPanel(w, h, price);
		add(panel);
		Thread t = new Thread(panel);
		t.start();
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	public static void main(String args[]) {
		PriceComponent price = new PriceComponent();
		DataTick tick = new DataTick();
		tick.setAsk(93.710);
		tick.setBid(93.694);
		price.updateNumbers(93.694 + 0.05, 93.710 - 0.05, new DataBar(), tick, 5, 0.01);
		new FloatLimitStrategyFrame("Test", 300, 400, price);
	}
}
