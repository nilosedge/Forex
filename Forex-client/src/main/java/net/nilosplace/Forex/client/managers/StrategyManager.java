package net.nilosplace.Forex.client.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.models.Strategy;
import net.nilosplace.Forex.client.models.StrategyConfig;

public class StrategyManager extends Manager {

	private DataManager dataManager;
	private ConcurrentLinkedQueue<Event> eventQueue;
	private List<String> instrumentList;
	// Instrument, StrategyName, Config, List<Strategy>
	private HashMap<String, HashMap<String, HashMap<String, ArrayList<Strategy>>>> strategies = new HashMap<String, HashMap<String, HashMap<String, ArrayList<Strategy>>>>();

	public StrategyManager(Properties programConfig, DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue) {
		this.dataManager = dataManager;
		this.eventQueue = eventQueue;
		String instruments = (String)programConfig.get("instruments");
		this.instrumentList = Arrays.asList(instruments.split(",")); 
	}

	protected void initStrategies(ArrayList<StrategyConfig> configs) {
		int idCounter = 0;
		for(String instrument: instrumentList) {
			for(StrategyConfig sc: configs) {
				
				ArrayList<Strategy> strategyArray = getStrategyArray(instrument, sc.getStrategyClassName(), sc.getClass().getName());

				try {
					Strategy s = (Strategy)sc.getStrategyClass().getDeclaredConstructor(DataManager.class, ConcurrentLinkedQueue.class, StrategyConfig.class).newInstance(dataManager, eventQueue, sc);
					s.setInstrument(instrument);
					s.setId(idCounter++);
					strategyArray.add(s);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private ArrayList<Strategy> getStrategyArray(String instrument, String strategy, String strategyConfig) {
		//System.out.println("Key1: " + instrument + " Key2: " + strategy + " Key3: " + strategyConfig);
		HashMap<String, ArrayList<Strategy>> strategyHash = getStrategyHash(instrument, strategy);
		ArrayList<Strategy> strategyArray = strategyHash.get(strategyConfig);
		if(strategyArray == null) {
			strategyArray = new ArrayList<Strategy>();
			strategyHash.put(strategyConfig, strategyArray);
		}
		return strategyArray;
	}
	
	private HashMap<String, ArrayList<Strategy>> getStrategyHash(String instrument, String strategy) {
		//System.out.println("Key1: " + instrument + " Key2: " + strategy);
		HashMap<String, HashMap<String, ArrayList<Strategy>>> instumentHash = getInstrumentHash(instrument);
		HashMap<String, ArrayList<Strategy>> strategyHash = instumentHash.get(strategy);
		if(strategyHash == null) {
			strategyHash = new HashMap<String, ArrayList<Strategy>>();
			instumentHash.put(strategy, strategyHash);
		}
		return strategyHash;
	}

	private HashMap<String, HashMap<String, ArrayList<Strategy>>> getInstrumentHash(String instrument) {
		//System.out.println("Key1: " + instrument);
		HashMap<String, HashMap<String, ArrayList<Strategy>>> instumentHash = strategies.get(instrument);
		if(instumentHash == null) {
			instumentHash = new HashMap<String, HashMap<String, ArrayList<Strategy>>>();
			strategies.put(instrument, instumentHash);
		}
		return instumentHash;
	}
	
	public void handle(Event event) {
		MarketEvent m = (MarketEvent)event;
		for(String instrument: strategies.keySet()) {
			if(instrument.equals(m.getTick().getInstrument())) {
				for(String strategyName: strategies.get(instrument).keySet()) {
					for(String config: strategies.get(instrument).get(strategyName).keySet()) {
						for(Strategy strategy: strategies.get(instrument).get(strategyName).get(config)) {
							strategy.handleMarketEvent(m);
						}
					}
				}
			}
		}
	}
	
}
