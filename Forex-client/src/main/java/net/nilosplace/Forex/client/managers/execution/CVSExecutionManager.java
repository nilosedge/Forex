package net.nilosplace.Forex.client.managers.execution;

import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.FillEvent;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.managers.ExecutionManager;
import net.nilosplace.Forex.client.models.ForexExitOrder;
import net.nilosplace.Forex.client.models.ForexFilledOrder;
import net.nilosplace.Forex.client.models.ForexLimitOrder;
import net.nilosplace.Forex.client.models.ForexMarketOrder;

import org.apache.log4j.Logger;

public class CVSExecutionManager extends ExecutionManager {

	private Logger log = Logger.getLogger(getClass());
	
	private ConcurrentLinkedQueue<Event> eventQueue;
	private DataManager dataManager;
	
	public CVSExecutionManager(Properties programConfig, DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue) {
		this.dataManager = dataManager;
		this.eventQueue = eventQueue;
	}

	@Override
	protected void handleMarketOrder(ForexMarketOrder marketOrder) {
		FillEvent fe = new FillEvent();
		ForexFilledOrder fill = new ForexFilledOrder(marketOrder.getStrategyId());
		if(marketOrder.getUnits() > 0) {
			fill.setFilledPrice(dataManager.getCurrentPrice(DataType.TICK, marketOrder.getInstrument()).getCloseAsk());
		} else {
			fill.setFilledPrice(dataManager.getCurrentPrice(DataType.TICK, marketOrder.getInstrument()).getCloseBid());
		}
		
		fill.setForexOrder(marketOrder);
		fe.setFilledOrder(fill);
		//log.info("Filled Order: " + fill);
		eventQueue.add(fe);
	}

	@Override
	protected void handleLimitOrder(ForexLimitOrder limitOrder) {
		log.warn("Limit Order not Implemented: ");
	}

	@Override
	protected void handleExitOrder(ForexExitOrder exitOrder) {
		FillEvent fe = new FillEvent();
		ForexFilledOrder fill = new ForexFilledOrder(exitOrder.getStrategyId());
		if(exitOrder.getUnits() > 0) {
			fill.setFilledPrice(dataManager.getCurrentPrice(DataType.TICK, exitOrder.getInstrument()).getCloseAsk());
		} else {
			fill.setFilledPrice(dataManager.getCurrentPrice(DataType.TICK, exitOrder.getInstrument()).getCloseBid());
		}
		fill.setForexOrder(exitOrder);
		fe.setFilledOrder(fill);
		//log.info("Filled Order: " + fill);
		eventQueue.add(fe);
	}

}
