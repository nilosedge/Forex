package net.nilosplace.Forex.client.strategies;

import net.nilosplace.Forex.client.models.StrategyConfig;

public class RSIHighLowStrategyConfig extends StrategyConfig {

	public RSIHighLowStrategyConfig() {
		super(RSIHighLowStrategy.class, "RSIHighLowStrategy");
	}

}
