package net.nilosplace.Forex.client.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import net.nilosplace.Forex.client.models.Portfolio;

import org.apache.log4j.Logger;

public class FileObjectUtil {

	private static Logger log = Logger.getLogger(FileObjectUtil.class);

	public static boolean savePortfolio(Portfolio portfolio, File file) {
		try {
			//log.info("Saving Portfolio to: " + file.getPath());
			FileOutputStream fos = new FileOutputStream(file);
			GZIPOutputStream gzos = new GZIPOutputStream(fos);
			ObjectOutputStream out = new ObjectOutputStream(gzos);
			out.writeObject(portfolio);
			out.flush();
			out.close();
			return true;
		}
		catch (IOException e) {
			log.error(e);
			return false;
		}
	}

	public static Portfolio loadPortfolio(File file) {
		Portfolio portfolio = new Portfolio(100000);
		try {
			log.info("Loading Portfolio from: " + file.getPath());
			FileInputStream fis = new FileInputStream(file);
			GZIPInputStream gzis = new GZIPInputStream(fis);
			ObjectInputStream in = new ObjectInputStream(gzis);
			portfolio = (Portfolio)in.readObject();
			in.close();
			return portfolio;
		}
		catch (Exception e) {
			log.error(e);
			return portfolio;
		}
	}
}
