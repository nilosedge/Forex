package net.nilosplace.Forex.client.csv;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import net.nilosplace.Forex.client.models.DataTick;

import org.apache.commons.io.filefilter.WildcardFileFilter;

public class CSVLoader extends Thread {
	
	HashMap<String, List<String>> sortedCurrencies = new HashMap<String, List<String>>();
	HashMap<String, CVSCurrencyHandler> currencyMap = new LinkedHashMap<String, CVSCurrencyHandler>();
	TreeMap<Long, List<DataTick>> tickMap = new TreeMap<Long, List<DataTick>>();

	public CSVLoader(String path, Date startTime) {
		File directory = new File(path);

		FileFilter ff1 = new WildcardFileFilter("20*");
		FileFilter ff2 = new WildcardFileFilter("DAT_ASCII_*_T_*.zip");

		File[] years = directory.listFiles(ff1);
		for(File year: years) {

			File[] curs = year.listFiles();
			for(File currency: curs) {

				String key = currency.getName();
				
				if(!sortedCurrencies.containsKey(key)) {
					sortedCurrencies.put(key, new ArrayList<String>());
				}
				List<String> fileMap = sortedCurrencies.get(key);

				File[] files = currency.listFiles(ff2);
				for(File file: files) {
					fileMap.add(file.getAbsolutePath());
				}
			}
		}
		
		for(String cur: sortedCurrencies.keySet()) {
			CVSCurrencyHandler cch = new CVSCurrencyHandler(cur, sortedCurrencies.get(cur));
			cch.start();
			currencyMap.put(cur, cch);
			addNext(cur);
		}
		
	}

	public DataTick getNextTick() {
		DataTick ret;
		if(tickMap.firstEntry().getValue().size() > 1) {
			ret = tickMap.firstEntry().getValue().remove(0);
		} else {
			ret = tickMap.remove(tickMap.firstEntry().getKey()).remove(0);
		}
		addNext(ret.getInstrument());
		return ret;

	}
	
	public void run() {
		while(true) {
			for(String key: currencyMap.keySet()) {
				addNext(key);
			}
			if(tickMap.size() % 50000 == 0) {
				System.out.println("Size: " + tickMap.size());
			}
		}
	}

	private void addNext(String instrument) {
		DataTick next = currencyMap.get(instrument).getNextTick();
		if(next != null) {
			if(tickMap.containsKey(next.getTimeStamp().getTime())) {
				tickMap.get(next.getTimeStamp().getTime()).add(next);
			} else {
				List<DataTick> list = new ArrayList<DataTick>();
				list.add(next);
				tickMap.put(next.getTimeStamp().getTime(), list);
			}
		}
	}
}
