package net.nilosplace.Forex.client.csv;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

public class TestMain {

	public static void main(String[] args) throws FileNotFoundException {
		CSVLoader csv = new CSVLoader("/Volumes/Internal Drive 2", new Date());
		csv.start();
		
		PrintWriter pw = null;
		
		int count = 0;
		int filecount = 0;
		
		while(count > -1) {
			if(count == 0) {
				if(pw != null) {
					pw.flush();
					pw.close();
				}
				pw = new PrintWriter("/Volumes/RAM Disk/out" + filecount);
				filecount++;
			}
			pw.println(csv.getNextTick().toString());
			//System.out.println("Next Tick: " + csv.getNextTick());
			count++;
			count %= 10000000;
		}
		
		pw.flush();
		pw.close();
	}

}
