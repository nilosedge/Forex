package net.nilosplace.Forex.client.UI.frames;

import java.awt.Color;
import java.awt.GridLayout;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import net.nilosplace.Forex.client.Oanda.OandaRateEventHandlerInterface;
import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.oanda.OandaDataType;
import net.nilosplace.Forex.client.managers.DataManager;

import org.apache.log4j.Logger;

import com.oanda.fxtrade.api.FXHistoryPoint;
import com.oanda.fxtrade.api.FXPair;
import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.RateTable;

public class OandaIndicatorFrame extends JFrame implements OandaRateEventHandlerInterface {

	private static final long serialVersionUID = 3016673353464293290L;
	private Logger log = Logger.getLogger(getClass());
	private JPanel panel = new JPanel();
	private RateTable rateTable;
	private DecimalFormat df5 = new DecimalFormat("#.00000");
//	private DecimalFormat df4 = new DecimalFormat("#.0000");
//	private DecimalFormat df3 = new DecimalFormat("#.000");
//	private DecimalFormat df2 = new DecimalFormat("#.00");
//	private DecimalFormat df1 = new DecimalFormat("#.0");
	private List<String> instrumentList;
	private HashMap<String, HashMap<String, JLabel>> labelMap = new HashMap<String, HashMap<String, JLabel>>();
	private Properties programConfig;
	private DataManager dataManager;

	public OandaIndicatorFrame(Properties programConfig, DataManager dataManager, RateTable rateTable) {
		super("Oanda Indicators");
		this.programConfig = programConfig;
		this.dataManager = dataManager;
		this.rateTable = rateTable;
		String instruments = (String)programConfig.get("instruments");
		this.instrumentList = Arrays.asList(instruments.split(",")); 
		init();
	}

	private void init() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		for(String inst: instrumentList) {
			Instrument i = rateTable.getInstrument(inst);
			updateAllLabels(i);
		}

		panel.setLayout(new GridLayout(instrumentList.size(), 13));

		add(panel);
		setSize(1024, 600);
		setVisible(true);
	}


	
	private void updateAllLabels(Instrument i) {
		JLabel l = getLabel(i, "Symbol");
		l.setText(i.getPair());
		
		updateLabel(i, DataType.MINUTE1);
		updateLabel(i, DataType.MINUTE5);
		updateLabel(i, DataType.MINUTE15);
		updateLabel(i, DataType.MINUTE30);
		updateLabel(i, DataType.MINUTE60);
		updateLabel(i, DataType.MINUTE240);
		updateLabel(i, DataType.DAY);
		updateLabel(i, DataType.WEEK);
		
		l = getLabel(i, "Daily Average");
		l.setText("ATR Pips: " + dataManager.getAtrPips(DataType.DAY, i.getPair(), 20));
		
		FXHistoryPoint yest = getYesterday(i);
		
		double high = 0;
		double low = 0;
		if(yest != null) {
			high = yest.getMax().getBid();
			low = yest.getMin().getBid();
		}
		
		l = getLabel(i, "YESTERDAYHIGH");
		l.setText("" + df5.format(high));
		
		l = getLabel(i, "YESTERDAYLOW");
		l.setText("" + df5.format(low));
		
		FXHistoryPoint week = getWeek(i);
		
		if(week != null) {
			high = week.getMax().getBid();
			low = week.getMin().getBid();
		}
		
		l = getLabel(i, "WEEKHIGH");
		l.setText("" + df5.format(high));
		
		l = getLabel(i, "WEEKLOW");
		l.setText("" + df5.format(low));
		
	}
	
	private FXHistoryPoint getWeek(Instrument i) {
		// TODO Fix with weekly numbers
		return getYesterday(i);
	}

	private void updateLabel(Instrument i, DataType tc) {
		JLabel l = getLabel(i, tc.toString());
		
		String emaS = df5.format(dataManager.getHeikenAshiEma(tc, i.getPair(), 20));
		double ema = Double.parseDouble(emaS);
		double current = 0;
		if(l.getText() != null && !l.getText().equals("")) {
			log.info("Text: " + l.getText());
			current = Double.parseDouble(l.getText());
		}
		
		log.info("Cur: " + current + " EMA: " + ema);
		
		if(current < ema) {
			l.setForeground(Color.GREEN);
		} else if(current == ema) {
			l.setForeground(Color.WHITE);
		} else {
			l.setForeground(Color.RED);
		}
		l.setText(df5.format(ema));
	}
	
	private JLabel getLabel(Instrument i, String label) {
		HashMap<String, JLabel> rowLabelMap = getRowMap(i);
		if(!rowLabelMap.containsKey(label)) {
			JLabel l = new JLabel("");
			panel.add(l);
			rowLabelMap.put(label, l);
			return l;
		} else {
			return rowLabelMap.get(label);
		}
	}
	
	private HashMap<String, JLabel> getRowMap(Instrument i) {
		if(!labelMap.containsKey(i.getPair())) {
			HashMap<String, JLabel> rowLabelMap = new HashMap<String, JLabel>();
			labelMap.put(i.getPair(), rowLabelMap);
			return rowLabelMap;
		} else {
			return labelMap.get(i.getPair());
		}
	}
	
	private FXHistoryPoint getYesterday(Instrument i) {
		Vector<FXHistoryPoint> history;
		try {
			history = rateTable.getHistory(i, OandaDataType.DAY.getDataType(), 2);
			return history.get(1);
		} catch (OAException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void handle(FXRateEventInfo info) {

		FXPair p = info.getPair();

		Instrument i = rateTable.getInstrument(p.getPair());
		//log.info("Inst: " + i.getPair() + " " + i.getPIP() + " " + i.getPipettes());


		updateAllLabels(i);
		//		try {
		//			FXTick t = rt.getRate(p);
		//			
		//			map.put(t.getPair().toString(), t);
		//
		//			if(!labels.containsKey(t.getPair().toString())) {
		//				JLabel l = new JLabel();
		//				panel.add(l);
		//				labels.put(t.getPair().toString(), l);
		//			}
		//			labels.get(t.getPair().toString()).setText(t.getPair() + ": " + df.format(t.getBid()) + "-" + df.format(t.getAsk()));
		//
		//		} catch (RateTableException e) {
		//			e.printStackTrace();
		//		}

	}


}
