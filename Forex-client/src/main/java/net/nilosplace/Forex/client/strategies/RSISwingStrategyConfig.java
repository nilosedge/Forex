package net.nilosplace.Forex.client.strategies;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.models.StrategyConfig;

public class RSISwingStrategyConfig extends StrategyConfig {

	private DataType dataType;
	private int rsiLength;
	private int emaLength;

	public RSISwingStrategyConfig(DataType dataType, int rsiLength, int emaLength) {
		super(RSISwingStrategy.class, "RSISwingStrategy");
		this.dataType = dataType;
		this.setRsiLength(rsiLength);
		this.setEmaLength(emaLength);
	}
	
	public DataType getDataType() {
		return dataType;
	}
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	public int getRsiLength() {
		return rsiLength;
	}
	public void setRsiLength(int rsiLength) {
		this.rsiLength = rsiLength;
	}
	public int getEmaLength() {
		return emaLength;
	}
	public void setEmaLength(int emaLength) {
		this.emaLength = emaLength;
	}
}
