package net.nilosplace.Forex.client.strategies;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.enums.StrategyState;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.Strategy;
import net.nilosplace.Forex.client.models.StrategyConfig;
import net.nilosplace.Forex.client.util.CoreHelper;
import net.nilosplace.Forex.client.util.LimitedQueue;

public class FourXDATStrategy extends Strategy {

	public FourXDATStrategy(DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue, StrategyConfig config) {
		super(dataManager, eventQueue);
		try {
			out = new PrintWriter(new File("/Users/olinblodgett/Desktop/file.output"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private PrintWriter out;
	//private HashMap<String, PurchaseInfo> purchaseMap = new HashMap<String, PurchaseInfo>();


	public void handleMarketEvent(MarketEvent event) {
		// TODO Auto-generated method stub
		
		//double pipSize = OandaCalculations.getDailtyAtrPips(t, i, 20);

		LimitedQueue<DataBar> masterBars = getHeikenAshiBars(DataType.DAY, 4);
		Direction masterDirection = masterBars.get(masterBars.size() - 2).getDir();

		LimitedQueue<DataBar> emaBars = getHeikenAshiBars(DataType.MINUTE60, 60);
		Direction lastHeikenAshiBarDirection = emaBars.get(emaBars.size() - 2).getDir();

		double ema20_1 = 0;
		double ema20_2 = 0;
		double macd[] = new double[3];
		try {

			double[] prices = new double[emaBars.size()];
			for(int ii = 0; ii < emaBars.size(); ii++) {
				prices[ii] = emaBars.get(ii).getCloseBid();
			}
			macd = CoreHelper.macd(prices, 12, 26, 9);

			prices = new double[emaBars.size()];
			for(int ii = 0; ii < emaBars.size(); ii++) {
				prices[ii] = emaBars.get(ii).getCloseBid();
			}

			ema20_1 = CoreHelper.ema(prices, 20);


			emaBars.removeLast();
			prices = new double[emaBars.size()];
			for(int ii = 0; ii < emaBars.size(); ii++) {
				prices[ii] = emaBars.get(ii).getCloseBid();
			}
			ema20_2 = CoreHelper.ema(prices, 20);


		} catch (TALibDataException e) {
			e.printStackTrace();
		}


		print("Step 1: ");
		print("MasterDirection (Daily): " + masterDirection);

		print("Step 2: ");
		print("Last Heiken Ashi Bar Direction (Hourly): " + lastHeikenAshiBarDirection);
		
		boolean priceCheck = false;
		if(masterDirection == Direction.UP && (event.getTick().getBid() > ema20_1)) {
			priceCheck = true;
		}
		if(masterDirection == Direction.DOWN && (event.getTick().getBid() < ema20_1)) {
			priceCheck = true;
		}
		print("Price Check: " + priceCheck);
		
		Direction ema20SlopeDirection;
		if(ema20_1 > ema20_2) {
			ema20SlopeDirection = Direction.UP;
		} else {
			ema20SlopeDirection = Direction.DOWN;
		}

		print("Ema Slope: (" + ema20_1 + " > " + ema20_2 + "): " + ema20SlopeDirection);

		print("Step 3: ");
		LimitedQueue<DataBar> bars = getHeikenAshiRenkoBars(DataType.MINUTE1, 10, 1440);
		print("Last Renko Bar Direction (10 Pip): " + bars.peekLast().getDir());

		Direction macdDirection;
		if(macd[0] > macd[1]) {
			macdDirection = Direction.UP;
		} else {
			macdDirection = Direction.DOWN;
		}
		
		print("Confirm with MACD: (" + macd[0] + " > " + macd[1] + "): " + macdDirection);
		
		print("Step 4: ");
		print("Market Condition: " + masterDirection + " " + lastHeikenAshiBarDirection + " " + ema20SlopeDirection + " " + priceCheck + " " + bars.peekLast().getDir() + " " + macdDirection);

		if(
				(masterDirection == lastHeikenAshiBarDirection) &&
				(masterDirection == ema20SlopeDirection) &&
				(priceCheck) &&
				(masterDirection == bars.peekLast().getDir()) &&
				(masterDirection == macdDirection)
		) {
			print("State: " + state);
			if(state == StrategyState.OUT) {
				if(masterDirection == Direction.UP) {
					signal(event.getTick().getTimeStamp(), SignalType.LONG, 1);
				} else if(masterDirection == Direction.DOWN) {
					signal(event.getTick().getTimeStamp(), SignalType.SHORT, 1);
				}
				print("Getting into the market with: " + masterDirection);
			}

		} else {
			print("Staying out until the right conditions");
		}

		print("-------------------------------------------------------------");
		out.flush();
	}

	private void print(String in) {
		out.println(instrument + ": " + in);
	}

}
