package net.nilosplace.Forex.client.models;

public class ForexLimitOrder extends ForexOrder {

	public ForexLimitOrder(int strategyId) {
		super(strategyId);
	}

	@Override
	public String toString() {
		return "Id: " + strategyId;
	}
}
