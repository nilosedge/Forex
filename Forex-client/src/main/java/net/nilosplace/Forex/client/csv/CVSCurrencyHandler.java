package net.nilosplace.Forex.client.csv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import net.nilosplace.Forex.client.models.DataTick;

public class CVSCurrencyHandler extends Thread {
	
	ZipFile openZipFile = null;
	BufferedReader br = null;
	List<String> fileList = null;
	String instrument;
	ArrayList<DataTick> tickBuffer = new ArrayList<DataTick>();

	public CVSCurrencyHandler(String instrument, List<String> fileList) {
		this.fileList = fileList;
		this.instrument = instrument;
		openCSVFile(fileList.remove(0));
	}

	private void openCSVFile(String filePath) {
		if(openZipFile != null) {
			try {
				openZipFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			openZipFile = new ZipFile(filePath);
			Enumeration<? extends ZipEntry> entries = openZipFile.entries();
			while(entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				if(entry.getName().endsWith(".csv")) {
					InputStream csvInputStream = openZipFile.getInputStream(entry);
					br = new BufferedReader(new InputStreamReader(csvInputStream));
				}
			}
				
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public DataTick getNextTick() {
		while(tickBuffer.isEmpty()) {
			try {
				System.out.println("Tick Buffer is Empty: " + instrument);
				yield();
				sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tickBuffer.remove(0);
	}
	
	public void run() {
		while(true) {
			if(tickBuffer.size() < 500) {
				try {
					String line = br.readLine();
					if(line == null) {
						if(fileList.size() > 0) {
							openCSVFile(fileList.remove(0));
							tickBuffer.add(new DataTick(instrument, br.readLine()));
						} else {
							openZipFile.close();
							return;
						}
					} else {
						tickBuffer.add(new DataTick(instrument, line));
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				//System.out.println(instrument + ": " + "Buffer Hit Max: " + tickBuffer.size());
				try {
					yield();
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

}