package net.nilosplace.Forex.client.strategies;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.models.StrategyConfig;

public class MovingAverageCrossStrategyConfig extends StrategyConfig {

	private DataType dataType;
	private int short_window;
	private int long_window;

	public MovingAverageCrossStrategyConfig(DataType dataType, int short_window, int long_window) {
		super(MovingAverageCrossStrategy.class, "MovingAverageCrossStrategy");
		this.dataType = dataType;
		this.long_window = long_window;
		this.short_window = short_window;
	}
	
	public DataType getDataType() {
		return dataType;
	}
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	public int getShort_window() {
		return short_window;
	}
	public void setShort_window(int short_window) {
		this.short_window = short_window;
	}
	public int getLong_window() {
		return long_window;
	}
	public void setLong_window(int long_window) {
		this.long_window = long_window;
	}
}
