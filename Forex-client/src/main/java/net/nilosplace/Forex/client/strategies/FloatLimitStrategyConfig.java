package net.nilosplace.Forex.client.strategies;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.models.StrategyConfig;

public class FloatLimitStrategyConfig extends StrategyConfig {

	public DataType dataType;
	public double pips;
	
	public FloatLimitStrategyConfig(DataType dataType, int pips) {
		super(FloatLimitStrategy.class, "FloatLimitStrategy");
		this.dataType = dataType;
		this.pips = pips;
	}

}
