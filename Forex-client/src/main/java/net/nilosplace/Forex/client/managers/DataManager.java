package net.nilosplace.Forex.client.managers;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.exceptions.DataUnavailableException;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.Portfolio;
import net.nilosplace.Forex.client.util.LimitedQueue;

public abstract class DataManager {

	public abstract DataBar getRsi(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException;
	public abstract DataBar getSma(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException;
	public abstract DataBar getEma(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException;
	
	public abstract Direction emaDir(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException;
	public abstract DataBar getCurrentPrice(DataType dataType, String instrument);
	
	public abstract double getAtrPips(DataType dataType, String instrument, int amount);
	public abstract double getHeikenAshiEma(DataType dataType, String instrument, int amount);
	
	public abstract LimitedQueue<DataBar> getHeikenAshiBars(DataType dataType, String instrument, int amount);
	public abstract LimitedQueue<DataBar> getHeikenAshiRenkoBars(DataType dataType, String instrument, int barsize, int amount);
	public abstract Portfolio loadPortFolio();
	public abstract void savePortFolio(Portfolio portfolio);
	
}
