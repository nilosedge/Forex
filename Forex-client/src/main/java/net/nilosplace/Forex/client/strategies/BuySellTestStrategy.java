package net.nilosplace.Forex.client.strategies;

import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.enums.StrategyState;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.Strategy;
import net.nilosplace.Forex.client.models.StrategyConfig;

import org.apache.log4j.Logger;

public class BuySellTestStrategy extends Strategy {

	public BuySellTestStrategy(DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue, StrategyConfig config) {
		super(dataManager, eventQueue);
	}

	private Logger log = Logger.getLogger(getClass());
	
	int count = 0;
	int orderdelay = 20;
	
	@Override
	public void handleMarketEvent(MarketEvent event) {
		log.info("Counter: " + count);
		if((count % orderdelay) == 0) {
			if(state == StrategyState.SHORT) {
				signal(event.getTick().getTimeStamp(), SignalType.EXIT, 1.0);
			} else if(state == StrategyState.OUT) {
				signal(event.getTick().getTimeStamp(), SignalType.SHORT, 1.0);
			}
		}
		count++;
	}

}
