package net.nilosplace.Forex.client.enums;

public enum DataType {
	TICK,
	MINUTE1,
	MINUTE5,
	MINUTE15,
	MINUTE30,
	MINUTE60,
	HOUR1,
	MINUTE180,
	HOUR3,
	MINUTE240,
	HOUR4,
	DAY,
	WEEK,
	MONTH,
	;

}
