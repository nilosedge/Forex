package net.nilosplace.Forex.client.strategies;

import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.enums.StrategyState;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.exceptions.DataUnavailableException;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.Strategy;
import net.nilosplace.Forex.client.models.StrategyConfig;

import org.apache.log4j.Logger;

public class RSIHighLowStrategy extends Strategy {

	private Logger log = Logger.getLogger(getClass());
	
	private int last1, last5, last15, last30;
	
	public RSIHighLowStrategy(DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue, StrategyConfig config) {
		super(dataManager, eventQueue);
	}
	
	/*
	 * Basic strategy
	 * Buy Signal: When all 4 rsi's drop below a certain level (30) and the SMA 200 is in an uptrend
	 * Exit Signal: Once the RSI gets back to 50
	 * 
	 * Sell Signal: When all 4 rsi's raise above a certain level (70) and the SMA 200 is in an downtrend
	 * Exit Signal: Once the RSI gets back to 50
	 */

	@Override
	public void handleMarketEvent(MarketEvent event) {

		DataBar rsi_min1, rsi_min5, rsi_min15, rsi_min30;
		Direction emaDirection;
		try {
			int window = 25;
			rsi_min1 = rsi(DataType.MINUTE1, window);
			rsi_min5 = rsi(DataType.MINUTE5, window);
			rsi_min15 = rsi(DataType.MINUTE15, window);
			rsi_min30 = rsi(DataType.MINUTE30, window);
			emaDirection = emaup(DataType.MINUTE30, window);
		} catch (TALibDataException e) {
			return;
		} catch (DataUnavailableException e) {
			return;
		}
		
		//log.trace("Candle: " + tick);
		if(
				(int)rsi_min1.getCloseBid() > (last1 + 1) ||
				(int)rsi_min1.getCloseBid() < (last1 - 1) ||
				(int)rsi_min5.getCloseBid() > (last5 + 1) ||
				(int)rsi_min5.getCloseBid() < (last5 - 1) ||
				(int)rsi_min15.getCloseBid() > (last15 + 1) ||
				(int)rsi_min15.getCloseBid() < (last15 - 1) ||
				(int)rsi_min30.getCloseBid() > (last30 + 1) ||
				(int)rsi_min30.getCloseBid() < (last30 - 1)
			) {
			last1 = (int)rsi_min1.getCloseBid();
			last5 = (int)rsi_min5.getCloseBid();
			last15 = (int)rsi_min15.getCloseBid();
			last30 = (int)rsi_min30.getCloseBid();
			//log.info("I: " + instrument + " rsi_min1: " + rsi_min1 + " rsi_min5: " + rsi_min5 + " rsi_min15: " + rsi_min15 + " rsi_min30: " + rsi_min30 + " EMADir: " + emaDirection);
			log.info(rsi_min1 + "," + rsi_min5 + "," + rsi_min15 + "," + rsi_min30);
		}
		
		if(rsi_min1.getCloseAsk() < 30 && rsi_min5.getCloseAsk() < 30 && rsi_min15.getCloseAsk() < 30 && rsi_min30.getCloseAsk() < 30 && emaDirection == Direction.UP) {
			if(state == StrategyState.OUT) {
				signal(event.getTick().getTimeStamp(), SignalType.LONG, 1.0);
				return;
			}
		}
		if(rsi_min30.getCloseBid() > 50 && state == StrategyState.LONG) {
			signal(event.getTick().getTimeStamp(), SignalType.EXIT, 1.0);
			return;
		}
		
		if(rsi_min1.getCloseAsk() > 70 && rsi_min5.getCloseAsk() > 70 && rsi_min15.getCloseAsk() > 70 && rsi_min30.getCloseAsk() > 70 && emaDirection == Direction.DOWN) {
			if(state == StrategyState.OUT) {
				signal(event.getTick().getTimeStamp(), SignalType.SHORT, 1.0);
				return;
			}
		}
		if(rsi_min30.getCloseBid() < 50 && state == StrategyState.SHORT) {
			signal(event.getTick().getTimeStamp(), SignalType.EXIT, 1.0);
			return;
		}

		//double avg = (rsi_min1 + rsi_min5 + rsi_min15 + rsi_min30) / 4;
	}

}
