package net.nilosplace.Forex.client.programs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import net.nilosplace.Forex.client.Oanda.OandaRateEvent;
import net.nilosplace.Forex.client.UI.frames.OandaAccountFrame;
import net.nilosplace.Forex.client.UI.frames.OandaOrdersFrame;
import net.nilosplace.Forex.client.enums.oanda.OandaSystemType;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.FillEvent;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.events.OrderEvent;
import net.nilosplace.Forex.client.events.SignalEvent;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.managers.ExecutionManager;
import net.nilosplace.Forex.client.managers.PortfolioManager;
import net.nilosplace.Forex.client.managers.StrategyManager;
import net.nilosplace.Forex.client.managers.data.OandaDataManager;
import net.nilosplace.Forex.client.managers.execution.OandaExecutionManager;
import net.nilosplace.Forex.client.managers.strategies.OandaStrategyManager;
import net.nilosplace.Forex.client.models.Program;

import org.apache.log4j.Logger;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.FXClient;
import com.oanda.fxtrade.api.RateTable;
import com.oanda.fxtrade.api.User;

public class OandaPracticeProgram extends Program {
	
	private Logger log = Logger.getLogger(getClass());
	private DataManager dataManager;
	private StrategyManager strategyManager;
	private PortfolioManager portfolioManager;
	private ExecutionManager executionManager;
	
	public OandaPracticeProgram(Properties programConfig) {
		super(programConfig);
		
		try {
			programConfig.load(new FileInputStream(new File(programConfig.getProperty("oandaPropertiesFile"))));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		FXClient fxclient;
		fxclient = API.createFXGame();
		fxclient.setWithKeepAliveThread(true);
		fxclient.setWithRateThread(true);

		OandaSystemType type = OandaSystemType.valueOf(programConfig.getProperty("oanda_system_type"));
		
		try {

			fxclient.login(programConfig.getProperty(type.getUsername()), programConfig.getProperty(type.getPassword()), "Example2 Test");

			RateTable rateTable = fxclient.getRateTable();
			
			User me = fxclient.getUser();
			Vector<?> accounts = me.getAccounts();
			Account account = (Account)accounts.firstElement();
			
			dataManager = new OandaDataManager(programConfig, eventQueue, rateTable, account);
			portfolioManager = new PortfolioManager(programConfig, dataManager, eventQueue);
			executionManager = new OandaExecutionManager(programConfig, dataManager, eventQueue, rateTable, account);
			strategyManager = new OandaStrategyManager(programConfig, dataManager, eventQueue);
			
			//OandaIndicatorFrame oif = new OandaIndicatorFrame(programConfig, dataManager, rateTable);
			//OandaOrdersFrame orderFrame = new OandaOrdersFrame(account);
			//OandaAccountFrame accountFrame = new OandaAccountFrame(account);
			
			//Oanda4XDATStrategy strat = new Oanda4XDATStrategy(t, instruments);
			
			OandaRateEvent rateEvent = new OandaRateEvent(programConfig);
			
			//OandaAccountEvent accountEvent = new OandaAccountEvent();
			//event.registerHandler(oif);
			//event.registerHandler(strat);
			rateEvent.registerHandler((OandaDataManager)dataManager);
			//rateEvent.registerHandler(orderFrame);
			//rateEvent.registerHandler(accountFrame);
			//accountEvent.registerHandler(oom);

			rateTable.getEventManager().add(rateEvent);
			//t.getEventManager().add(accountEvent);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
		while(true) {
			if(!eventQueue.isEmpty()) {
				
				Event e = eventQueue.poll();
				
				if(e instanceof MarketEvent) {
					strategyManager.handle(e);
				} else if(e instanceof SignalEvent) {
					portfolioManager.handle(e);
				} else if(e instanceof OrderEvent) {
					executionManager.handle(e);
				} else if(e instanceof FillEvent) {
					portfolioManager.handle(e);
				}
			} else {
				try {
					sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
