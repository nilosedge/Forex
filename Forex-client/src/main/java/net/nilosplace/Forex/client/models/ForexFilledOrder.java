package net.nilosplace.Forex.client.models;

public class ForexFilledOrder extends ForexOrder {

	private ForexOrder forexOrder;
	private double filledPrice;

	public ForexFilledOrder(int strategyId) {
		super(strategyId);
	}

	public ForexOrder getForexOrder() {
		return forexOrder;
	}
	public void setForexOrder(ForexOrder forexOrder) {
		this.forexOrder = forexOrder;
	}
	public double getFilledPrice() {
		return filledPrice;
	}
	public void setFilledPrice(double filledPrice) {
		this.filledPrice = filledPrice;
	}
	
	@Override
	public String toString() {
		return "FP: " + filledPrice + " O: " + forexOrder;
	}
}
