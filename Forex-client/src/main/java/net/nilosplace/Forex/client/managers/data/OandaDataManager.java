package net.nilosplace.Forex.client.managers.data;

import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.AccountException;
import com.oanda.fxtrade.api.FXHistoryPoint;
import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.RateTable;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

import net.nilosplace.Forex.client.Oanda.OandaRateEventHandlerInterface;
import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.enums.oanda.OandaDataType;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.exceptions.DataUnavailableException;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.DataTick;
import net.nilosplace.Forex.client.models.Portfolio;
import net.nilosplace.Forex.client.util.CoreHelper;
import net.nilosplace.Forex.client.util.LimitedQueue;

public class OandaDataManager extends DataManager implements OandaRateEventHandlerInterface {

	protected Properties programConfig;
	protected RateTable rateTable;
	protected Account account;
	protected ConcurrentLinkedQueue<Event> eventQueue;
	private Logger log = Logger.getLogger(getClass());
	
	
	
	public OandaDataManager(Properties programConfig, ConcurrentLinkedQueue<Event> eventQueue, RateTable rateTable, Account account) {
		this.programConfig = programConfig;
		this.eventQueue = eventQueue;
		this.rateTable = rateTable;
		this.account = account;
	}
	
	@Override
	public DataBar getRsi(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		Instrument i = rateTable.get(instrument);

		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), window + 1);
			
//			double[] pricesHighAsk = new double[history.size()];
//			double[] pricesLowAsk = new double[history.size()];
//			double[] pricesOpenAsk = new double[history.size()];
			double[] pricesCloseAsk = new double[history.size()];
			
//			double[] pricesHighBid = new double[history.size()];
//			double[] pricesLowBid = new double[history.size()];
//			double[] pricesOpenBid = new double[history.size()];
			double[] pricesCloseBid = new double[history.size()];
			
			int ii = 0;
			for(FXHistoryPoint hp: history) {
//				pricesHighAsk[ii] = hp.getMax().getAsk();
//				pricesLowAsk[ii] = hp.getMin().getAsk();
//				pricesOpenAsk[ii] = hp.getOpen().getAsk();
				pricesCloseAsk[ii] = hp.getClose().getAsk();
//				pricesHighBid[ii] = hp.getMax().getBid();
//				pricesLowBid[ii] = hp.getMin().getBid();
//				pricesOpenBid[ii] = hp.getOpen().getBid();
				pricesCloseBid[ii] = hp.getClose().getBid();
				ii++;
			}
			DataBar b = new DataBar();
			
//			b.setOpenAsk(CoreHelper.rsi(pricesOpenAsk, window));
//			b.setOpenBid(CoreHelper.rsi(pricesOpenBid, window));
//			b.setHighAsk(CoreHelper.rsi(pricesHighAsk, window));
//			b.setHighBid(CoreHelper.rsi(pricesHighBid, window));
//			b.setLowAsk(CoreHelper.rsi(pricesLowAsk, window));
//			b.setLowBid(CoreHelper.rsi(pricesLowBid, window));
			b.setCloseAsk(CoreHelper.rsi(pricesCloseAsk, window));
			b.setCloseBid(CoreHelper.rsi(pricesCloseBid, window));
			
			return b;
		} catch (OAException e) {
			e.printStackTrace();
		}
		throw new DataUnavailableException(dataType, window, window, "Not enough data");
	}

	@Override
	public DataBar getSma(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		Instrument i = rateTable.get(instrument);

		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), window + 1);

//			double[] pricesHighAsk = new double[history.size()];
//			double[] pricesLowAsk = new double[history.size()];
//			double[] pricesOpenAsk = new double[history.size()];
			double[] pricesCloseAsk = new double[history.size()];
			
//			double[] pricesHighBid = new double[history.size()];
//			double[] pricesLowBid = new double[history.size()];
//			double[] pricesOpenBid = new double[history.size()];
			double[] pricesCloseBid = new double[history.size()];
			
			int ii = 0;
			for(FXHistoryPoint hp: history) {
//				pricesHighAsk[ii] = hp.getMax().getAsk();
//				pricesLowAsk[ii] = hp.getMin().getAsk();
//				pricesOpenAsk[ii] = hp.getOpen().getAsk();
				pricesCloseAsk[ii] = hp.getClose().getAsk();
//				pricesHighBid[ii] = hp.getMax().getBid();
//				pricesLowBid[ii] = hp.getMin().getBid();
//				pricesOpenBid[ii] = hp.getOpen().getBid();
				pricesCloseBid[ii] = hp.getClose().getBid();
				ii++;
			}
			DataBar b = new DataBar();
			
//			b.setOpenAsk(CoreHelper.rsi(pricesOpenAsk, window));
//			b.setOpenBid(CoreHelper.rsi(pricesOpenBid, window));
//			b.setHighAsk(CoreHelper.rsi(pricesHighAsk, window));
//			b.setHighBid(CoreHelper.rsi(pricesHighBid, window));
//			b.setLowAsk(CoreHelper.rsi(pricesLowAsk, window));
//			b.setLowBid(CoreHelper.rsi(pricesLowBid, window));
			b.setCloseAsk(CoreHelper.sma(pricesCloseAsk, window));
			b.setCloseBid(CoreHelper.sma(pricesCloseBid, window));
			
			return b;
		} catch (OAException e) {
			e.printStackTrace();
		}
		throw new DataUnavailableException(dataType, window, window, "Not enough data");
	}

	@Override
	public DataBar getEma(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		Instrument i = rateTable.get(instrument);

		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), window + 1);

//			double[] pricesHighAsk = new double[history.size()];
//			double[] pricesLowAsk = new double[history.size()];
//			double[] pricesOpenAsk = new double[history.size()];
			double[] pricesCloseAsk = new double[history.size()];
			
//			double[] pricesHighBid = new double[history.size()];
//			double[] pricesLowBid = new double[history.size()];
//			double[] pricesOpenBid = new double[history.size()];
			double[] pricesCloseBid = new double[history.size()];
			
			int ii = 0;
			for(FXHistoryPoint hp: history) {
//				pricesHighAsk[ii] = hp.getMax().getAsk();
//				pricesLowAsk[ii] = hp.getMin().getAsk();
//				pricesOpenAsk[ii] = hp.getOpen().getAsk();
				pricesCloseAsk[ii] = hp.getClose().getAsk();
//				pricesHighBid[ii] = hp.getMax().getBid();
//				pricesLowBid[ii] = hp.getMin().getBid();
//				pricesOpenBid[ii] = hp.getOpen().getBid();
				pricesCloseBid[ii] = hp.getClose().getBid();
				ii++;
			}
			DataBar b = new DataBar();
			
//			b.setOpenAsk(CoreHelper.rsi(pricesOpenAsk, window));
//			b.setOpenBid(CoreHelper.rsi(pricesOpenBid, window));
//			b.setHighAsk(CoreHelper.rsi(pricesHighAsk, window));
//			b.setHighBid(CoreHelper.rsi(pricesHighBid, window));
//			b.setLowAsk(CoreHelper.rsi(pricesLowAsk, window));
//			b.setLowBid(CoreHelper.rsi(pricesLowBid, window));
			b.setCloseAsk(CoreHelper.ema(pricesCloseAsk, window));
			b.setCloseBid(CoreHelper.ema(pricesCloseBid, window));
			
			return b;
		} catch (OAException e) {
			e.printStackTrace();
		}
		throw new DataUnavailableException(dataType, window, window, "Not enough data");
	}

	@Override
	public Direction emaDir(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		Instrument i = rateTable.get(instrument);
		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), window + 1);
			
			double[] prices = new double[history.size()];
			for(int ii = 0; ii < history.size(); ii++) {
				prices[ii] = history.get(ii).getClose().getBid();
			}
			
			double[] out = new double[prices.length];
			MInteger begin = new MInteger();
			MInteger length = new MInteger();
			Core c = new Core();
			RetCode retCode = c.ema(0, prices.length - 1, prices, window, begin, length, out);
			if (retCode == RetCode.Success && out.length > 2) {
				if(out[length.value - 1] > out[length.value - 2]) {
					return Direction.UP;
				} else {
					return Direction.DOWN;
				}
			}

		} catch (OAException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public DataBar getCurrentPrice(DataType dataType, String instrument) {
		Instrument i = rateTable.get(instrument);
		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), 1);
			return new DataBar(history.get(0));
		} catch (OAException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public double getAtrPips(DataType dataType, String instrument, int amount) {
		Instrument i = rateTable.get(instrument);
		double atr = 0;
		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), amount + 1);
			double[] highs = new double[history.size()];
			double[] lows = new double[history.size()];
			double[] closes = new double[history.size()];
			
			int ii = 0;
			for(FXHistoryPoint hp: history) {
				highs[ii] = hp.getMax().getBid();
				lows[ii] = hp.getMin().getBid();
				closes[ii] = hp.getClose().getBid();
				ii++;
			}
			atr = Math.floor(CoreHelper.atr(highs, lows, closes, amount) / i.getPIP());
		} catch (OAException e) {
			e.printStackTrace();
		} catch (TALibDataException e) {
			e.printStackTrace();
		}
		return atr;
	}

	@Override
	public double getHeikenAshiEma(DataType dataType, String instrument, int amount) {
		Instrument i = rateTable.get(instrument);
		double haema = 0;
		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), amount + 1);
			LimitedQueue<DataBar> queue = generateHeikenAshiBars(history, i);
			double[] prices = new double[queue.size()];
			int ii = 0;
			for(DataBar ob: queue) {
				prices[ii++] = ob.getCloseBid();
			}
			haema = CoreHelper.ema(prices, amount);
		} catch (OAException e) {
			e.printStackTrace();
		} catch (TALibDataException e) {
			e.printStackTrace();
		}
		
		return haema;
	}

	@Override
	public LimitedQueue<DataBar> getHeikenAshiBars(DataType dataType, String instrument, int amount) {
		Instrument i = rateTable.get(instrument);
		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), amount+1);
			LimitedQueue<DataBar> queue = generateHeikenAshiBars(history, i);
			return queue;
		} catch (OAException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public LimitedQueue<DataBar> getHeikenAshiRenkoBars(DataType dataType, String instrument, int barsize, int amount) {
		Instrument i = rateTable.get(instrument);
		try {
			Vector<FXHistoryPoint> history = rateTable.getHistory(i, OandaDataType.lookup(dataType), amount+1);
			LimitedQueue<DataBar> queue = generateHeikenAshiBars(history, i);
			queue = generateRenkoBars(queue, i, barsize);
			return queue;
		} catch (OAException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static LimitedQueue<DataBar> generateHeikenAshiBars(Vector<FXHistoryPoint> history, Instrument i) {
		
		LimitedQueue<DataBar> queue = new LimitedQueue<DataBar>(history.size());

		FXHistoryPoint first = history.remove(0);
		//DataBar firstbar = new DataBar(first.getOpen().getBid(), first.getMax().getBid(), first.getMin().getBid(), first.getClose().getBid(), i.getPIP());
		//queue.add(firstbar);

		for(FXHistoryPoint p: history) {
			double newOpen = ((queue.peekLast().getOpenBid() + queue.peekLast().getCloseBid()) / 2);
			double newClose = (p.getOpen().getBid() + p.getMax().getBid() + p.getMin().getBid() + p.getClose().getBid())/4;	
			//DataBar curBar = new DataBar(newOpen, Math.max(Math.max(p.getMax().getBid(), newOpen), newClose), Math.min(Math.min(p.getMin().getBid(), newOpen), newClose), newClose, i.getPIP());
			//queue.add(curBar);
		}
		return queue;
	}
	
	private LimitedQueue<DataBar> generateRenkoBars(LimitedQueue<DataBar> inBars, Instrument i, int barsize) {
		LimitedQueue<DataBar> queue = new LimitedQueue<DataBar>(inBars.size());
		for(DataBar bar: inBars.getElements()) {
			if(queue.size() == 0) {
				double highAsk = (bar.getCloseAsk() + ((barsize / 2) * i.getPIP()));
				double lowAsk = (bar.getCloseAsk() - ((barsize / 2) * i.getPIP()));
				double highBid = (bar.getCloseBid() + ((barsize / 2) * i.getPIP()));
				double lowBid = (bar.getCloseBid() - ((barsize / 2) * i.getPIP()));
				
				//queue.add(new DataBar(lowAsk, highAsk, lowAsk, highAsk, i.getPIP()));
				//queue.add(new DataBar(lowBid, highBid, lowBid, highBid, i.getPIP()));
			} else {
				
				if(bar.getLowAsk() > queue.peekLast().getHighAsk()) {
					while(bar.getHighAsk() > queue.peekLast().getHighAsk()) {
						double newHigh = queue.peekLast().getHighAsk() + (i.getPIP() * barsize);
						double newLow = queue.peekLast().getHighAsk();
						//queue.add(new DataBar(newLow, newHigh, newLow, newHigh, i.getPIP()));
					}
				}
				
				if(bar.getLowBid() > queue.peekLast().getHighBid()) {
					while(bar.getHighBid() > queue.peekLast().getHighBid()) {
						double newHigh = queue.peekLast().getHighBid() + (i.getPIP() * barsize);
						double newLow = queue.peekLast().getHighBid();
						//queue.add(new DataBar(newLow, newHigh, newLow, newHigh, i.getPIP()));
					}
				}
				
				if(bar.getHighAsk() < queue.peekLast().getLowAsk()) {
					while(bar.getLowAsk() < queue.peekLast().getLowAsk()) {
						double newHigh = queue.peekLast().getLowAsk();
						double newLow = queue.peekLast().getLowAsk() - (i.getPIP() * barsize);
						//queue.add(new DataBar(newHigh, newHigh, newLow, newLow, i.getPIP()));
					}
				}
				
				if(bar.getHighBid() < queue.peekLast().getLowBid()) {
					while(bar.getLowBid() < queue.peekLast().getLowBid()) {
						double newHigh = queue.peekLast().getLowBid();
						double newLow = queue.peekLast().getLowBid() - (i.getPIP() * barsize);
						//queue.add(new DataBar(newHigh, newHigh, newLow, newLow, i.getPIP()));
					}
				}
			}
		}
		return queue;
	}

	@Override
	public Portfolio loadPortFolio() {
		// TODO Need to Load Portfolio from OANDA!!!!
		log.error("Need to Load Portfolio from OANDA!!!!");
		return new Portfolio(getAccountCapital());
	}
	
	public double getAccountCapital() {
		try {
			return account.getBalance();
		} catch (AccountException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void savePortFolio(Portfolio portfolio) {
		// TODO Need to Save Portfolio from OANDA!!!!
		log.error("Need to Save Portfolio from OANDA!!!!");
	}

	@Override
	public void handle(FXRateEventInfo info) {
		DataTick dataTick = new DataTick(info.getPair().getPair(), info.getTick().getBid(), info.getTick().getAsk(), info.getTick().getTimestamp());
		DataBar dataBar = new DataBar(dataTick);
		eventQueue.add(new MarketEvent(dataTick));
	}
	
//	
//	private LimitedQueue<DataBar> generateRenkoBars(RateTable t, Instrument i, OandaDataType column, int barsize, int amount) {
//		try {
//			Vector<FXHistoryPoint> history = t.getHistory(i, column.getDataType(), amount * barsize);
//			LimitedQueue<DataBar> queue = generateRenkoBars(history, i, barsize);
//			return queue;
//
//		} catch (OAException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

//	private LimitedQueue<DataBar> generateRenkoBars(Vector<FXHistoryPoint> history, Instrument i, int barsize) {
//		LimitedQueue<DataBar> queue = new LimitedQueue<DataBar>(history.size());
//		
//		for(FXHistoryPoint h: history) {
//			if(queue.size() == 0) {
//				double high = (h.getClose().getBid() + ((barsize / 2) * i.getPIP()));
//				double low = (h.getClose().getBid() - ((barsize / 2) * i.getPIP()));
//				
//				queue.push(new DataBar(high, high, low, low, i.getPIP()));
//			} else {
//				
//				double lastHigh = queue.peekLast().getHighBid();
//				double lastLow = queue.peekLast().getLowBid();
//				
//				if(h.getMin().getBid() > lastHigh) {
//					while(h.getMax().getBid() > lastHigh) {
//						double newHigh = lastHigh + (i.getPIP() * barsize);
//						double newLow = lastHigh;
//						queue.add(new DataBar(newHigh, newHigh, newLow, newLow, i.getPIP()));
//					}
//				}
//				
//				if(h.getMax().getBid() < lastLow) {
//					while(h.getMin().getBid() < lastLow) {
//						double newHigh = lastLow;
//						double newLow = lastLow - (i.getPIP() * barsize);
//						queue.add(new DataBar(newHigh, newHigh, newLow, newLow, i.getPIP()));
//					}
//				}
//			}
//		}
//		return queue;
//	}


}
