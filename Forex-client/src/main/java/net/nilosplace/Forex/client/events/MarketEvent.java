package net.nilosplace.Forex.client.events;

import net.nilosplace.Forex.client.models.DataTick;

public class MarketEvent extends Event {
	
	DataTick tick;

	public MarketEvent(DataTick tick) {
		this.tick = tick;
	}

	public DataTick getTick() {
		return tick;
	}
	public void setTick(DataTick tick) {
		this.tick = tick;
	}
}
