package net.nilosplace.Forex.client.events;

import net.nilosplace.Forex.client.models.ForexOrder;

public class OrderEvent extends Event {

	private ForexOrder order;

	public OrderEvent(ForexOrder order) {
		this.order = order;
	}

	public ForexOrder getOrder() {
		return order;
	}
	public void setOrder(ForexOrder order) {
		this.order = order;
	}
}
