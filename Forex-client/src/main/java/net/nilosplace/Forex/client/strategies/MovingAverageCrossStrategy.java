package net.nilosplace.Forex.client.strategies;

import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.enums.StrategyState;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.exceptions.DataUnavailableException;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.DataTick;
import net.nilosplace.Forex.client.models.Strategy;
import net.nilosplace.Forex.client.models.StrategyConfig;

import org.apache.log4j.Logger;

public class MovingAverageCrossStrategy extends Strategy {

	protected Logger log = Logger.getLogger(getClass());
	protected MovingAverageCrossStrategyConfig config;
	
	public MovingAverageCrossStrategy(DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue, StrategyConfig config) {
		super(dataManager, eventQueue);
		this.config = (MovingAverageCrossStrategyConfig) config;
	}
	
	protected void runStrategy(DataTick tick) {

		DataBar short_sma, long_sma;
		try {
			short_sma = sma(config.getDataType(), config.getShort_window());
			long_sma = sma(config.getDataType(), config.getLong_window());
		} catch (TALibDataException e) {
			return;
		} catch (DataUnavailableException e) {
			return;
		}

		log.info("Instrument: " + instrument + " Short SMA: " + short_sma + " Long SMA: " + long_sma);

		double strength = 1.0;

		if(short_sma.getCloseAsk() > long_sma.getCloseAsk() && state == StrategyState.OUT) {
			log.info("short_sma > long_sma Buying In");
			signal(tick.getTimeStamp(), SignalType.LONG, strength);
		} else if(short_sma.getCloseBid() < long_sma.getCloseBid() && state == StrategyState.LONG) {
			log.info("short_sma < long_sma Getting Out");
			signal(tick.getTimeStamp(), SignalType.EXIT, strength);
		}
	}

	@Override
	public void handleMarketEvent(MarketEvent event) {
		// TODO Auto-generated method stub
		
	}


}
