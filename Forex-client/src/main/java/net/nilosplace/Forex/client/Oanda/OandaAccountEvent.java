package net.nilosplace.Forex.client.Oanda;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.oanda.fxtrade.api.FXAccountEvent;
import com.oanda.fxtrade.api.FXAccountEventInfo;
import com.oanda.fxtrade.api.FXEventInfo;
import com.oanda.fxtrade.api.FXEventManager;

public class OandaAccountEvent extends FXAccountEvent {

	protected ArrayList<OandaAccountEventHandlerInterface> handlers = new ArrayList<OandaAccountEventHandlerInterface>();
	
	private Logger log = Logger.getLogger(getClass());
	
	public void registerHandler(OandaAccountEventHandlerInterface handler) {
		handlers.add(handler);
	}

	@Override
	public void handle(FXEventInfo event, FXEventManager manager) {
		log.info("Account Event Info: " + event);
		FXAccountEventInfo ae = (FXAccountEventInfo)event;
		for(OandaAccountEventHandlerInterface h: handlers) {
			h.handle(ae);
		}
	}
}