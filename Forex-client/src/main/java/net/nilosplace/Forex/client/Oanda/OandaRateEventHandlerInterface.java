package net.nilosplace.Forex.client.Oanda;

import com.oanda.fxtrade.api.FXRateEventInfo;

public interface OandaRateEventHandlerInterface {

	public void handle(FXRateEventInfo info);
}
