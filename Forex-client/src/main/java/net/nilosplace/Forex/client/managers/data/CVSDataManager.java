package net.nilosplace.Forex.client.managers.data;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.MarketEvent;
import net.nilosplace.Forex.client.exceptions.DataUnavailableException;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.DataTick;
import net.nilosplace.Forex.client.models.Portfolio;
import net.nilosplace.Forex.client.util.CoreHelper;
import net.nilosplace.Forex.client.util.FileObjectUtil;
import net.nilosplace.Forex.client.util.LimitedQueue;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.log4j.Logger;

public class CVSDataManager extends DataManager {

	private Logger log = Logger.getLogger(getClass());
	
	private HashMap<String, DataInputStream> fileMap = new HashMap<String, DataInputStream>();
	private ConcurrentLinkedQueue<Event> eventQueue;
	private List<String> instrumentList;
	private String dataPath;
	private int fileReadCounter = 0;
	private Date start = new Date();
	private Date end = new Date();
	private String portfolioFileName = "CVSPortfolio.portfolio";
	
	private HashMap<String, HashMap<String, LimitedQueue<DataBar>>> priceCache = new HashMap<String, HashMap<String, LimitedQueue<DataBar>>>();
	private int cacheSize = 1000;
	
	public CVSDataManager(Properties programConfig, ConcurrentLinkedQueue<Event> eventQueue) {
		this.eventQueue = eventQueue;
		this.dataPath = (String)programConfig.get("cvsFilePath");
		String instruments = (String)programConfig.get("instruments");
		this.instrumentList = Arrays.asList(instruments.split(",")); 
		initFiles();
	}

	private void initFiles() {
		for(String instrument: instrumentList) {
			File f = new File(dataPath + "/" + instrument.replace("/", "") + ".csv.bz2");
			if(!f.exists()) {
				log.error("File can not be found for: " + instrument + " - " + f.getPath());
			} else {
				try {
					FileInputStream fis = new FileInputStream(f);
					BZip2CompressorInputStream bzcis = new BZip2CompressorInputStream(fis);
					DataInputStream di = new DataInputStream(bzcis);
					fileMap.put(instrument, di);
					log.info("Registered file: " + f.getPath());
				} catch (FileNotFoundException e) {
					log.error("Error opening: " + f.getPath() + " Error: " + e.getMessage());
				} catch (IOException e) {
					log.error("Error opening: " + f.getPath() + " Error: " + e.getMessage());
				}
			}
		}
	}

	@Override
	public DataBar getRsi(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		LimitedQueue<DataBar> queue = getPriceDataCache(instrument, dataType.name());
		List<DataBar> candles = queue.getElements();
		if(candles.size() > window) {
//			double[] pricesHighAsk = new double[candles.size()];
//			double[] pricesLowAsk = new double[candles.size()];
//			double[] pricesOpenAsk = new double[candles.size()];
			double[] pricesCloseAsk = new double[candles.size()];
			
//			double[] pricesHighBid = new double[candles.size()];
//			double[] pricesLowBid = new double[candles.size()];
//			double[] pricesOpenBid = new double[candles.size()];
			double[] pricesCloseBid = new double[candles.size()];
			
			for(int i = 0; i < candles.size(); i++) {
//				pricesHighAsk[i] = candles.get(i).getHighAsk();
//				pricesLowAsk[i] = candles.get(i).getLowAsk();
//				pricesOpenAsk[i] = candles.get(i).getOpenAsk();
				pricesCloseAsk[i] = candles.get(i).getCloseAsk();
//				pricesHighBid[i] = candles.get(i).getHighBid();
//				pricesLowBid[i] = candles.get(i).getLowBid();
//				pricesOpenBid[i] = candles.get(i).getOpenBid();
				pricesCloseBid[i] = candles.get(i).getCloseBid();
			}
			DataBar b = new DataBar();
			
//			b.setOpenAsk(CoreHelper.rsi(pricesOpenAsk, window));
//			b.setOpenBid(CoreHelper.rsi(pricesOpenBid, window));
//			b.setHighAsk(CoreHelper.rsi(pricesHighAsk, window));
//			b.setHighBid(CoreHelper.rsi(pricesHighBid, window));
//			b.setLowAsk(CoreHelper.rsi(pricesLowAsk, window));
//			b.setLowBid(CoreHelper.rsi(pricesLowBid, window));
			b.setCloseAsk(CoreHelper.rsi(pricesCloseAsk, window));
			b.setCloseBid(CoreHelper.rsi(pricesCloseBid, window));
			return b;
		} else {
			throw new DataUnavailableException(dataType, candles.size(), window, "Not enough data");
		}
	}

	@Override
	public DataBar getSma(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		LimitedQueue<DataBar> queue = getPriceDataCache(instrument, dataType.name());
		List<DataBar> candles = queue.getElements();
		if(candles.size() >= window) {
//			double[] pricesHighAsk = new double[candles.size()];
//			double[] pricesLowAsk = new double[candles.size()];
//			double[] pricesOpenAsk = new double[candles.size()];
			double[] pricesCloseAsk = new double[candles.size()];
			
//			double[] pricesHighBid = new double[candles.size()];
//			double[] pricesLowBid = new double[candles.size()];
//			double[] pricesOpenBid = new double[candles.size()];
			double[] pricesCloseBid = new double[candles.size()];
			for(int i = 0; i < candles.size(); i++) {
//				pricesHighAsk[i] = candles.get(i).getHighAsk();
//				pricesLowAsk[i] = candles.get(i).getLowAsk();
//				pricesOpenAsk[i] = candles.get(i).getOpenAsk();
				pricesCloseAsk[i] = candles.get(i).getCloseAsk();
//				pricesHighBid[i] = candles.get(i).getHighBid();
//				pricesLowBid[i] = candles.get(i).getLowBid();
//				pricesOpenBid[i] = candles.get(i).getOpenBid();
				pricesCloseBid[i] = candles.get(i).getCloseBid();
			}
			DataBar b = new DataBar();
			
//			b.setOpenAsk(CoreHelper.sma(pricesOpenAsk, window));
//			b.setOpenBid(CoreHelper.sma(pricesOpenBid, window));
//			b.setHighAsk(CoreHelper.sma(pricesHighAsk, window));
//			b.setHighBid(CoreHelper.sma(pricesHighBid, window));
//			b.setLowAsk(CoreHelper.sma(pricesLowAsk, window));
//			b.setLowBid(CoreHelper.sma(pricesLowBid, window));
			b.setCloseAsk(CoreHelper.sma(pricesCloseAsk, window));
			b.setCloseBid(CoreHelper.sma(pricesCloseBid, window));
			return b;

		} else {
			throw new DataUnavailableException(dataType, candles.size(), window, "Not enough data");
		}
	}

	@Override
	public DataBar getEma(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		LimitedQueue<DataBar> queue = getPriceDataCache(instrument, dataType.name());
		List<DataBar> candles = queue.getElements();
		if(candles.size() >= window) {
//			double[] pricesHighAsk = new double[candles.size()];
//			double[] pricesLowAsk = new double[candles.size()];
//			double[] pricesOpenAsk = new double[candles.size()];
			double[] pricesCloseAsk = new double[candles.size()];
			
//			double[] pricesHighBid = new double[candles.size()];
//			double[] pricesLowBid = new double[candles.size()];
//			double[] pricesOpenBid = new double[candles.size()];
			double[] pricesCloseBid = new double[candles.size()];
			for(int i = 0; i < candles.size(); i++) {
//				pricesHighAsk[i] = candles.get(i).getHighAsk();
//				pricesLowAsk[i] = candles.get(i).getLowAsk();
//				pricesOpenAsk[i] = candles.get(i).getOpenAsk();
				pricesCloseAsk[i] = candles.get(i).getCloseAsk();
//				pricesHighBid[i] = candles.get(i).getHighBid();
//				pricesLowBid[i] = candles.get(i).getLowBid();
//				pricesOpenBid[i] = candles.get(i).getOpenBid();
				pricesCloseBid[i] = candles.get(i).getCloseBid();
			}
			DataBar b = new DataBar();
			
//			b.setOpenAsk(CoreHelper.ema(pricesOpenAsk, window));
//			b.setOpenBid(CoreHelper.ema(pricesOpenBid, window));
//			b.setHighAsk(CoreHelper.ema(pricesHighAsk, window));
//			b.setHighBid(CoreHelper.ema(pricesHighBid, window));
//			b.setLowAsk(CoreHelper.ema(pricesLowAsk, window));
//			b.setLowBid(CoreHelper.ema(pricesLowBid, window));
			b.setCloseAsk(CoreHelper.ema(pricesCloseAsk, window));
			b.setCloseBid(CoreHelper.ema(pricesCloseBid, window));
			return b;

		} else {
			throw new DataUnavailableException(dataType, candles.size(), window, "Not enough data");
		}
	}

	@Override
	public Direction emaDir(DataType dataType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		LimitedQueue<DataBar> queue = getPriceDataCache(instrument, dataType.name());
		List<DataBar> candles = queue.getElements();
		
		if(candles.size() > window) {
			double[] prices = new double[candles.size()];
			for(int i = 0; i < candles.size(); i++) {
				// TODO potential bias due to always using bid price?
				prices[i] = candles.get(i).getCloseBid();
			}
			if(CoreHelper.emaup(prices, window)) {
				return Direction.UP;
			} else {
				return Direction.DOWN;
			}
		} else {
			throw new DataUnavailableException(dataType, candles.size(), window, "Not enough data");
		}
	}

	@Override
	public DataBar getCurrentPrice(DataType dataType, String instrument) {
		LimitedQueue<DataBar> queue = getPriceDataCache(instrument, dataType.name());
		return queue.peekLast();
	}

	public boolean getNextBar(String instrument) {
		// Read next line in file
		try {
			String line = fileMap.get(instrument).readLine();
			if(line == null) { return false; }
			DataTick dataTick = new DataTick(instrument, line);
			DataBar dataBar = new DataBar(dataTick);
			updateCache(dataBar);
			eventQueue.add(new MarketEvent(dataTick));
			if(fileReadCounter % 250000 == 0 && fileReadCounter > 0) {
				end = new Date();
				log.info("Counts: " + (fileReadCounter / (end.getTime() - start.getTime())) + " tpms");
				fileReadCounter = 0;
				start = new Date();
			} else {
				fileReadCounter++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public void updateCache(DataBar tick) {


		updateBars(DataType.TICK, tick);
		//printQueue(queue, tick);
		updateBars(DataType.MINUTE1, tick);
		//printQueue(queue, tick);
		updateBars(DataType.MINUTE5, tick);
		//printQueue(queue, candle);
		updateBars(DataType.MINUTE15, tick);
		//printQueue(queue, candle);
		updateBars(DataType.MINUTE30, tick);
		//printQueue(queue, candle);
		updateBars(DataType.HOUR1, tick);
		//printQueue(queue, candle);
		updateBars(DataType.HOUR4, tick);
		//printQueue(queue, candle);
		updateBars(DataType.DAY, tick);
		//printQueue(queue, tick);
		updateBars(DataType.WEEK, tick);
		//printQueue(queue, candle);
		updateBars(DataType.MONTH, tick);
		//printQueue(queue, tick);
	}	
	

	
//	public int getBarCount(DataType type, String instrument) {
//		LimitedQueue<DataBar> queue = getPriceDataCache(instrument, type.name());
//		return queue.size();
//	}
	
//	private void updateBars(LimitedQueue<DataBar> queue, OandaCandles candles) {
//		for(DataBar c: candles.getCandles()) {
//			queue.add(c);
//		}
//	}

	private void updateBars(DataType type, DataBar tick) {
		LimitedQueue<DataBar> queue = getPriceDataCache(tick.getInstrument(), type.name());
		DataBar last = queue.peekLast();
		if(last == null) {
			queue.add(tick);
		} else {
			//log.info("Candle: " + candle);
			//log.info("Time: " + candle.toBar(candle.getTimeScale()));
			//log.info("Last: " + last.toBar(candle.getTimeScale()));
			if(tick.toBar(type).equals(last.toBar(type))) {
				updateLastDataBar(last, tick);
			} else {
				queue.add(tick);
			}
		}
	}

	private void updateLastDataBar(DataBar last, DataBar tick) {
		if(last.getHighBid() < tick.getHighBid()) {
			last.setHighBid(tick.getHighBid());
		}
		if(last.getLowBid() > tick.getLowBid()) {
			last.setLowBid(tick.getLowBid());
		}
		if(last.getHighAsk() < tick.getHighAsk()) {
			last.setHighAsk(tick.getHighAsk());
		}
		if(last.getLowAsk() > tick.getLowAsk()) {
			last.setLowAsk(tick.getLowAsk());
		}
		last.setCloseBid(tick.getCloseBid());
		last.setCloseAsk(tick.getCloseAsk());
	}

	private void printQueue(LimitedQueue<DataBar> queue, DataBar tick) {
		log.info(tick.getTime() + ": Updating with: " + tick);
		String outh = "";
		String outl = "";
		String outo = "";
		String outc = "";
		for(DataBar c: queue) {
			outh += c.getHighBid() + " ";
			outl += c.getLowBid() + " ";
			outo += c.getOpenBid() + " ";
			outc += c.getCloseBid() + " ";
		}
		log.info("Prices: ");
		log.info("High: " + outh);
		log.info("Low: " + outl);
		log.info("Open: " + outo);
		log.info("Close: " + outc);
		log.info("");
	}

	public void printCache() {
		log.info("{");
		for(String key1: priceCache.keySet()) {
			log.info("\tkey1: " + key1 + "{");
			for(String key2: priceCache.get(key1).keySet()) {
				log.info("\t\tkey2: " + key2 + "{");
				LimitedQueue<DataBar> queue = priceCache.get(key1).get(key2);
				for(DataBar c: queue) {
					log.info("\t\t\t\t{" + c.toString() + "}");
				}
				log.info("\t\t}");
			}
			log.info("\t}");
		}
		log.info("}");
	}

	
	public LimitedQueue<DataBar> getPriceDataCache(String key1, String key2) {
		//log.info("Getting cahce for: Key1: " + key1 + " Key2: " + key2);

		HashMap<String, LimitedQueue<DataBar>> instrumentCache = priceCache.get(key1);
		if(instrumentCache == null) {
			instrumentCache = new HashMap<String, LimitedQueue<DataBar>>();
			priceCache.put(key1, instrumentCache);
		}
		
		LimitedQueue<DataBar> queue = instrumentCache.get(key2);
		if(queue == null) {
			queue = new LimitedQueue<DataBar>(cacheSize);
			instrumentCache.put(key2, queue);
		}
		return queue;
	}

	
	@Override
	public double getAtrPips(DataType dataType, String instrument, int amount) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getHeikenAshiEma(DataType dataType, String instrument, int amount) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public LimitedQueue<DataBar> getHeikenAshiBars(DataType dataType, String instrument, int amount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LimitedQueue<DataBar> getHeikenAshiRenkoBars(DataType dataType, String instrument, int barsize, int amount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Portfolio loadPortFolio() {
		return FileObjectUtil.loadPortfolio(new File(portfolioFileName));
	}

	@Override
	public void savePortFolio(Portfolio portfolio) {
		FileObjectUtil.savePortfolio(portfolio, new File(portfolioFileName));
	}

}
