package net.nilosplace.Forex.client.managers;

import net.nilosplace.Forex.client.events.Event;

public abstract class Manager {

	public abstract void handle(Event e);
	
}
