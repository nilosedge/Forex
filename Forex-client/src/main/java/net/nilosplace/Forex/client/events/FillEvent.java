package net.nilosplace.Forex.client.events;

import net.nilosplace.Forex.client.models.ForexFilledOrder;

public class FillEvent extends Event {

	private ForexFilledOrder filledOrder;

	public ForexFilledOrder getFilledOrder() {
		return filledOrder;
	}
	public void setFilledOrder(ForexFilledOrder filledOrder) {
		this.filledOrder = filledOrder;
	}

}
