package net.nilosplace.Forex.client.models;

public class ForexExitOrder extends ForexOrder {

	private String instrument;
	private int units;
	private double originalPrice;
	
	public ForexExitOrder(int strategyId, String instrument, int units, double originalPrice) {
		super(strategyId);
		this.instrument = instrument;
		this.units = units;
		this.originalPrice = originalPrice;
	}
	
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public int getUnits() {
		return units;
	}
	public void setUnits(int units) {
		this.units = units;
	}
	public double getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(double originalPrice) {
		this.originalPrice = originalPrice;
	}
	
	@Override
	public String toString() {
		return "Id: " + strategyId + " I: " + instrument + " U: " + units + " OP: " + originalPrice;
	}
}
