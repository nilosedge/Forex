package net.nilosplace.Forex.client.UI.frames;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import net.nilosplace.Forex.client.Oanda.OandaRateEventHandlerInterface;

import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.AccountException;
import com.oanda.fxtrade.api.FXRateEventInfo;

public class OandaAccountFrame extends JFrame implements OandaRateEventHandlerInterface {

	private static final long	serialVersionUID	= 5977721011710970059L;
	private Account account;
	
	private JLabel label01 = new JLabel();
	private JLabel label02 = new JLabel();
	private JLabel label03 = new JLabel();
	private JLabel label04 = new JLabel();
	private JLabel label05 = new JLabel();
	private JLabel label06 = new JLabel();
	private JLabel label07 = new JLabel();
	private JLabel label08 = new JLabel();
	private JLabel label09 = new JLabel();
	private JLabel label10 = new JLabel();
	private JLabel label11 = new JLabel();
	private JLabel label12 = new JLabel();
	private JLabel label13 = new JLabel();
	private JLabel label14 = new JLabel();
	
	public OandaAccountFrame(Account account) {
		super("Oanda Account");
		this.account = account;
		JPanel p = new JPanel(new GridLayout(14, 1));
		p.add(label01); p.add(label02); p.add(label03); p.add(label04);
		p.add(label05); p.add(label06); p.add(label07); p.add(label08);
		p.add(label09); p.add(label10); p.add(label11); p.add(label12);
		p.add(label13); p.add(label14);
		add(p);
		setSize(200, 300);
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		updateLabels();
	}


	@Override
	public void handle(FXRateEventInfo info) {
		updateLabels();
	}

	private void updateLabels() {
		try {
			label01.setText("AccountId: " + account.getAccountId());
			label02.setText("AccountName: " + account.getAccountName());
			label03.setText("Balance: " + account.getBalance());
			label04.setText("CreateDate: " + account.getCreateDate());
			label05.setText("EnteredMarginCallTime: " + account.getEnteredMarginCallTime());
			label06.setText("HomeCurrency: " + account.getHomeCurrency());
			label07.setText("MarginAvailable: " + account.getMarginAvailable());
			label08.setText("MarginCallRate: " + account.getMarginCallRate());
			label09.setText("MarginRate: " + account.getMarginRate());
			label10.setText("MarginUsed: " + account.getMarginUsed());
			label11.setText("PositionValue: " + account.getPositionValue());
			label12.setText("Profile: " + account.getProfile());
			label13.setText("RealizedPL: " + account.getRealizedPL());
			label14.setText("UnrealizedPL: " + account.getUnrealizedPL());

		} catch (AccountException e) {
			e.printStackTrace();
		}
	}
}
