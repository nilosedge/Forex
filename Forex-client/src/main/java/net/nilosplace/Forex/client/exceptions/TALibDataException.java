package net.nilosplace.Forex.client.exceptions;

import com.tictactec.ta.lib.RetCode;

public class TALibDataException extends Exception {

	private static final long	serialVersionUID	= -4896101248121884944L;
	RetCode retCode;
	
	public TALibDataException(RetCode retCode) {
		this.retCode = retCode;
	}

}
