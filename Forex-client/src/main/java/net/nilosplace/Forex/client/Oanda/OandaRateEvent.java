package net.nilosplace.Forex.client.Oanda;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.oanda.fxtrade.api.FXEventInfo;
import com.oanda.fxtrade.api.FXEventManager;
import com.oanda.fxtrade.api.FXRateEvent;
import com.oanda.fxtrade.api.FXRateEventInfo;

public class OandaRateEvent extends FXRateEvent {

	protected ArrayList<OandaRateEventHandlerInterface> handlers = new ArrayList<OandaRateEventHandlerInterface>();
	private List<String> instruments;
	protected Properties programConfig;
	private Logger log = Logger.getLogger(getClass());

	public OandaRateEvent(Properties programConfig) {
		this.programConfig = programConfig;
		String instruments = (String)programConfig.get("instruments");
		this.instruments = Arrays.asList(instruments.split(",")); 
	}

	public void registerHandler(OandaRateEventHandlerInterface handler) {
		handlers.add(handler);
	}

	@Override
	public void handle(FXEventInfo event, FXEventManager manager) {
		FXRateEventInfo ri = (FXRateEventInfo)event;
		if(!instruments.contains(ri.getPair().getPair())) return;
		
		for(OandaRateEventHandlerInterface h: handlers) {
			h.handle(ri);
		}
	}
}