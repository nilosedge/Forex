package net.nilosplace.Forex.client.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.oanda.OandaSystemType;
import net.nilosplace.Forex.client.events.Event;

public abstract class Program extends Thread {

	protected ConcurrentLinkedQueue<Event> eventQueue = new ConcurrentLinkedQueue<Event>();
	protected List<String> instrumentList;
	protected OandaSystemType type;
	protected Properties programConfig;
	
	public Program(Properties programConfig) {
		String instruments = (String)programConfig.get("instruments");
		this.instrumentList = Arrays.asList(instruments.split(","));
		
		try {
			programConfig.load(new FileInputStream(new File(programConfig.getProperty("oandaPropertiesFile"))));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		type = OandaSystemType.valueOf(programConfig.getProperty("oanda_system_type"));
		this.programConfig = programConfig;
	}
	
	protected String getUsername() {
		return programConfig.getProperty(type.getUsername());
	}
	
	protected String getPassword() {
		return programConfig.getProperty(type.getPassword());
	}

	public abstract void run();

}
