package net.nilosplace.Forex.client.models;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DataTick implements Serializable {

	private static final long serialVersionUID = -151183107964091266L;
	private double bid;
	private double ask;
	private String instrument;
	private Date timeStamp = new Date();
	
	public DataTick(String instrument, String cvsLine) {

		String year, mon, day, hour, min, sec, msec;
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		
		String[] array = cvsLine.split(",");

		year = array[0].substring(0, 4);
		mon = array[0].substring(4, 6);
		day = array[0].substring(6, 8);
		
		hour = array[0].substring(9, 11);
		min = array[0].substring(11, 13);
		sec = array[0].substring(13, 15);
		
		msec = array[0].substring(15, 18);
		
		cal.set(
				Integer.parseInt(year),
				Integer.parseInt(mon) - 1,
				Integer.parseInt(day),
				Integer.parseInt(hour),
				Integer.parseInt(min),
				Integer.parseInt(sec));
		
		cal.setTimeInMillis(cal.getTimeInMillis() + Integer.parseInt(msec));

		this.instrument = instrument;
		this.bid = Double.parseDouble(array[1]);
		this.ask = Double.parseDouble(array[2]);
		this.timeStamp = cal.getTime();
		if(bid > ask) {
			double t = bid;
			bid = ask;
			ask = t;
		}
	}

	public DataTick(String instrument, double bid, double ask, long timestamp) {
		this.instrument = instrument;
		this.bid = bid;
		this.ask = ask;
		this.timeStamp = new Date(timestamp * 1000);
	}

	public DataTick() {}

	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public double getAsk() {
		return ask;
	}
	public void setAsk(double ask) {
		this.ask = ask;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public String toString() {
		return "B: " + bid + " A: " + ask + " I: " + instrument + " T: " + timeStamp;
	}
}
