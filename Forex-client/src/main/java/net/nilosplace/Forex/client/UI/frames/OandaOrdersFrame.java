package net.nilosplace.Forex.client.UI.frames;

import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import net.nilosplace.Forex.client.Oanda.OandaRateEventHandlerInterface;

import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.AccountException;
import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.MarketOrder;
import com.oanda.fxtrade.api.Position;

public class OandaOrdersFrame extends JFrame implements OandaRateEventHandlerInterface {

	private static final long	serialVersionUID	= -972228263053449741L;
	private Account account;
	private Vector<MarketOrder> trades;
	private Vector<Position> positions;
	private JLabel positionsLabel = new JLabel("Positions");
	private JLabel tradesLabel = new JLabel("Trades");
	
	public OandaOrdersFrame(Account account) {
		super("Oanda Orders");
		this.account = account;
		JPanel p = new JPanel(new GridLayout(2, 1));

		//JLabel up = new JLabel(new ImageIcon("green-arrows-set-up-clip-art.jpg"));
		//up.setSize(8, 11);
		//JLabel down = new JLabel(new ImageIcon("red-arrows-set-down-clip-art.jpg"));
		//down.setSize(8, 11);
		p.add(positionsLabel);
		p.add(tradesLabel);
		add(p);
		setSize(200, 400);
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	@Override
	public void handle(FXRateEventInfo info) {
		try {
			trades = account.getTrades();
			StringBuffer buf = new StringBuffer("Trades:<br>");
			if(trades != null && trades.size() > 0) {
				for(MarketOrder o: trades) {
					buf.append(getPriceInfo(o, info));
				}
			} else {
				buf.append("No Trades");
			}
			tradesLabel.setText("<html>" + buf.toString() + "</html>");
			
			positions = account.getPositions();
			buf = new StringBuffer("Positions:<br>");
			if(positions != null && positions.size() > 0) {
				for(Position p: positions) {
					buf.append(getPriceInfo(p, info));
				}
			} else {
				buf.append("No Positions<br>");
			}
			positionsLabel.setText("<html>" + buf.toString() + "</html>");
			// Positions are the sum of all trades
		} catch (AccountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private Object getPriceInfo(MarketOrder o, FXRateEventInfo info) {
		StringBuffer buf = new StringBuffer();
		buf.append(o.getPair().getPair() + ":<br>");
		buf.append("Price: " + o.getPrice() + " Current: " + info.getTick().getMean() + "<br>");
		buf.append("Units: " + o.getUnits() + "<br>");
		buf.append("UnPL: " + o.getUnrealizedPL(info.getTick()) + "<br>");
		return buf.toString();
	}

	public String getPriceInfo(Position p, FXRateEventInfo info) {
		StringBuffer buf = new StringBuffer();
		buf.append(p.getPair().getPair() + ":<br>");
		buf.append("Price: " + p.getPrice() + " Current: " + info.getTick().getMean() + "<br>");
		buf.append("Units: " + p.getUnits() + "<br>");
		buf.append("UnPL: " + p.getUnrealizedPL(info.getTick()) + "<br>");
		return buf.toString();
	}
}
