package net.nilosplace.Forex.client.managers.execution;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.AccountException;
import com.oanda.fxtrade.api.MarketOrder;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.RateTable;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.FillEvent;
import net.nilosplace.Forex.client.managers.DataManager;
import net.nilosplace.Forex.client.managers.ExecutionManager;
import net.nilosplace.Forex.client.models.ForexExitOrder;
import net.nilosplace.Forex.client.models.ForexFilledOrder;
import net.nilosplace.Forex.client.models.ForexLimitOrder;
import net.nilosplace.Forex.client.models.ForexMarketOrder;

public class OandaExecutionManager extends ExecutionManager {

	private Logger log = Logger.getLogger(getClass());
	private HashMap<Integer, LinkedList<MarketOrder>> map = new HashMap<Integer, LinkedList<MarketOrder>>();
	
	private ConcurrentLinkedQueue<Event> eventQueue;
	private DataManager dataManager;
	private RateTable rateTable;
	private Account account;
	
	public OandaExecutionManager(Properties programConfig, DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue, RateTable rateTable, Account account) {
		this.dataManager = dataManager;
		this.eventQueue = eventQueue;
		this.rateTable = rateTable;
		this.account = account;
	}

	@Override
	protected void handleMarketOrder(ForexMarketOrder marketOrder) {
		
		MarketOrder m = API.createMarketOrder();
		m.setPair(rateTable.get(marketOrder.getInstrument()));
		m.setUnits(marketOrder.getUnits());
		log.info("Enter Order: " + m);
		try {
			printOrder(m);
			account.execute(m);
			getList(marketOrder.getStrategyId()).push(m);
		} catch (OAException e) {
			e.printStackTrace();
		}
		
		
//		FillEvent fe = new FillEvent();
//		ForexFilledOrder fill = new ForexFilledOrder(marketOrder.getStrategyId());
//		if(marketOrder.getUnits() > 0) {
//			fill.setFilledPrice(dataManager.getCurrentPrice(DataType.TICK, marketOrder.getInstrument()).getCloseAsk());
//		} else {
//			fill.setFilledPrice(dataManager.getCurrentPrice(DataType.TICK, marketOrder.getInstrument()).getCloseBid());
//		}
//		
//		fill.setForexOrder(marketOrder);
//		fe.setFilledOrder(fill);
//		//log.info("Filled Order: " + fill);
//		eventQueue.add(fe);
		
	}

	@Override
	protected void handleLimitOrder(ForexLimitOrder limitOrder) {
		
		
	}

	@Override
	protected void handleExitOrder(ForexExitOrder exitOrder) {
		try {
			MarketOrder o = getList(exitOrder.getStrategyId()).pop();
			log.info("Exit Order: " + o);
			account.close(o);
		} catch (OAException e) {
			e.printStackTrace();
		}
	}
	
	private void printOrder(MarketOrder m) {
		log.info("getHighPriceLimit: " + m.getHighPriceLimit());
		log.info("getLowPriceLimit: " + m.getLowPriceLimit());
		log.info("getPrice: " + m.getPrice());
		log.info("getRealizedPL: " + m.getRealizedPL());
		log.info("getTimestamp: " + m.getTimestamp());
		log.info("getTrailingStopLoss: " + m.getTrailingStopLoss());
		log.info("getTransactionLink: " + m.getTransactionLink());
		log.info("getTransactionNumber: " + m.getTransactionNumber());
		log.info("getUnits: " + m.getUnits());
	}

	private LinkedList<MarketOrder> getList(Integer strategyId) {
		LinkedList<MarketOrder> list = map.get(strategyId);
		if(list == null) {
			list = new LinkedList<MarketOrder>();
			map.put(strategyId, list);
		}
		return list;
	}
}
