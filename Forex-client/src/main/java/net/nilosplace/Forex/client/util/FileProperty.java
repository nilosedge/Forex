package net.nilosplace.Forex.client.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public enum FileProperty {

	OANDAPROPERTIESFILE				("net.nilosplace.oanda.properties.file") { },
	MYFXPROPERTIESFILE				("net.nilosplace.myfx.properties.file") { },
	;
	
	private String propertyName;

	
	private FileProperty(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getPropertyName() { return propertyName; }
	
	public String toString() {
		return System.getProperties().getProperty(propertyName);
	}
	
	public String getValue(String key) {
		try {
			Properties props = new Properties();
			InputStream in = new FileInputStream(new File(System.getProperties().getProperty(propertyName)));
			props.load(in);
			in.close();

			if(key == null) return null;
			else return props.getProperty(key);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
