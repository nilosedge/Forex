package net.nilosplace.Forex.client.events;

import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.models.Strategy;

public class SignalEvent extends Event {

	private Strategy strategy;
	private String instrument;
	private SignalType signalType;
	private double strength;

	public SignalEvent(Strategy strategy, String instrument, SignalType type, double strength) {
		this.strategy = strategy;
		this.instrument = instrument;
		this.signalType = type;
		this.strength = strength;
	}
	
	public Strategy getStrategy() {
		return strategy;
	}
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public SignalType getSignalType() {
		return signalType;
	}
	public void setSignalType(SignalType signalType) {
		this.signalType = signalType;
	}
	public double getStrength() {
		return strength;
	}
	public void setStrength(double strength) {
		this.strength = strength;
	}
}
