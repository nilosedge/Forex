package net.nilosplace.Forex.client.Oanda;

import com.oanda.fxtrade.api.FXAccountEventInfo;

public interface OandaAccountEventHandlerInterface {

	public void handle(FXAccountEventInfo event);
}
