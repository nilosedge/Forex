package net.nilosplace.Forex.client.managers;

import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Forex.client.enums.DataType;
import net.nilosplace.Forex.client.enums.SignalType;
import net.nilosplace.Forex.client.events.Event;
import net.nilosplace.Forex.client.events.FillEvent;
import net.nilosplace.Forex.client.events.OrderEvent;
import net.nilosplace.Forex.client.events.SignalEvent;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.ForexExitOrder;
import net.nilosplace.Forex.client.models.ForexLimitOrder;
import net.nilosplace.Forex.client.models.ForexMarketOrder;
import net.nilosplace.Forex.client.models.ForexOrder;
import net.nilosplace.Forex.client.models.Portfolio;
import net.nilosplace.Forex.client.models.Position;

import org.apache.log4j.Logger;

public class PortfolioManager extends Manager {

	private Logger log = Logger.getLogger(getClass());
	private DataManager dataManager;
	private ConcurrentLinkedQueue<Event> eventQueue;
	private Portfolio portfolio;

	public PortfolioManager(Properties programConfig, DataManager dataManager, ConcurrentLinkedQueue<Event> eventQueue) {
		this.dataManager = dataManager;
		this.eventQueue = eventQueue;
		portfolio = dataManager.loadPortFolio();
	}

	@Override
	public void handle(Event e) {
		if(e instanceof SignalEvent) {
			updateSignal((SignalEvent)e);
		} else if(e instanceof FillEvent) {
			ForexOrder fo = ((FillEvent)e).getFilledOrder().getForexOrder();
			if(fo instanceof ForexMarketOrder) {
				updateMarketFill((ForexMarketOrder)fo, ((FillEvent)e).getFilledOrder().getFilledPrice());
			} else if(fo instanceof ForexLimitOrder) {
				//updateLimitFile((ForexLimitOrder)fo);
			} else if(fo instanceof ForexExitOrder) {
				updateExitFill((ForexExitOrder)fo, ((FillEvent)e).getFilledOrder().getFilledPrice());
			}
		}
	}

	private void updateExitFill(ForexExitOrder feo, double filledPrice) {
		printCurrnetValue();
		
		Position p = portfolio.getPosition(feo.getInstrument(), feo.getStrategyId());
		
		double profit = calcProfitLoss(feo.getInstrument(), feo.getOriginalPrice(), filledPrice, Math.abs(feo.getUnits()));
		log.info("Profit: " + profit);
		portfolio.updateCapital(profit);

		p.updateFill(feo.getUnits(), filledPrice);

		printCurrnetValue();
		
		dataManager.savePortFolio(portfolio);
		log.info("");
	}

	private void updateMarketFill(ForexMarketOrder fmo, double filledPrice) {
		//printCurrnetValue();
		
		Position p = portfolio.getPosition(fmo.getInstrument(), fmo.getStrategyId());
		p.updateFill(fmo.getUnits(), fmo.getPrice());

		//printCurrnetValue();
		dataManager.savePortFolio(portfolio);
		log.info("");
	}

	private void updateSignal(SignalEvent e) {

		double onepercent = portfolio.getCapital() / 100;
		int dailyRange = 10;
		double pipSizeDollars = onepercent / dailyRange;

		DataBar current = dataManager.getCurrentPrice(DataType.TICK, e.getInstrument());
		double current_price = 0;
		if(e.getSignalType() == SignalType.LONG) {
			current_price = current.getCloseAsk();
		}
		if(e.getSignalType() == SignalType.SHORT) {
			current_price = current.getCloseBid();
		}
		if(e.getSignalType() == SignalType.EXIT) {

		}

		int units = (int)(calcUnits(e.getInstrument(), current_price, pipSizeDollars) * e.getStrength());
		Position p = portfolio.getPosition(e.getInstrument(), e.getStrategy().getId());
		int current_units = p.getUnits();

		ForexOrder o = null;
		if(e.getSignalType() == SignalType.LONG) {
			o = new ForexMarketOrder(e.getStrategy().getId(), e.getInstrument(), units, current_price);
		}
		if(e.getSignalType() == SignalType.SHORT) {
			o = new ForexMarketOrder(e.getStrategy().getId(), e.getInstrument(), -units, current_price);
		}
		if(e.getSignalType() == SignalType.EXIT) {
			current_price = p.getPrice();
			o = new ForexExitOrder(e.getStrategy().getId(), e.getInstrument(), -current_units, current_price);

		}
		log.info("Order: O: " + o);
		eventQueue.add(new OrderEvent(o));

	}

	// Strategies Performance

	// Total return
	// return = ((final portfolio dollar value) − (initial portfolio value))/(final portfolio dollar value) × 100

	// Portfolie Performance
	// Equity Curve

	//Pip risk $1.00


	// TODO This method needs some testing!!
	public int calcUnits(String instrument, double current_price, double pipValue) {

		double pipSize = lookupPipSize(instrument);
		double ret = 0;

		if(instrument.contains("USD")) {
			if(instrument.startsWith("USD")) { // Indirect Rate
				//USD/JPY
				//13046.31 = (130.4631 * 1.00) / 0.01
				ret = (current_price * pipValue) / pipSize;

			} else if(instrument.endsWith("USD")) { // Direct Rate
				// GBP/USD
				ret = pipValue/pipSize;
				// $1.00 / 0.0001 = 10000 units

			} else {
				log.error("This should NEVER happen");
			}
		} else { // Cross Rate
			double current_cross_price = 0;
			// EUR/GBP
			// 8551.52 = ($1.50 * 0.675) / (0.0001 * 1.1840 EUR/USD)
			ret = (pipValue * current_price) / (pipSize * current_cross_price);

		}
		return (int)ret;
	}

	public double calcProfitLoss(String instrument, double entry_price, double current_price, int units) {
		log.info("Calc Profit: I: " + instrument + " E: " + entry_price + " C: " + current_price + " U: " + units);
		double pl = 0;

		if(instrument.contains("USD")) {
			if(instrument.startsWith("USD")) { // Indirect Rate
				//USD/JPY
				// $2.0003 = (130.4631 - 130.4431) * 13046.31 / 130.4431
				// $PL = (Sell - Purchase) * Units / current_price
				pl = ((current_price - entry_price) * units) / current_price;

			} else if(instrument.endsWith("USD")) { // Direct Rate
				// GBP/USD
				// $PL = (Sell - Purchase) * Units
				// $35.00 = (1.7540 - 1.7505) * 10000
				pl = (current_price - entry_price) * units;

			} else {
				log.error("This should NEVER happen");
			}
		} else { // Cross Rate
			double current_cross_price = 0;
			log.info("NEED TO CALC CROSS PRICE !!!!!!");
			// EUR/GBP
			// $PL = ((Sell - Buy) * Units * current_cross_price) / current_price
			// $1.50 = ((0.6751 - 0.6750) * Units * 1.1840) / 0.6750
			pl = ((current_price - entry_price) * units * current_cross_price) / current_price;

		}
		return pl;
	}

	private double lookupPipSize(String instrument) {
		if(instrument.contains("JPY")) {
			return 0.01;
		}
		return 0.001;
	}

	public void printCurrnetValue() {
		double tp = 0;
		HashMap<String, HashMap<Integer, Position>> positions = portfolio.getPositions();

		for(String instrument: positions.keySet()) {
			for(Integer strategyId: positions.get(instrument).keySet()) {
				Position p = positions.get(instrument).get(strategyId);
				DataBar bar = dataManager.getCurrentPrice(DataType.TICK, instrument);
				double cp = 0;
				if(p.getUnits() > 0) {
					cp = bar.getCloseBid();
				} else {
					cp = bar.getCloseAsk();
				}
				//log.info("printCurrnetValue: I: " + instrument + " PP: " + p.getPrice() + " CP: " + cp + " U: " + p.getUnits());
				double profit = calcProfitLoss(instrument, p.getPrice(), cp, p.getUnits());
				log.info(strategyId + ": " + instrument + ": " + " profit: " + profit);
				tp += profit;
			}
		}

		log.info("printCurrnetValue: initialCapital: " + portfolio.getCapital() + " Total Value: " + (portfolio.getCapital() + tp));

	}
}
