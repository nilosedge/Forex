package net.nilosplace.Forex.client.UI.components;

import java.awt.Color;
import java.awt.Graphics2D;
import java.text.DecimalFormat;

import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.DataTick;


public class PriceComponent {

	private double longPrice;
	private double shortPrice;
	private DataBar bar;
	private DataTick tick;
	private double pipSize;
	private double pips;
	private DecimalFormat df = new DecimalFormat("#.00000");
	
	public void redraw(Graphics2D g) {
		if(bar == null || tick == null) return;
		g.setColor(Color.black);
		g.drawString("Long Price: " + df.format(longPrice), 80, 30);
		
		g.drawLine(10, 30, 75, 30);
		g.drawLine(210, 30, 260, 30);
		
		g.setColor(Color.black);
		g.drawString("Short Price: " + df.format(shortPrice), 80, 330);
		
		g.drawLine(10, 330, 75, 330);
		g.drawLine(210, 330, 260, 330);
		
		int bidX = (int)(((longPrice - tick.getBid()) / (pips * pipSize)) * 150);

		g.drawLine(160, 30 + bidX, 175, 30 + bidX);
		g.drawString("Bid: " + df.format((tick.getBid())), 180, 30 + bidX);
		
		int askX = (int)(300 - (((tick.getAsk() - shortPrice) / (pips * pipSize)) * 150));
		
		g.drawLine(10, 30 + askX, 25, 30 + askX);
		g.drawString("Ask: " + df.format(tick.getAsk()), 30, 30 + askX);
		
	}

	public void updateNumbers(double longPrice, double shortPrice, DataBar bar, DataTick tick, double pips, double pipSize) {
		this.bar = bar;
		this.longPrice = longPrice;
		this.shortPrice = shortPrice;
		this.tick = tick;
		this.pipSize = pipSize;
		this.pips = pips;
	}
	
}
