package net.nilosplace.Forex.client;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;

import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.models.DataBar;

import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.MarketOrder;

public class StatsFrame extends JFrame {

	private Instrument purchasePair;
	private Instrument hedgePairB;
	private Instrument hedgePairA;
	private boolean showZone;
	private FXRateEventInfo purchasePairInfo;
	private FXRateEventInfo hedgePair1Info;
	private double ATRSize;
	//private int zoneSize;
	private FXRateEventInfo hedgePair2Info;
	private Direction step2EMADirection;
	private Direction step2LastBarDirection;
	private DataBar step2EMA;
	private Direction rule1CurrentBarDirection;
	private Direction step3LastBarDirection;
	private double positionPrice;
	private long positionUnits;
	private int entryCount;
	private Direction rule1LastBarDirection;
	private Direction step1;
	private Direction step3;
	private Direction step2;
	private double[] macd;
	private int	aTRZoneSize;
	private double aTRZoneEdgePrice;
	private int	aTRParts;
	
	
	// Zone Info Vars
	private int numberOfTrades;
	private int gainPips;
	private int zoneInfoZoneSize;
	private double zoneEdgePrice;
	private double longTargetPrice;
	private double shortTargetPrice;
	private ArrayList<Double> mults;
	private long ziEntryUnits;
	private double ziEntryPrice;
	private boolean nextOrderHedge;
	private boolean[] orders;
	private double	unrealizedPL;


	private JLabel label = new JLabel();
	private DecimalFormat pf = new DecimalFormat("#.00000");
	private DecimalFormat pipf = new DecimalFormat("#.0");
	private TreeMap<String, String> sorted = new TreeMap<String, String>();




	public StatsFrame() {
		setSize(640, 580);
		add(label);
		setVisible(true);
	}

	public void setPurchasePair(Instrument purchasePair) { this.purchasePair = purchasePair; }
	public void setHedgePair1(Instrument hedgePairA) { this.hedgePairA = hedgePairA; }
	public void setHedgePair2(Instrument hedgePairB) { this.hedgePairB = hedgePairB; }
	public void showZoneRecovery(boolean showZone) { this.showZone = showZone; }
	public void updatePurchasePair(FXRateEventInfo purchasePairInfo) { this.purchasePairInfo = purchasePairInfo; }
	public void updateHedgePair1(FXRateEventInfo hedgePair1Info) { this.hedgePair1Info = hedgePair1Info; }
	public void updateHedgePair2(FXRateEventInfo hedgePair2Info) { this.hedgePair2Info = hedgePair2Info; }
	//public void setCurrentZoneSize(int zoneSize) { this.zoneSize = zoneSize; }
	public void setATR(double ATRSize) { this.ATRSize = ATRSize; }
	public void setStep2EMADirection(Direction step2EMADirection) { this.step2EMADirection = step2EMADirection; }
	public void setStep2LastBarDirection(Direction step2LastBarDirection) { this.step2LastBarDirection = step2LastBarDirection; }
	public void setStep2EMA(DataBar step2EMA) { this.step2EMA = step2EMA; }
	public void setStep3LastBarDirection(Direction step3LastBarDirection) { this.step3LastBarDirection = step3LastBarDirection; }
	public void setRule1LastBarDirection(Direction rule1LastBarDirection) { this.rule1LastBarDirection = rule1LastBarDirection; }
	public void setRule1CurrentBarDirection(Direction rule1CurrentBarDirection) { this.rule1CurrentBarDirection = rule1CurrentBarDirection; }
	public void updateSteps(Direction step1, Direction step2, Direction step3) { this.step1 = step1; this.step2 = step2; this.step3 = step3; }
	public void setPositionUnits(long positionUnits) { this.positionUnits = positionUnits; }
	public void setPositionPrice(double positionPrice) { this.positionPrice = positionPrice; }
	public void setEntryCount(int entryCount) { this.entryCount = entryCount; }
	public void setStep3MACD(double[] macd) { this.macd = macd; }
	public void updateUnrealizedPL(double unrealizedPL) { this.unrealizedPL = unrealizedPL; }
	public void setATRZoneSize(int aTRZoneSize) { this.aTRZoneSize = aTRZoneSize; }
	public void setATRATRZoneEdgePrice(double aTRZoneEdgePrice) { this.aTRZoneEdgePrice = aTRZoneEdgePrice; }
	public void setATRParts(int aTRParts) { this.aTRParts = aTRParts; }
	
	// Zone Info Methods
	public void setZoneInfoNumberOfTrades(int numberOfTrades) { this.numberOfTrades = numberOfTrades; }
	public void setZoneInfoGainPips(int gainPips) { this.gainPips = gainPips; }
	//public void setZoneInfoOffset(int offset) { this.offset = offset; }
	public void setZoneInfoZoneSize(int zoneInfoZoneSize) { this.zoneInfoZoneSize = zoneInfoZoneSize; }
	public void setZoneInfoZoneEdgePrice(double zoneEdgePrice) { this.zoneEdgePrice = zoneEdgePrice; }
	public void setZoneInfoLongTargetPrice(double longTargetPrice) { this.longTargetPrice = longTargetPrice; }
	public void setZoneInfoShortTargetPrice(double shortTargetPrice) { this.shortTargetPrice = shortTargetPrice; }
	public void setZoneInfoMults(ArrayList<Double> mults) { this.mults = mults; }
	public void setZoneInfoEntryUnits(long ziEntryUnits) { this.ziEntryUnits = ziEntryUnits; }
	public void setZoneInfoEntryPrice(double ziEntryPrice) { this.ziEntryPrice = ziEntryPrice; }
	public void setZoneInfoNextOrderHedge(boolean nextOrderHedge) { this.nextOrderHedge = nextOrderHedge; }
	public void setZoneInfoOrders(boolean[] orders) { this.orders = orders; }
	
	//System.out.println("Step 2: EMA: " + ema);
	//System.out.println("Current ATR: " + (int)(ATRSize / purchasePair.getPIP()));
	//System.out.println("Step 2: EMA: " + emaDirection + " Bar: " + bars.peekLast().getDir() + " Price Above: " + (info.getTick().getAsk() > ema.getCloseAsk()) + " Price Below: " + (info.getTick().getBid() < ema.getCloseBid()));



	public void updateStats() {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<html>");
		generateHeader(buffer);

		generateTableHeader(buffer);

		buffer.append("</html>");

		label.setText(buffer.toString());
	}

	private void generateHeader(StringBuffer buffer) {
		buffer.append("<h1>" + purchasePair.getPair() + " = " + hedgePairA.getPair() + " * " + hedgePairB.getPair() + "</h1>");
		if(purchasePairInfo != null && hedgePair1Info != null && hedgePair2Info != null) {
			buffer.append("<h1>" + pf.format(purchasePairInfo.getTick().getBid()) + " = " + pf.format(hedgePair1Info.getTick().getBid()) + " * " + pf.format(hedgePair2Info.getTick().getBid()) + "</h1>");
			buffer.append("ARB 0: " + pf.format((hedgePair1Info.getTick().getBid() * hedgePair2Info.getTick().getBid()) - purchasePairInfo.getTick().getBid()) + "<br/>");
			buffer.append("ARB 1: " + pf.format((hedgePair1Info.getTick().getBid() * hedgePair2Info.getTick().getBid()) / purchasePairInfo.getTick().getBid()) + "<br/>");
		}
	}

	private void generateTableHeader(StringBuffer buffer) {
		buffer.append("<table width=600>");
		if(showZone) {
			generateZoneInfoRow(buffer);
		} else {
			generateProfitRow(buffer);
			generateStepHeaderRow(buffer);
			generateStepBodyRow(buffer);
			generateRuleHeaderRow(buffer);
			generateRuleBodyRow(buffer);
		}
		buffer.append("</table>");
	}

	private void generateZoneInfoRow(StringBuffer buffer) {
		buffer.append("<tr>");
		generateZITargetsColumn(buffer);
		generateZIProfitColumn(buffer);
		buffer.append("</tr>");
		buffer.append("<tr>");
		generateZIMultsColumn(buffer);
		//generateZIProfitColumn(buffer);
		buffer.append("</tr>");
	}

	private void generateZITargetsColumn(StringBuffer buffer) {
		buffer.append("<td>");

		if(purchasePairInfo == null) {
			buffer.append("</td>");
			return;
		}
			
		double currentPrice = 0;
		if(ziEntryUnits > 0) {
			if(nextOrderHedge) {
				currentPrice = purchasePairInfo.getTick().getBid();
			} else {
				currentPrice = purchasePairInfo.getTick().getAsk();
			}
		} else {
			if(nextOrderHedge) {
				currentPrice = purchasePairInfo.getTick().getAsk();
			} else {
				currentPrice = purchasePairInfo.getTick().getBid();
			}
		}
		
		String lt = "Long Target: " + pf.format(longTargetPrice) + " Dist: " + getPriceStr((longTargetPrice - currentPrice) / purchasePair.getPIP(), pipf);
		sorted.put(pf.format(longTargetPrice), lt);
		
		String st = "Short Target: " + pf.format(shortTargetPrice) + " Dist: " + getPriceStr((shortTargetPrice - currentPrice) / purchasePair.getPIP(), pipf);
		sorted.put(pf.format(shortTargetPrice), st);
		
		String ep = "Entry Price: " + pf.format(positionPrice) + " Dist: " + getPriceStr((positionPrice - currentPrice) / purchasePair.getPIP(), pipf);
		sorted.put(pf.format(positionPrice), ep);
		
		String zp = "Zone Edge Price: " + pf.format(zoneEdgePrice) + " Dist: " + getPriceStr((zoneEdgePrice - currentPrice) / purchasePair.getPIP(), pipf);
		sorted.put(pf.format(zoneEdgePrice), zp);
		
		String cp = "Current Price: " + pf.format(currentPrice);
		sorted.put(pf.format(currentPrice), cp);
		
		for(String key: sorted.descendingKeySet()) {
			buffer.append(sorted.get(key) + "<br/>");
		}
		sorted.clear();
		buffer.append("</td>");
	}
	
	private void generateZIProfitColumn(StringBuffer buffer) {
		buffer.append("<td>");
		
		if(purchasePairInfo == null || hedgePair1Info == null || hedgePair2Info == null) {
			buffer.append("</td>");
			return;
		}
		
		buffer.append("Profit: " + getPriceStr(unrealizedPL, pf) + "<br/>");
		int i = -1;
		while(orders[++i]);
		
		if(nextOrderHedge) {
			buffer.append("Next Hedge Order " + i + ": " + getUnits(-(mults.get(i) * ziEntryUnits)) + "<br/>");
		} else {
			buffer.append("Next Regular Order " + i + ": " + getUnits((mults.get(i) * ziEntryUnits)) + "<br/>");
		}
		
		buffer.append("</td>");
	}
	
	private void generateZIMultsColumn(StringBuffer buffer) {
		buffer.append("<td>");
		
		for(int i = 0; i < mults.size(); i++) {
			if(orders[i]) {
				buffer.append("Mult[" + i + "]: " + pipf.format(mults.get(i)) + "<br/>");
			} else {
				if(i > 0 && orders[i - 1]) {
					if(nextOrderHedge) {
						buffer.append("Mult[" + i + "]: " + pipf.format(mults.get(i)) + " * " + ziEntryUnits + " = " + getUnits(-(mults.get(i) * ziEntryUnits)) + "<br/>");
					} else {
						buffer.append("Mult[" + i + "]: " + pipf.format(mults.get(i)) + " * " + ziEntryUnits + " = " + getUnits((mults.get(i) * ziEntryUnits)) + "<br/>");
					}
				} else {
					buffer.append("Mult[" + i + "]: " + pipf.format(mults.get(i)) + "<br/>");
				}
			}
			//if(i > 2 && orders[i - 3]) break;
		}
		
		buffer.append("</td>");
	}

	private void generateStepHeaderRow(StringBuffer buffer) {
		buffer.append("<tr>");
		buffer.append("<td>Step 1: " + getDirStr(step1) + "<br/>Master Direction<br/>240 Minute Chart</td>");
		buffer.append("<td>Step 2: " + getDirStr(step2) + "<br/>Trend Chart<br/>30 Minute Chart</td>");
		buffer.append("<td>Step 3: " + getDirStr(step3) + "<br/>Trigger Entry<br/>5 Tick Renko<br/>Heiken Ashi</td>");
		buffer.append("</tr>");
	}

	private void generateStepBodyRow(StringBuffer buffer) {
		buffer.append("<tr>");
		buffer.append("<td>Last HK Bar Dir: " + getDirStr(step1) + "</td>");

		buffer.append("<td>");
		buffer.append("EMA Slope: " + getDirStr(step2EMADirection) + "<br/>");
		buffer.append("Last HK Bar Dir: " + getDirStr(step2LastBarDirection) + "<br />");
		
		
		if(purchasePairInfo.getTick().getAsk() < step2EMA.getCloseBid()) {
			buffer.append("Price to EMA: " + getDirStr(Direction.DOWN) + "<br/>");
			buffer.append("EMA(20): " + pf.format(step2EMA.getCloseBid()) + "<br/>");
			buffer.append("Price: " + pf.format(purchasePairInfo.getTick().getAsk()) + "<br/>");
		} else if(purchasePairInfo.getTick().getBid() > step2EMA.getCloseAsk()) {
			buffer.append("Price to EMA: " + getDirStr(Direction.UP) + "<br/>");
			buffer.append("Price: " + pf.format(purchasePairInfo.getTick().getBid()) + "<br/>");
			buffer.append("EMA(20): " + pf.format(step2EMA.getCloseAsk()) + "<br/>");
		} else {
			buffer.append("Price to EMA: " + getDirStr(Direction.NONE) + "<br/>");
			buffer.append("Price Ask: " + pf.format(purchasePairInfo.getTick().getAsk()) + "<br/>");
			buffer.append("Price Bid: " + pf.format(purchasePairInfo.getTick().getBid()) + "<br/>");
			buffer.append("EMA(20) Ask: " + pf.format(step2EMA.getCloseAsk()) + "<br/>");
			buffer.append("EMA(20) Bid: " + pf.format(step2EMA.getCloseBid()) + "<br/>");
		}
		buffer.append("</td>");

		buffer.append("<td>");
		buffer.append("Last HK Bar Dir: " + getDirStr(step3LastBarDirection) + "<br/>");
		if(macd != null) {
			buffer.append("MACD(12, 26, 9): " + getPriceStr(((macd[2] + macd[5]) / 2), pf) + "<br/>");
		}
		buffer.append("</td>");
		buffer.append("</tr>");

	}

	private void generateRuleHeaderRow(StringBuffer buffer) {
		buffer.append("<tr>");

		buffer.append("<td>");
		if(positionUnits > 0) {
			buffer.append("Rule 1: " + getTFStr((purchasePairInfo.getTick().getBid() > positionPrice && rule1LastBarDirection == Direction.DOWN && rule1CurrentBarDirection == Direction.DOWN)) + "<br/>");
		} else if(positionUnits < 0) {
			buffer.append("Rule 1: " + getTFStr((purchasePairInfo.getTick().getAsk() < positionPrice && rule1LastBarDirection == Direction.UP && rule1CurrentBarDirection == Direction.UP)) + "<br/>");
		} else {
			buffer.append("Rule 1: No Entry<br/>");
		}
		buffer.append("Take Profit Exit<br/>");
		buffer.append("</td>");
		
		
		buffer.append("<td>");
		if(positionUnits > 0) {
			if(purchasePairInfo.getTick().getAsk() < aTRZoneEdgePrice && step1 == Direction.UP && step2 == Direction.UP && step3 == Direction.UP) {
				buffer.append("Rule 2: " + getTFStr(true) + "<br/>");
			} else {
				buffer.append("Rule 2: " + getTFStr(false) + "<br/>");
			}
		} else if(positionUnits < 0) {
			if(purchasePairInfo.getTick().getBid() > aTRZoneEdgePrice && step1 == Direction.DOWN && step2 == Direction.DOWN && step3 == Direction.DOWN) {
				buffer.append("Rule 2: " + getTFStr(true) + "<br/>");
			} else {
				buffer.append("Rule 2: " + getTFStr(false) + "<br/>");
			}
		} else {
			buffer.append("Rule 2: No Entry<br/>");
		}
		buffer.append("Cost Average<br/>Enter New Trade<br/>");
		buffer.append("</td>");
		
		buffer.append("<td>");
		if(positionUnits > 0) {
			if(purchasePairInfo.getTick().getAsk() < aTRZoneEdgePrice && (step1 == Direction.DOWN || step2 == Direction.DOWN)) {
				buffer.append("Rule 3: " + getTFStr(true) + "<br/>");
			} else {
				buffer.append("Rule 3: " + getTFStr(false) + "<br/>");
			}
		} else if(positionUnits < 0) {
			if(purchasePairInfo.getTick().getBid() > aTRZoneEdgePrice && (step1 == Direction.UP || step2 == Direction.UP)) {
				buffer.append("Rule 3: " + getTFStr(true) + "<br/>");
			} else {
				buffer.append("Rule 3: " + getTFStr(false) + "<br/>");
			}
		} else {
			buffer.append("Rule 3: No Entry<br/>");
		}
		buffer.append("Zone Recovery<br/>Trend Change<br/>");
		buffer.append("</tr>");
	}


	private void generateRuleBodyRow(StringBuffer buffer) {
		buffer.append("<tr>");

		buffer.append("<td>");
		if(positionUnits > 1) {
			buffer.append("In Profit: " + getPriceStr(unrealizedPL, pf) + "<br/>");
			if(unrealizedPL > 0) {
				buffer.append("Current Price: " + getPriceStr(purchasePairInfo.getTick().getBid(), pf) + "<br/>");
				buffer.append("Position Price: " + getPriceStr(positionPrice, pf) + "<br/>");
			} else {
				buffer.append("Position Price: " + getPriceStr(positionPrice, pf) + "<br/>");
				buffer.append("Current Price: " + getPriceStr(purchasePairInfo.getTick().getBid(), pf) + "<br/>");
			}
		} else if(positionUnits < 1) {
			buffer.append("In Profit: " + getPriceStr(unrealizedPL, pf) + "<br/>");
			if(unrealizedPL > 0) {
				buffer.append("Position Price: " + getPriceStr(positionPrice, pf) + "<br/>");
				buffer.append("Current Price: " + getPriceStr(purchasePairInfo.getTick().getAsk(), pf) + "<br/>");
			} else {
				buffer.append("Current Price: " + getPriceStr(purchasePairInfo.getTick().getAsk(), pf) + "<br/>");
				buffer.append("Position Price: " + getPriceStr(positionPrice, pf) + "<br/>");
			}
		} else {
			buffer.append("No Entry");
		}
		buffer.append("Last Bar Dir: " + getDirStr(rule1LastBarDirection) + "<br/>");
		buffer.append("Current Bar Dir: " + getDirStr(rule1CurrentBarDirection) + "<br/>");

		//buffer.append("Get Out Point: " + );
		buffer.append("</td>");


		buffer.append("<td>");
		System.out.println("Stats Zone Edge Price: " + aTRZoneEdgePrice);
		if(positionUnits > 0) {
			buffer.append("All Steps: " + getDirStr(Direction.UP) + "<br/>");
			buffer.append("Price Below Zone Edge</>");
			if(purchasePairInfo.getTick().getAsk() < aTRZoneEdgePrice) {
				buffer.append("Zone Edge: " + pf.format(aTRZoneEdgePrice) + "<br/>");
				buffer.append("Current Price: " + pf.format(purchasePairInfo.getTick().getAsk()) + "<br/>");
			} else {
				buffer.append("Current Price: " + pf.format(purchasePairInfo.getTick().getAsk()) + "<br/>");
				buffer.append("Zone Edge: " + pf.format(aTRZoneEdgePrice) + "<br/>");
			}
		} else if(positionUnits < 0) {
			buffer.append("All Steps: " + getDirStr(Direction.DOWN) + "<br/>");
			buffer.append("Price Above Zone Edge<br/>");
			if(purchasePairInfo.getTick().getBid() > aTRZoneEdgePrice) {
				buffer.append("Current Price: " + pf.format(purchasePairInfo.getTick().getBid()) + "<br/>");
				buffer.append("Zone Edge: " + pf.format(aTRZoneEdgePrice) + "<br/>");
			} else {
				buffer.append("Zone Edge: " + pf.format(aTRZoneEdgePrice) + "<br/>");
				buffer.append("Current Price: " + pf.format(purchasePairInfo.getTick().getBid()) + "<br/>");
			}
		} else {
			buffer.append("Rule 2: No Entry<br/>");
		}
		
		buffer.append("</td>");
		
		
		
		buffer.append("<td></td>");
		buffer.append("</tr>");
	}


	private void generateProfitRow(StringBuffer buffer) {
		buffer.append("<tr>");
		buffer.append("<td>");
		if(purchasePairInfo != null && purchasePair != null) {
			if(positionUnits > 0) {
				double profit = (purchasePairInfo.getTick().getBid() - positionPrice) / purchasePair.getPIP();
				buffer.append("Profit: " + getPriceStr(profit, pipf) + " pips<br/>");
				buffer.append("Profit: " + getPriceStr(unrealizedPL, pf));
			} else if(positionUnits < 0) {
				double profit = (positionPrice - purchasePairInfo.getTick().getAsk()) / purchasePair.getPIP();
				buffer.append("Profit: " + getPriceStr(profit, pipf) + " pips<br/>");
				buffer.append("Profit: " + getPriceStr(unrealizedPL, pf));
			} else {
				buffer.append("Profit: 0.0 pips<br/>");
				buffer.append("Profit: " + getPriceStr(unrealizedPL, pf));
			}
		}
		buffer.append("</td>");

		buffer.append("<td>ATR: " + (int)(ATRSize / purchasePair.getPIP()) + "</td>");
		buffer.append("<td>Zone Size: " + (int)(ATRSize / (purchasePair.getPIP() * aTRParts)) + "</td>");

		buffer.append("</tr>");
	}

	private String getDirStr(Direction dir) {
		if(dir == Direction.UP) {
			return "<font color=green>" + dir + "</font>";
		} else if(dir == Direction.DOWN) {
			return "<font color=red>" + dir + "</font>";
		} else {
			return "<font color=blue>" + dir + "</font>";
		}
	}

	private String getTFStr(boolean truth) {
		if(truth) {
			return "<font color=green>" + truth + "</font>";
		} else {
			return "<font color=red>" + truth + "</font>";
		}
	}

	private String getPriceStr(double price, DecimalFormat format) {
		if(price > 0) {
			return "<font color=green>" + format.format(price) + "</font>";
		} else if(price < 0) {
			return "<font color=red>" + format.format(price) + "</font>";
		} else {
			return format.format(price).toString();
		}
	}
	
	private String getUnits(double d) {
		if(d < 0) {
			return pipf.format(d) + " <font color=red>SHORT</font>";
		} else {
			return pipf.format(d) + " <font color=green>LONG</font>";
		}
	}

}
