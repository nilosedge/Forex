package net.nilosplace.Forex.client;

import org.apache.log4j.Logger;

import net.nilosplace.Forex.client.Oanda.OandaAccountEventHandlerInterface;
import net.nilosplace.Forex.client.Oanda.OandaRateEventHandlerInterface;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.AccountException;
import com.oanda.fxtrade.api.FXAccountEventInfo;
import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.FXTick;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.MarketOrder;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.RateTable;
import com.oanda.fxtrade.api.StopLossOrder;

public class AccountOrderManager implements OandaRateEventHandlerInterface, OandaAccountEventHandlerInterface {

	private Logger log = Logger.getLogger(getClass());
	
	protected Account account;
	protected RateTable rateTable;
	protected FXTick tick;
	private int trailingStop = 14;
	
	public AccountOrderManager(RateTable rateTable, Account account) {
		this.rateTable = rateTable;
		this.account = account;
	}

//	protected LimitOrder modifyOrderBuy(LimitOrder limit, int tpPips, double distPips) {
//		System.out.println("Updating Order: " + limit);
//		Instrument i = rateTable.get(tick.getPair().getPair());
//		TakeProfitOrder tpo = API.createTakeProfitOrder(tick.getAsk() + (i.getPIP() * tpPips));
//		limit.setPrice(tick.getAsk() + (i.getPIP() * distPips));
//		limit.setTakeProfit(tpo);
//		System.out.println("Updating Order: " + limit);
//		return limit;
//	}
//	
//	protected LimitOrder modifyOrderSell(LimitOrder limit, int tpPips, double distPips) {
//		System.out.println("Updating Order: " + limit);
//		Instrument i = rateTable.get(tick.getPair().getPair());
//		TakeProfitOrder tpo = API.createTakeProfitOrder(tick.getBid() - (i.getPIP() * tpPips));
//		limit.setPrice(tick.getBid() - (i.getPIP() * distPips));
//		limit.setTakeProfit(tpo);
//		System.out.println("Updating Order: " + limit);
//		return limit;
//	}
//
//	protected LimitOrder createOrderBuy(int tpPips, int units, double distPips, int tsPips) {
//		Instrument i = rateTable.get(tick.getPair().getPair());
//
//		TakeProfitOrder tpo = API.createTakeProfitOrder(tick.getAsk() + (i.getPIP() * tpPips));
//		LimitOrder limit = API.createLimitOrder();
//		limit.setUnits(units);
//		Date now = new Date();
//		limit.setExpiry((now.getTime() / 1000) + 7776000);
//		limit.setPair(i);
//		
//		limit.setHighPriceLimit(0);
//		limit.setLowPriceLimit(0);
//		
//		limit.setPrice(tick.getAsk() + (i.getPIP() * distPips));
//		limit.setTrailingStopLoss(tsPips);
//		//limit.setStopLoss(slo);
//		limit.setTakeProfit(tpo);
//		
//		return limit;
//		
//	}
//	
//	protected LimitOrder createOrderSell(int tpPips, int units, double distPips, int tsPips) {
//		Instrument i = rateTable.get(tick.getPair().getPair());
//
//		//TakeProfitOrder tpo = API.createTakeProfitOrder(tick.getBid() - (i.getPIP() * tpPips));
//		LimitOrder limit = API.createLimitOrder();
//		limit.setUnits(units);
//		Date now = new Date();
//		limit.setExpiry((now.getTime() / 1000) + 7776000);
//		limit.setPair(i);
//		
//		limit.setHighPriceLimit(0);
//		limit.setLowPriceLimit(0);
//		
//		limit.setPrice(tick.getBid() - (i.getPIP() * distPips));
//		limit.setTrailingStopLoss(tsPips);
//		//limit.setStopLoss(slo);
//		//limit.setTakeProfit(tpo);
//		
//		return limit;
//		
//	}
	
	@Override
	public void handle(FXAccountEventInfo event) {
		// TODO Auto-generated method stub
		//System.out.println("Event: " + event.getKey().toString());
		//System.out.println("Event: " + event.getTransaction());
		//System.out.println("Event: " + event.getClass());
		
	}

	@Override
	public void handle(FXRateEventInfo info) {
		tick = info.getTick();
		Instrument i = rateTable.get(tick.getPair().getPair());

		try {
			//System.out.println("Trade: " + account.getTrades());

			for(Object o: account.getTrades()) {
				MarketOrder m = (MarketOrder)o;
				if(m.getPair().equals(tick.getPair())) {
					log.info(tick);
					log.info(m);
					if(m.getUnits() > 0) {
						log.info("tick.getAsk(): " + tick.getAsk());
						log.info("i.getPIP(): " + i.getPIP());
						log.info("m.getPrice(): " + m.getPrice());
						log.info("m.getTrailingStopLoss(): " + m.getTrailingStopLoss());
						log.info("m.getStopLoss(): " + m.getStopLoss());
						if(
								m.getTrailingStopLoss() > 0 &&
								tick.getBid() > (m.getPrice() + (i.getPIP() * (m.getTrailingStopLoss() + 1))) &&
								tick.getBid() < (m.getPrice() + (i.getPIP() * trailingStop))
							) {
							// Modify to be a stop loss order
							log.info("Modify order for stop loss: " + (m.getPrice() + i.getPIP()));
							StopLossOrder slo = API.createStopLossOrder((m.getPrice() + i.getPIP()));
							m.setStopLoss(slo);
							m.setTrailingStopLoss(0);
							log.info("Modified: " + m);
							account.modify(m);
						} else if(m.getStopLoss().getPrice() > 0 && tick.getBid() > (m.getPrice() + (i.getPIP() * (trailingStop + 1)))) {
							// Modify to be a trailing stop again at 15 pips
							log.info("Modify order for trailing stop: 15 pips");
							StopLossOrder slo = API.createStopLossOrder(0);
							m.setStopLoss(slo);
							m.setTrailingStopLoss(trailingStop);
							log.info("Modified: " + m);
							account.modify(m);
						}
					} else {
						log.info("tick.getAsk(): " + tick.getAsk());
						log.info("i.getPIP(): " + i.getPIP());
						log.info("m.getPrice(): " + m.getPrice());
						log.info("m.getTrailingStopLoss(): " + m.getTrailingStopLoss());
						log.info("m.getStopLoss(): " + m.getStopLoss());
						if(
								m.getTrailingStopLoss() > 0 &&
								tick.getAsk() < (m.getPrice() - (i.getPIP() * (m.getTrailingStopLoss() + 1))) &&
								tick.getAsk() > (m.getPrice() - (i.getPIP() * trailingStop))
							) {
							// Modify to be a stop loss order
							log.info("Modify order for stop loss: " + (m.getPrice() - i.getPIP()));
							StopLossOrder slo = API.createStopLossOrder((m.getPrice() - i.getPIP()));
							m.setStopLoss(slo);
							m.setTrailingStopLoss(0);
							log.info("Modified: " + m);
							account.modify(m);
							
						} else if(m.getStopLoss().getPrice() > 0 && tick.getAsk() < (m.getPrice() - (i.getPIP() * (trailingStop + 1)))) {
							// Modify to be a trailing stop again at 15 pips
							log.info("Modify order for trailing stop: 15 pips");
							StopLossOrder slo = API.createStopLossOrder(0);
							m.setStopLoss(slo);
							m.setTrailingStopLoss(trailingStop);
							log.info("Modified: " + m);
							account.modify(m);
						}
					}
				}
				//log.info();
			}
			//log.info();
		} catch (AccountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OAException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}