package net.nilosplace.Forex.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import net.nilosplace.Forex.client.models.DataBar;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.data.xy.DefaultOHLCDataset;
import org.jfree.data.xy.OHLCDataItem;
import org.jfree.data.xy.OHLCDataset;

public class OHLCCandleChart {

	OHLCDataset dataset;
	OHLCDataItem[] data;
	JFreeChart chart;
	XYPlot plot;
	JFrame myFrame;
	String name;

	public OHLCCandleChart(String name, LinkedList<DataBar> bars) {
		this.name = name;
		List<OHLCDataItem> dataItems = new ArrayList<OHLCDataItem>();
		
		for(DataBar bar: bars) {
			dataItems.add(new OHLCDataItem(bar.getTime(), bar.getOpenAsk(), bar.getHighAsk(), bar.getLowAsk(), bar.getCloseAsk(), 0));
		}

		data = dataItems.toArray(new OHLCDataItem[dataItems.size()]);
		dataset = new DefaultOHLCDataset(name, data);		

		chart = ChartFactory.createCandlestickChart(name, "Time", "Price", dataset, false);
		
		chart.setBackgroundPaint(Color.white);

		plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.WHITE); // light yellow = new Color(0xffffe0)
		plot.setDomainGridlinesVisible(true);
		plot.setDomainGridlinePaint(Color.lightGray);
		plot.setRangeGridlinePaint(Color.lightGray);
		((NumberAxis) plot.getRangeAxis()).setAutoRangeIncludesZero(false);

		// 5. Skip week-ends on the date axis
		//((DateAxis) plot.getDomainAxis()).setTimeline(SegmentedTimeline.newMondayThroughFridayTimeline());
		((DateAxis) plot.getDomainAxis()).setAutoRange(true);
		
		

		// 6. No volume drawn
		((CandlestickRenderer) plot.getRenderer()).setDrawVolume(false);

		// 7. Create and display full-screen JFrame
		myFrame = new JFrame();
		myFrame.setResizable(true);
		myFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		myFrame.add(new ChartPanel(chart), BorderLayout.CENTER);
		Toolkit kit = Toolkit.getDefaultToolkit();
		Insets insets = kit.getScreenInsets(myFrame.getGraphicsConfiguration());
		Dimension screen = kit.getScreenSize();
		myFrame.setSize((int) (screen.getWidth() - insets.left - insets.right), (int) (screen.getHeight() - insets.top - insets.bottom));
		myFrame.setLocation((int) (insets.left), (int) (insets.top));
		//myFrame.setVisible(true);
	}
	
	public void setVisible(boolean visible) {
		myFrame.setVisible(visible);
	}
	public boolean isVisible() {
		return myFrame.isVisible();
	}

	public void update(LinkedList<DataBar> bars) {

		List<OHLCDataItem> dataItems = new ArrayList<OHLCDataItem>();
		for(DataBar bar: bars) {
			dataItems.add(new OHLCDataItem(bar.getTime(), bar.getOpenAsk(), bar.getHighAsk(), bar.getLowAsk(), bar.getCloseAsk(), 0));
		}

		data = dataItems.toArray(new OHLCDataItem[dataItems.size()]);
		dataset = new DefaultOHLCDataset(name, data);	
		
		chart.getXYPlot().setDataset(dataset);
		
		chart.fireChartChanged();
	}
	
	public String getName() {
		return name;
	}
}
