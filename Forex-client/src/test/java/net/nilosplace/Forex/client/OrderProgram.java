package net.nilosplace.Forex.client;

import java.util.Properties;
import java.util.Vector;

import net.nilosplace.Forex.client.Oanda.OandaAccountEvent;
import net.nilosplace.Forex.client.Oanda.OandaRateEvent;
import net.nilosplace.Forex.client.models.Program;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.FXClient;
import com.oanda.fxtrade.api.RateTable;
import com.oanda.fxtrade.api.User;

public class OrderProgram extends Program {

	FXClient fxclient;
	
	public OrderProgram(Properties programConfig) {
		super(programConfig);

		fxclient = API.createFXGame();
		fxclient.setWithRateThread(true);
		fxclient.setWithKeepAliveThread(true);
		//fxclient.setWithTrailingStopMonitor(true);

		try {

			fxclient.login(getUsername(), getPassword(), "Example2 Test");

			RateTable rateTable = fxclient.getRateTable();
			
			User me = fxclient.getUser();
			Vector<?> accounts = me.getAccounts();
			
			OandaAccountEvent accountEvent = new OandaAccountEvent();
			MainStrategyOrdermanager msom = null;
			//TestOpenCloseManager tocm = null;
			
			for(Object a: accounts) {
				Account account = (Account)a;
				System.out.println(account.getAccountName());
				if(account.getAccountName().equals("Primary")) {
					msom = new MainStrategyOrdermanager(rateTable, account, new Triad("EUR/JPY", "EUR/USD", "USD/JPY"), new StatsFrame());
					//msom = new MainStrategyOrdermanager(rateTable, account, new Triad("EUR/USD", "EUR/GBP", "GBP/USD"));
					//tocm = new TestOpenCloseManager(rateTable, account);
					accountEvent.registerHandler(msom);
					//accountEvent.registerHandler(tocm);
					account.getEventManager().add(accountEvent);
				}
			}
			
			OandaRateEvent rateEvent = new OandaRateEvent(programConfig);
			rateEvent.registerHandler(msom);
			//rateEvent.registerHandler(tocm);
			rateTable.getEventManager().add(rateEvent);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		

	}

}
