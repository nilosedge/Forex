package net.nilosplace.Forex.client;

public class ZoneInfo {

	protected double longTargetPrice;
	protected double shortTargetPrice;
	protected double zoneEdgePrice;

	public ZoneInfo(long entryUnits, double entryPrice, int gainPips, int zoneSize, double pipsize) {
		if(entryUnits > 0) {
			longTargetPrice = (entryPrice + (pipsize * (double)gainPips));
			zoneEdgePrice = entryPrice - (pipsize * (double)zoneSize);
			shortTargetPrice = (zoneEdgePrice - (pipsize * (double)gainPips));
		} else {
			shortTargetPrice = (entryPrice - (pipsize * (double)gainPips));
			zoneEdgePrice = entryPrice + (pipsize * (double)zoneSize);
			longTargetPrice = (zoneEdgePrice + (pipsize * (double)gainPips));
		}
	}
	
	public double getLongTargetPrice() {
		return longTargetPrice;
	}
	public void setLongTargetPrice(double longTargetPrice) {
		this.longTargetPrice = longTargetPrice;
	}
	public double getShortTargetPrice() {
		return shortTargetPrice;
	}
	public void setShortTargetPrice(double shortTargetPrice) {
		this.shortTargetPrice = shortTargetPrice;
	}
	public double getZoneEdgePrice() {
		return zoneEdgePrice;
	}
	public void setZoneEdgePrice(double zoneEdgePrice) {
		this.zoneEdgePrice = zoneEdgePrice;
	}
}
