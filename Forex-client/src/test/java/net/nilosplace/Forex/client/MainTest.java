package net.nilosplace.Forex.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Vector;

import net.nilosplace.Forex.client.enums.oanda.OandaSystemType;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.models.Program;

import org.apache.log4j.PropertyConfigurator;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.CandlePoint;
import com.oanda.fxtrade.api.FXClient;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.InvalidPasswordException;
import com.oanda.fxtrade.api.InvalidUserException;
import com.oanda.fxtrade.api.MultiFactorAuthenticationException;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.RateTable;
import com.oanda.fxtrade.api.SessionDisconnectedException;
import com.oanda.fxtrade.api.SessionException;

public class MainTest {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		//BasicConfigurator.configure();
		PropertyConfigurator.configure("log4j.properties");
		
		Properties programConfig = new Properties();
		if(args.length > 0) {
			programConfig.load(new FileInputStream(new File(args[0])));
		} else {
			programConfig.load(new FileInputStream(new File("config.properties")));
		}
		
		Program program = new OrderProgram(programConfig);
		program.start();
		
		
		
		/*
		BarHelper bh = new BarHelper();
		try {
			FXClient fxclient;
			fxclient = API.createFXGame();
			fxclient.setWithRateThread(true);
			fxclient.setWithKeepAliveThread(true);
			//fxclient.setWithTrailingStopMonitor(true);
	
			System.out.println(programConfig);
			OandaSystemType type = OandaSystemType.PRACTICE;
			
			try {
				programConfig.load(new FileInputStream(new File(programConfig.getProperty("oandaPropertiesFile"))));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
			
			fxclient.login(programConfig.getProperty(type.getUsername()), programConfig.getProperty(type.getPassword()), "Example2 Test");
	
			RateTable rateTable = fxclient.getRateTable();
			
			Instrument i = rateTable.getInstrument("EUR/USD");
			
			//Vector<FXHistoryPoint> history = rateTable.getHistory(i.getInverse().getInverse(), FXClient.INTERVAL_1_MIN, 5000);

			Vector<CandlePoint> history = rateTable.getCandles(i.getInverse().getInverse(), FXClient.INTERVAL_3_HOUR, 40);
			
			System.out.println("History: " + history.size());
			
			LinkedList<DataBar> bars = bh.convertCandleBars(history);
			System.out.println("Bars 01: " + bars.size());
			//bh.printBars(bars);
			double pipsize = i.getPIP();
			bars = bh.generateRenko(bars, pipsize, 5);
			//bh.printBars(bars);
			System.out.println("Bars 02: " + bars.size());
			bars = bh.generateHeikenAshi(bars, i.getPIP());
			bh.printBars(bars);
			System.out.println("Bars 03: " + bars.size());
			bars.removeLast();
			System.out.println("Bars 04: " + bars);
		} catch (SessionDisconnectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidPasswordException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SessionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MultiFactorAuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OAException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		
		
		
		
		
		
		
	}

}
