package net.nilosplace.Forex.client;

import java.util.Date;
import java.util.LinkedList;
import java.util.Vector;

import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.models.DataBar;
import net.nilosplace.Forex.client.util.CoreHelper;

import com.oanda.fxtrade.api.CandlePoint;
import com.oanda.fxtrade.api.FXHistoryPoint;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class BarHelper {
	
	public LinkedList<DataBar> convertCandleBars(Vector<CandlePoint> candles) {
		LinkedList<DataBar> bars = new LinkedList<DataBar>();
		for(CandlePoint candle: candles) {
			DataBar bar = new DataBar(candle);
			bars.add(bar);
		}
		return bars;
	}
	
	public LinkedList<DataBar> convertHistoryBars(Vector<FXHistoryPoint> candles) {
		LinkedList<DataBar> bars = new LinkedList<DataBar>();
		for(FXHistoryPoint candle: candles) {
			DataBar bar = new DataBar(candle);
			bars.add(bar);
		}
		return bars;
	}
	
	public LinkedList<DataBar> convertHourHistoryToDayBars(Vector<FXHistoryPoint> candles) {
		LinkedList<DataBar> bars = new LinkedList<DataBar>();
		DataBar currentBar = new DataBar(candles.get(0));
		bars.add(currentBar);
		for(FXHistoryPoint candle: candles) {
			Date barDate = new Date(candle.getTimestamp() * 1000);
			if(barDate.getHours() == 17 && barDate.getMinutes() == 0) {
				DataBar newBar = new DataBar(candle);
				newBar.setOpenAsk(currentBar.getCloseAsk());
				newBar.setOpenBid(currentBar.getCloseBid());
				currentBar = newBar;
				bars.add(currentBar);
			} else {
				currentBar.updateCandle(candle);
			}
		}
		return bars;
	}

	public LinkedList<DataBar> convertHourHistoryTo4HourBars(Vector<FXHistoryPoint> candles) {
		LinkedList<DataBar> bars = new LinkedList<DataBar>();
		DataBar currentBar = new DataBar(candles.get(0));
		bars.add(currentBar);
		for(FXHistoryPoint candle: candles) {
			Date barDate = new Date(candle.getTimestamp() * 1000);
			if((barDate.getHours() - 1) % 4 == 0 && barDate.getMinutes() == 0) {
				DataBar newBar = new DataBar(candle);
				newBar.setOpenAsk(currentBar.getCloseAsk());
				newBar.setOpenBid(currentBar.getCloseBid());
				currentBar = newBar;
				bars.add(currentBar);
			} else {
				currentBar.updateCandle(candle);
			}
		}
		return bars;
	}	
	
	public LinkedList<DataBar> convertHourCandlesTo4HourBars(Vector<CandlePoint> candles) {
		LinkedList<DataBar> bars = new LinkedList<DataBar>();
		DataBar currentBar = new DataBar(candles.get(0));
		bars.add(currentBar);
		for(CandlePoint candle: candles) {
			Date barDate = new Date(candle.getTimestamp() * 1000);
			if((barDate.getHours() - 1) % 4 == 0 && barDate.getMinutes() == 0) {
				DataBar newBar = new DataBar(candle);
				newBar.setOpenAsk(currentBar.getCloseAsk());
				newBar.setOpenBid(currentBar.getCloseBid());
				currentBar = newBar;
				bars.add(currentBar);
			} else {
				currentBar.updateCandle(candle);
			}
		}
		return bars;
	}
	
	public LinkedList<DataBar> generateHeikenAshi(LinkedList<DataBar> bars, double pipsize) {
		LinkedList<DataBar> queue = new LinkedList<DataBar>();
		
		DataBar first = bars.removeFirst();
		DataBar firstbar = new DataBar(first);
		queue.add(firstbar);
		for(DataBar b: bars) {
			double newOpenAsk = ((queue.peekLast().getOpenAsk() + queue.peekLast().getCloseAsk()) / 2);
			double newOpenBid = ((queue.peekLast().getOpenBid() + queue.peekLast().getCloseBid()) / 2);
			
			double newCloseAsk = (b.getOpenAsk() + b.getHighAsk() + b.getLowAsk() + b.getCloseAsk())/4;
			double newCloseBid = (b.getOpenBid() + b.getHighBid() + b.getLowBid() + b.getCloseBid())/4;
			
			DataBar curBar = new DataBar();
			curBar.setOpenAsk(newOpenAsk);
			curBar.setOpenBid(newOpenBid);
			
			curBar.setHighAsk(Math.max(Math.max(b.getHighAsk(), newOpenAsk), newCloseAsk));
			curBar.setHighBid(Math.max(Math.max(b.getHighBid(), newOpenBid), newCloseBid));
			
			curBar.setLowAsk(Math.min(Math.min(b.getLowAsk(), newOpenAsk), newCloseAsk));
			curBar.setLowBid(Math.min(Math.min(b.getLowBid(), newOpenBid), newCloseBid));
			
			curBar.setCloseAsk(newCloseAsk);
			curBar.setCloseBid(newCloseBid);
			curBar.setTime(b.getTime());
			curBar.setPipSize(pipsize);
			
			queue.add(curBar);
		}
		return queue;
	}
	
	
	//Traditional – Uses a user-pre-defined absolute value for brick size. New bricks are only created when price movement 
    //is at least as large as the pre-determined brick size. The upside to this method is that it is very straightforward and 
    //it is easy to anticipate when and where new bricks will form. The downside is that selecting the correct brick size for 
    //a specific instrument will take some experimentation. Typically, you will want to select a brick size that is 
    //about 1/20th of the current value of the instrument.
	
	public LinkedList<DataBar> generateRenko(LinkedList<DataBar> bars, double pipsize, int barsize) {
		
		LinkedList<DataBar> queue = new LinkedList<DataBar>();
		
		double highAsk, lowAsk, highBid, lowBid;
		
		for(DataBar bar: bars) {
			//System.out.println("Bar: " + bar);
			if(queue.size() == 0) {
				
				int fullPrice = (int)Math.ceil(bar.getOpenAsk() * (1 / pipsize));
				int rem = fullPrice % barsize;
				lowAsk = (fullPrice - rem) * pipsize;
				highAsk = (lowAsk + (barsize * pipsize));
				
				fullPrice = (int)Math.ceil(bar.getOpenBid() * (1 / pipsize));
				rem = fullPrice % barsize;
				lowBid = (fullPrice - rem) * pipsize;;
				highBid = (lowBid + (barsize * pipsize));

				//highBid = (bar.getOpenBid() + ((barsize / 2) * pipsize));
				//lowBid = (bar.getOpenBid() - ((barsize / 2) * pipsize));
				
				if(bar.getOpenAsk() < bar.getCloseAsk()) {
					DataBar newBar = new DataBar(lowAsk, highAsk, lowAsk, highAsk, lowBid, highBid, lowBid, highBid, pipsize);
					newBar.setTime(bar.getTime());
					queue.add(newBar);
					//System.out.println("First Bar: " + queue.peekLast());
				} else {
					DataBar newBar = new DataBar(highAsk, highAsk, lowAsk, lowAsk, highBid, highBid, lowBid, lowBid, pipsize);
					newBar.setTime(bar.getTime());
					queue.add(newBar);
					//System.out.println("First Bar: " + queue.peekLast());
				}
			}
			
			//System.out.println("Adding up Bars: ");
			if(bar.getCloseBid() > queue.peekLast().getHighAsk()) {
				while(bar.getCloseAsk() > queue.peekLast().getHighAsk() + (pipsize * barsize)) {
					double newHighAsk = queue.peekLast().getHighAsk() + (pipsize * barsize);
					double newLowAsk = queue.peekLast().getHighAsk();
					
					double newHighBid = queue.peekLast().getHighBid() + (pipsize * barsize);
					double newLowBid = queue.peekLast().getHighBid();
					DataBar newBar = new DataBar(newLowAsk, newHighAsk, newLowAsk, newHighAsk, newLowBid, newHighBid, newLowBid, newHighBid, pipsize);
					newBar.setTime(bar.getTime());
					queue.add(newBar);
					//System.out.println("Added Up Bar: " + queue.peekLast());
				}
			}
			//System.out.println("Adding down Bars: ");
			if(bar.getCloseAsk() < queue.peekLast().getLowAsk()) {
				while(bar.getCloseBid() < queue.peekLast().getLowAsk() - (pipsize * barsize)) {
					
					double newHighAsk = queue.peekLast().getLowAsk();
					double newLowAsk = queue.peekLast().getLowAsk() - (pipsize * barsize);
					double newHighBid = queue.peekLast().getLowBid();
					double newLowBid = queue.peekLast().getLowBid() - (pipsize * barsize);
					DataBar newBar = new DataBar(newHighAsk, newHighAsk, newLowAsk, newLowAsk, newHighBid, newHighBid, newLowBid, newLowBid, pipsize);
					newBar.setTime(bar.getTime());
					queue.add(newBar);
					//System.out.println("Added Down Bar: " + queue.peekLast());
				}
			}

		}
		return queue;
	}
	
	public DataBar getEma(LinkedList<DataBar> bars) throws TALibDataException {
		return getEma(bars, bars.size());
	}

	public DataBar getEma(LinkedList<DataBar> bars, int window) throws TALibDataException {
		if(window >= bars.size()) window = bars.size() - 1;
		
		double[] pricesCloseAsk = new double[bars.size()];
		double[] pricesCloseBid = new double[bars.size()];
		
		int ii = 0;
		for(DataBar bar: bars) {
			pricesCloseAsk[ii] = bar.getCloseAsk();
			pricesCloseBid[ii] = bar.getCloseBid();
			ii++;
		}
		DataBar b = new DataBar();
		b.setCloseAsk(CoreHelper.ema(pricesCloseAsk, window));
		b.setCloseBid(CoreHelper.ema(pricesCloseBid, window));
		return b;
	}
	
	public DataBar getAtr(LinkedList<DataBar> bars, int window) throws TALibDataException {
		if(window >= bars.size()) window = bars.size() - 1;
		
		double[] highsAsk = new double[bars.size()];
		double[] highsBid = new double[bars.size()];
		double[] lowsAsk = new double[bars.size()];
		double[] lowsBid = new double[bars.size()];
		double[] closeAsk = new double[bars.size()];
		double[] closeBid = new double[bars.size()];
		
		int ii = 0;
		for(DataBar bar: bars) {
			highsAsk[ii] = bar.getCloseAsk();
			highsBid[ii] = bar.getCloseBid();
			lowsAsk[ii] = bar.getLowAsk();
			lowsBid[ii] = bar.getLowBid();
			closeAsk[ii] = bar.getCloseAsk();
			closeBid[ii] = bar.getCloseBid();
			ii++;
		}
		DataBar b = new DataBar();
		b.setCloseAsk(CoreHelper.atr(highsAsk, lowsAsk, closeAsk, window));
		b.setCloseBid(CoreHelper.atr(highsBid, lowsBid, closeBid, window));
		return b;
	}
	
	public Direction emaDir(LinkedList<DataBar> bars) throws TALibDataException {
		return emaDir(bars, bars.size());
	}
	
	public Direction emaDir(LinkedList<DataBar> bars, int window) throws TALibDataException {
		if(window >= bars.size()) window = bars.size() - 1;

		double[] prices = new double[bars.size()];
		for(int ii = 0; ii < bars.size(); ii++) {
			prices[ii] = bars.get(ii).getCloseBid();
		}
		
		double[] out = new double[prices.length];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		RetCode retCode = c.ema(0, prices.length - 1, prices, window, begin, length, out);
		if (retCode == RetCode.Success && out.length > 2) {
			if(out[length.value - 1] > out[length.value - 2]) {
				return Direction.UP;
			} else {
				return Direction.DOWN;
			}
		}
		return Direction.NONE;
	}
	
	public double[] getMACD(LinkedList<DataBar> bars, int fast, int slow, int signal) throws TALibDataException {
		double macdAsk[] = new double[3];
		double macdBid[] = new double[3];
		double ret[] = new double[6];

		double[] pricesBid = new double[bars.size()];
		double[] pricesAsk = new double[bars.size()];
		
		for(int ii = 0; ii < bars.size(); ii++) {
			pricesBid[ii] = bars.get(ii).getCloseBid();
			pricesAsk[ii] = bars.get(ii).getCloseAsk();
		}
		
		macdAsk = CoreHelper.macd(pricesAsk, fast, slow, signal);
		macdBid = CoreHelper.macd(pricesBid, fast, slow, signal);
		ret[0] = macdAsk[0];
		ret[1] = macdAsk[1];
		ret[2] = macdAsk[2];
		ret[3] = macdBid[0];
		ret[4] = macdBid[1];
		ret[5] = macdBid[2];
		
		return ret;
	}

	
	public void printBars(LinkedList<DataBar> bars) {
		for(DataBar b: bars) {
			System.out.println(b.cvsString());
		}
		System.out.println();
	}
	
}
