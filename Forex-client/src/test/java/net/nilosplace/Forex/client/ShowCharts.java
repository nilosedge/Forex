package net.nilosplace.Forex.client;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ShowCharts extends JFrame {

	LinkedHashMap<String, OHLCCandleChart> charts = new LinkedHashMap<String, OHLCCandleChart>();
	HashMap<String, JButton> buttons = new HashMap<String, JButton>();
	
	public ShowCharts() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setLayout(new FlowLayout());
		setSize(400, 350);
		
		JButton hideAll = new JButton("Hide All");
		hideAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(OHLCCandleChart c: charts.values()) {
					c.setVisible(false);
				}
			}
		});
		add(hideAll);
		setVisible(true);
	}
	
	public void registerChart(OHLCCandleChart chart) {
		charts.put(chart.getName(), chart);
		JButton button = new JButton("Show: " + chart.getName());
		button.setName(chart.getName());
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton button = (JButton)e.getSource();
				System.out.println(button.getName());
				if(charts.get(button.getName()).isVisible()) {
					charts.get(button.getName()).setVisible(false);
					button.setText("Show: " + button.getName());
				} else {
					charts.get(button.getName()).setVisible(true);
					button.setText("Hide: " + button.getName());
				}
			}
		});
		buttons.put(chart.getName(), button);
		add(button);
		repaint();
	}
	
	

}
