package net.nilosplace.Forex.client;

import net.nilosplace.Forex.client.Oanda.OandaAccountEventHandlerInterface;
import net.nilosplace.Forex.client.Oanda.OandaRateEventHandlerInterface;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.FXAccountEventInfo;
import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.MarketOrder;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.RateTable;

public class NewAccountOrderManager implements OandaRateEventHandlerInterface, OandaAccountEventHandlerInterface {

	protected Account account;
	protected RateTable rateTable;
	protected boolean order;
	
	protected double eur_usd_price = 0;
	protected double gbp_usd_price = 0;
	protected double eur_gbp_price = 0;
	
	public NewAccountOrderManager(RateTable rateTable, Account account) {
		this.rateTable = rateTable;
		this.account = account;
	}

	@Override
	public void handle(FXAccountEventInfo event) {
		
		
	}

	@Override
	public void handle(FXRateEventInfo info) {
		System.out.println("Info: " + info.getTick());
		
		if(info.getPair().getPair().equals("EUR/USD")) {
			eur_usd_price = info.getTick().getAsk();
		}
		if(info.getPair().getPair().equals("GBP/USD")) {
			gbp_usd_price = info.getTick().getAsk();
		}
		if(info.getPair().getPair().equals("EUR/GBP")) {
			eur_gbp_price = info.getTick().getAsk();
		}
		

		if(!order && eur_usd_price > 0 && gbp_usd_price > 0 && eur_gbp_price > 0) {
			order = true;
			
			Instrument eur_usd = rateTable.get("EUR/USD");
			Instrument gbp_usd = rateTable.get("GBP/USD");
			Instrument eur_gbp = rateTable.get("EUR/GBP");
			
			MarketOrder m1 = API.createMarketOrder();
			m1.setPair(eur_usd);
			m1.setUnits(1000);

			MarketOrder m2 = API.createMarketOrder();
			m2.setPair(eur_gbp);
			m2.setUnits(-1000*2);
			
			MarketOrder m3 = API.createMarketOrder();
			m3.setPair(gbp_usd);
			m3.setUnits((long)(-1000*2*eur_gbp_price));
			
			try {
				account.execute(m1);
				account.execute(m2);
				account.execute(m3);
			} catch (OAException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
