package net.nilosplace.Forex.client;

public class Triad {

	private String primaryCurrency;
	private String hedgeCurrency1;
	private String hedgeCurrency2;
	
	public Triad(String primaryCurrency, String hedgeCurrency1, String hedgeCurrency2) {
		this.primaryCurrency = primaryCurrency;
		this.hedgeCurrency1 = hedgeCurrency1;
		this.hedgeCurrency2 = hedgeCurrency2;
	}
	public String getPrimaryCurrency() {
		return primaryCurrency;
	}
	public void setPrimaryCurrency(String primaryCurrency) {
		this.primaryCurrency = primaryCurrency;
	}
	public String getHedgeCurrency1() {
		return hedgeCurrency1;
	}
	public void setHedgeCurrency1(String hedgeCurrency1) {
		this.hedgeCurrency1 = hedgeCurrency1;
	}
	public String getHedgeCurrency2() {
		return hedgeCurrency2;
	}
	public void setHedgeCurrency2(String hedgeCurrency2) {
		this.hedgeCurrency2 = hedgeCurrency2;
	}
	
}
