package net.nilosplace.Forex.client;

import java.util.ArrayList;
import java.util.Vector;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.AccountException;
import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.MarketOrder;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.RateTable;

public class ZoneRecoveryOrderManager {

	protected Account account;
	protected RateTable rateTable;
	
	//protected double longTargetPrice;
	//protected double shortTargetPrice;
	protected double entryPrice = 0;
	protected double entryUnits = 0;
	//protected double zoneEdgePrice;
	
	protected ArrayList<Double> mults;
	protected boolean[] orders;
	
	protected ZoneInfo zi;
	protected long gainPips = 0;
	protected long zoneSize = 0;
	
	
	protected double currentPairPriceAsk = 0;
	protected double currentPairPriceBid = 0;
	protected double currentHedge1Price = 0;
	protected double currentHedge2Price = 0;
	
	protected Instrument purchasePair;
	protected Instrument hedgePairA;
	protected Instrument hedgePairB;
	private StatsFrame frame;

	public ZoneRecoveryOrderManager(RateTable rateTable, Account account, Triad tri, long entryUnits, double entryPrice, int gainPips, int zoneSize, int numberoftrades, StatsFrame frame) {
		this.rateTable = rateTable;
		this.account = account;
		this.entryUnits = entryUnits;
		this.entryPrice = entryPrice;
		this.gainPips = gainPips;
		this.zoneSize = zoneSize;
		this.frame = frame;
		purchasePair = rateTable.get(tri.getPrimaryCurrency());
		hedgePairA = rateTable.get(tri.getHedgeCurrency1());
		hedgePairB = rateTable.get(tri.getHedgeCurrency2());
		
		zi = new ZoneInfo(entryUnits, entryPrice, gainPips, zoneSize, purchasePair.getPIP());
		
		mults = SequenceCalculator.calcMultipliers(gainPips, zoneSize, 10, 1.5);
		
		orders = new boolean[mults.size()];
		
		int offset = (int)(Math.ceil((double)numberoftrades / 1.5) - 1);
		// I less then offset so numberoftrades = 1 will not turn on any orders
		for(int i = 0; i < offset; i++) {
			orders[i] = true;
		}
		
		frame.setZoneInfoNumberOfTrades(numberoftrades);
		frame.setZoneInfoGainPips(gainPips);
		frame.setZoneInfoZoneSize(zoneSize);
		
		frame.setZoneInfoEntryUnits(entryUnits);
		frame.setZoneInfoEntryPrice(entryPrice);
		
		frame.setZoneInfoZoneEdgePrice(zi.getZoneEdgePrice());
		frame.setZoneInfoLongTargetPrice(zi.getLongTargetPrice());
		frame.setZoneInfoShortTargetPrice(zi.getShortTargetPrice());
		frame.setZoneInfoMults(mults);
		frame.setZoneInfoOrders(orders);
		frame.showZoneRecovery(true);
	}

	public void rateInfo(FXRateEventInfo info) {
		updatePrices(info);
		if(!purchasePair.getPair().equals(info.getPair().getPair()) || !havePrices()) return;

		try {

			for(int i = 0; i < mults.size(); i++) {
				boolean hedge = i % 2 == 0;
				if(orders[i] == false) {
					frame.setZoneInfoNextOrderHedge(hedge);
					if(hedgeCondition(hedge)) {
						if(hedge) {
							hedgeOrder(info, mults.get(i));
						} else {
							regOrder(info, mults.get(i));
						}
						orders[i] = true;
					}
					break;
				}
			}
			System.out.println("Profit: " + account.getUnrealizedPL());
			//if(currentPairPriceBid > zi.getLongTargetPrice() || currentPairPriceAsk < zi.getShortTargetPrice()) {
			if(account.getUnrealizedPL() > 0) {
				account.closeAllMarketOrders();
				return;
			}
			frame.updateStats();
		} catch (OAException e) {
			e.printStackTrace();
		}
	}

	private boolean hedgeCondition(boolean hedge) {
		if(hedge) {
			if(entryUnits > 0) {
				//System.out.println(currentPairPriceBid + " < " + zoneEdgePrice);
				return currentPairPriceBid < zi.getZoneEdgePrice();
			} else {
				//System.out.println(currentPairPriceAsk + " > " + zoneEdgePrice);
				return currentPairPriceAsk > zi.getZoneEdgePrice();
			}
		} else {
			if(entryUnits > 0) {
				//System.out.println(currentPairPriceAsk + " > " + entryPrice);
				return currentPairPriceAsk > entryPrice;
			} else {
				//System.out.println(currentPairPriceBid + " < " + entryPrice);
				return currentPairPriceBid < entryPrice;
			}
		}
	}

	private boolean havePrices() {
		return (currentHedge1Price > 0 && currentHedge2Price > 0 && currentPairPriceAsk > 0 && currentPairPriceBid > 0);
	}

	private void updatePrices(FXRateEventInfo info) {
		if(info.getPair().getPair().equals(purchasePair.getPair())) {
			currentPairPriceAsk = info.getTick().getAsk();
			currentPairPriceBid = info.getTick().getBid();
		}
		if(info.getPair().getPair().equals(hedgePairA.getPair())) {
			if(entryUnits > 0) {
				currentHedge1Price = info.getTick().getBid();
			} else {
				currentHedge1Price = info.getTick().getAsk();
			}
		}
		if(info.getPair().getPair().equals(hedgePairB.getPair())) {
			if(entryUnits > 0) {
				currentHedge2Price = info.getTick().getBid();
			} else {
				currentHedge2Price = info.getTick().getAsk();
			}
		}
	}
	
	//EUR/USD long 10,000: EUR + 10,000, USD -14,169. Take the USD and impute the amount of GBP as follows:
	//GBP/USD short 14,169 / 1.60655 = short 8,820 (rounded) GBP which comes out to 8820 x 1.60655 = 14,170 USD long (rounded).
	//EUR/GBP short sell 10,000 to offset the existing 10,000 long EUR which comes out in GBP as 10,000 * .8821 = 8821 GBP long

	private void hedgeOrder(FXRateEventInfo info, double mult) throws OAException {
		System.out.println("Creating Hedge Order: " + hedgePairA + " -> " + hedgePairB + " M: " + mult);
		MarketOrder hedge1 = createOrder(hedgePairA, (long)(-entryUnits*mult));
		MarketOrder hedge2 = createOrder(hedgePairB, (long)(-entryUnits*currentHedge1Price*mult));
		account.execute(hedge1);
		account.execute(hedge2);
	}

	private void regOrder(FXRateEventInfo info, double mult) throws OAException {
		System.out.println("Creating Regular Order: " + purchasePair + " M: " + mult);
		MarketOrder regularOrder = createOrder(purchasePair, (long)(entryUnits * mult));
		account.execute(regularOrder);
	}

	private MarketOrder createOrder(Instrument purchasePair, long amount) {
		MarketOrder m = API.createMarketOrder();
		m.setPair(purchasePair);
		m.setUnits(amount);
		return m;
	}
}
