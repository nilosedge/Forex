package net.nilosplace.Forex.client;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.Vector;

import net.nilosplace.Forex.client.Oanda.OandaAccountEventHandlerInterface;
import net.nilosplace.Forex.client.Oanda.OandaRateEventHandlerInterface;
import net.nilosplace.Forex.client.enums.Direction;
import net.nilosplace.Forex.client.exceptions.TALibDataException;
import net.nilosplace.Forex.client.models.DataBar;

import com.oanda.fxtrade.api.API;
import com.oanda.fxtrade.api.Account;
import com.oanda.fxtrade.api.AccountException;
import com.oanda.fxtrade.api.FXAccountEventInfo;
import com.oanda.fxtrade.api.FXClient;
import com.oanda.fxtrade.api.FXHistoryPoint;
import com.oanda.fxtrade.api.FXPair;
import com.oanda.fxtrade.api.FXRateEventInfo;
import com.oanda.fxtrade.api.Instrument;
import com.oanda.fxtrade.api.MarketOrder;
import com.oanda.fxtrade.api.OAException;
import com.oanda.fxtrade.api.Position;
import com.oanda.fxtrade.api.RateTable;

public class MainStrategyOrdermanager implements OandaRateEventHandlerInterface, OandaAccountEventHandlerInterface {

	protected Account account;
	protected RateTable rateTable;
	protected ZoneRecoveryOrderManager zrom;

	protected int entryCount = 0;
	protected int entryCountMax = 5;
	protected double positionPrice;
	protected long positionUnits;
	//protected Position entryPosition;
	
	protected Instrument purchasePair;
	protected Instrument hedgePairA;
	protected Instrument hedgePairB;

	private OHLCCandleChart liveChartRaw;
	private OHLCCandleChart liveChartRenko;
	private OHLCCandleChart liveChartHK;
	
	private OHLCCandleChart step1Chart;
	private OHLCCandleChart step2ChartRaw;
	private OHLCCandleChart step2ChartHK;
	
	private OHLCCandleChart step3ChartRaw;
	private OHLCCandleChart step3ChartRenko;
	private OHLCCandleChart step3ChartHK;
	
	private OHLCCandleChart rule1ChartRaw;
	private OHLCCandleChart rule1ChartRenko;
	private OHLCCandleChart rule1ChartHK;

	// Should be the account balance / 5
	protected long oneFifthUnits = 2000;
	protected int ATRZoneSize = 0;
	protected double ATRZoneEdgePrice = 0;
	protected int ATRParts = 7;
	protected int renkoBarSize = 5;
	protected double profitMult = 2.75;
	
	protected BarHelper bh = new BarHelper();
	protected DecimalFormat df = new DecimalFormat("#.00000");
	protected Triad triad;
	
	private Direction step1;
	private Direction step2;
	private Direction step3;
	
	private StatsFrame frame;
	
	private ShowCharts chartsControl = new ShowCharts();
	
	public MainStrategyOrdermanager(RateTable rateTable, Account account, Triad triad, StatsFrame frame) {
		this.rateTable = rateTable;
		this.account = account;
		this.triad = triad;
		this.frame = frame;

		purchasePair = rateTable.get(triad.getPrimaryCurrency());
		hedgePairA = rateTable.get(triad.getHedgeCurrency1());
		hedgePairB = rateTable.get(triad.getHedgeCurrency2());
		
		frame.setPurchasePair(purchasePair);
		frame.setHedgePair1(hedgePairA);
		frame.setHedgePair2(hedgePairB);
		
		// Setup Zone Recovery if trades are in place.
		
		try {
			updatePosition(purchasePair.getInverse().getInverse());
			Vector<MarketOrder> ts = account.getTrades();
			int numberOfTrades = ts.size();

			Position purchasePairPosition = account.getPosition(purchasePair.getInverse().getInverse());
			
			if(purchasePairPosition != null) {
				positionUnits = purchasePairPosition.getUnits();
				positionPrice = purchasePairPosition.getPrice();
				
				Position hedgePairAPostition = account.getPosition(hedgePairA.getInverse().getInverse());
				Position hedgePairBPostition = account.getPosition(hedgePairB.getInverse().getInverse());
				
				if(hedgePairAPostition != null && hedgePairBPostition != null) {
					if(positionUnits > 0) {
						double longPrice = positionPrice;
						double shortPrice = hedgePairAPostition.getPrice() * hedgePairBPostition.getPrice();
						int zoneSize = (int)((longPrice - shortPrice) / purchasePair.getPIP());
						Vector<MarketOrder> orders = account.getTrades();
						long firstEntryUnits = orders.get(0).getUnits();
						zrom = new ZoneRecoveryOrderManager(rateTable, account, triad, firstEntryUnits, positionPrice, (int)(zoneSize * profitMult), zoneSize, numberOfTrades, frame);
					} else {
						double shortPrice = positionPrice;
						double longPrice = hedgePairAPostition.getPrice() * hedgePairBPostition.getPrice();
						int zoneSize = (int)((longPrice - shortPrice) / purchasePair.getPIP());
						Vector<MarketOrder> orders = account.getTrades();
						long firstEntryUnits = orders.get(0).getUnits();
						zrom = new ZoneRecoveryOrderManager(rateTable, account, triad, firstEntryUnits, positionPrice, (int)(zoneSize * profitMult), zoneSize, numberOfTrades, frame);
					}
				} else {
					// Let the code handle this later on once we have prices for all currencies involved
//					updateAtrZoneSize(purchasePair.getInverse().getInverse());
//					int zoneSize = 0;
//					
//					if(positionUnits > 0 && purchasePair.getAsk() < positionPrice) {
//						zoneSize = (int)((positionPrice - purchasePair.getAsk()) / purchasePair.getPIP());
//					} else if(positionUnits < 0 && purchasePair.getBid() > positionPrice) {
//						zoneSize = (int)((purchasePair.getBid() - positionPrice) / purchasePair.getPIP());
//					}
//					
//					if(zoneSize >= ATRZoneSize) {
//						//zrom = new ZoneRecoveryOrderManager(rateTable, account, triad, positionUnits, positionPrice, (int)(zoneSize * profitMult), zoneSize, numberOfTrades, frame);
//					} else {
//						System.out.println("Zone Size not big enough to worry about yet: " + zoneSize);
//					}
				}
			}
			
		} catch (AccountException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handle(FXAccountEventInfo event) {
		// If Close all orders
		//    entryOrder = Null
		//    if in ZR
		//       switch from zone recover to normal
		if(event.getTransaction().getType().equalsIgnoreCase("CloseTradeS") || event.getTransaction().getType().equalsIgnoreCase("CloseTradeB")) {
			zrom = null;
			frame.showZoneRecovery(false);
		}
		
		try {
			updatePosition(purchasePair.getInverse().getInverse());
		} catch (AccountException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void handle(FXRateEventInfo info) {

		if(info.getPair().equals(purchasePair)) {
			frame.updatePurchasePair(info);
		}
		if(info.getPair().equals(hedgePairA)) {
			frame.updateHedgePair1(info);
		}
		if(info.getPair().equals(hedgePairB)) {
			frame.updateHedgePair2(info);
		}
	
		try {
			
			frame.updateUnrealizedPL(account.getUnrealizedPL());
			
			if(purchasePair.getPair().equals(info.getPair().getPair())) {
				step1 = getStep1MasterDirection(info);
				step2 = getStep2TrendDirection(info);
				step3 = getStep3EntryTrigger(info);
				updateLiveChart(info);
				updateAtrZoneSize(info.getPair());
				frame.updateSteps(step1, step2, step3);
			}

			if(zrom != null) {
				zrom.rateInfo(info);
			} else {
				if(!purchasePair.getPair().equals(info.getPair().getPair())) return;

				if(positionUnits == 0) {
					// do we get in?
					if(step1 == Direction.UP && step2 == Direction.UP && step3 == Direction.UP) {
						MarketOrder entryOrder = createOrder(purchasePair, oneFifthUnits);
						account.execute(entryOrder);
						entryCount++;
						System.out.println("No Current Orders creating new LONG Order: " + entryOrder + " Position: " + positionUnits);
					} else if(step1 == Direction.DOWN && step2 == Direction.DOWN && step3 == Direction.DOWN) {
						MarketOrder entryOrder = createOrder(purchasePair, -oneFifthUnits);
						account.execute(entryOrder);
						entryCount++;
						System.out.println("No Current Orders creating new SHORT Order: " + entryOrder + " Position: " + positionUnits);
					}
				} else {

					if(rule1(info)) {
						System.out.println("Exiting the Market with: Enter:" + positionPrice + " Exit: " + info.getTick());
						account.closeAllMarketOrders();
					} else if(rule2(info)) {
						if(entryCount < entryCountMax) {
							if(positionUnits > 0) {
								MarketOrder entryOrder = createOrder(purchasePair, oneFifthUnits);
								account.execute(entryOrder);
								System.out.println("New Entry Order to improve cost Basis: " + entryOrder);
							} else {
								MarketOrder entryOrder = createOrder(purchasePair, -oneFifthUnits);
								account.execute(entryOrder);
								System.out.println("New Entry Order to improve cost Basis: " + entryOrder);
							}
							entryCount++;
						}
					} else if(rule3(info)) {
						int zoneSize = 0;
						if(positionUnits > 0) {
							zoneSize = (int)((positionPrice - info.getTick().getBid()) / purchasePair.getPIP());
						} else {
							zoneSize = (int)((info.getTick().getAsk() - positionPrice) / purchasePair.getPIP());
						}
						//frame.setCurrentZoneSize(zoneSize);
						if(zoneSize >= ATRZoneSize) {
							System.out.println("Zone Recovery Kicking In zone size: " + zoneSize);
							zrom = new ZoneRecoveryOrderManager(rateTable, account, triad, positionUnits, positionPrice, (int)(zoneSize * profitMult), zoneSize, 1, frame);
							zrom.rateInfo(info);
						}
					}
				}
			}
			if(info.getPair().equals(purchasePair)) {
				updatePosition(info.getPair());
			}
			frame.updateStats();
		} catch (OAException e) {
			e.printStackTrace();
		} catch (TALibDataException e) {
			e.printStackTrace();
		}
	}
	
	private void updateLiveChart(FXRateEventInfo info) throws OAException {
		Vector<FXHistoryPoint> candles = rateTable.getHistory(info.getPair(), FXClient.INTERVAL_5_SEC, 1440);
		
		LinkedList<DataBar> bars = bh.convertHistoryBars(candles);
		
		double pipsize = purchasePair.getPIP();
		
		if(liveChartRaw == null) {
			liveChartRaw = new OHLCCandleChart("Live Chart Raw", bars);
			chartsControl.registerChart(liveChartRaw);
		} else {
			liveChartRaw.update(bars);
		}
		
		bars = bh.generateRenko(bars, pipsize, renkoBarSize);
		
		if(liveChartRenko == null) {
			liveChartRenko = new OHLCCandleChart("Live Chart Renko", bars);
			chartsControl.registerChart(liveChartRenko);
		} else {
			liveChartRenko.update(bars);
		}

		bars = bh.generateHeikenAshi(bars, pipsize);
		
		if(liveChartHK == null) {
			liveChartHK = new OHLCCandleChart("Live Chart Heiken Ashi", bars);
			chartsControl.registerChart(liveChartHK);
		} else {
			liveChartHK.update(bars);
		}

	}

	private Direction getStep1MasterDirection(FXRateEventInfo info) throws OAException {
//		Step 1: (Daily Chart)
//		   (Daily or 240) Heiken Ashi
//		   Take positions based on the previous bar color
		
		Vector<FXHistoryPoint> candles = rateTable.getHistory(info.getPair(), FXClient.INTERVAL_1_HOUR, 160);
		LinkedList<DataBar> bars = bh.convertHourHistoryTo4HourBars(candles);
		
		double pipsize = purchasePair.getPIP();
		bars = bh.generateHeikenAshi(bars, pipsize);
		//bh.printBars(bars);

		if(step1Chart == null) {
			step1Chart = new OHLCCandleChart("Step 1 HeikenAshi", bars);
			chartsControl.registerChart(step1Chart);
		} else {
			step1Chart.update(bars);
		}
		bars.removeLast();
		return bars.peekLast().getDir();
	}

	private Direction getStep2TrendDirection(FXRateEventInfo info) throws OAException, TALibDataException {
//		Step 2: (Trend Chart)
//		   Determine Trend
//		   (60 or 30) Slope of EMA(20)
//		   Last Bar same as bar in step 1
//		   Price Above or UP the EMA(20)
//		   Price Below or DOWN the EMA(20)
		
		Vector<FXHistoryPoint> history = rateTable.getHistory(info.getPair(), FXClient.INTERVAL_30_MIN, 40);
		LinkedList<DataBar> bars = bh.convertHistoryBars(history);
		
		if(step2ChartRaw == null) {
			step2ChartRaw = new OHLCCandleChart("Step 2 Raw", bars);
			chartsControl.registerChart(step2ChartRaw);
		} else {
			step2ChartRaw.update(bars);
		}
		
		DataBar ema = bh.getEma(bars, 20);
		Direction emaDirection = bh.emaDir(bars, 20);
		
		bars = bh.generateHeikenAshi(bars, purchasePair.getPIP());
		
		if(step2ChartHK == null) {
			step2ChartHK = new OHLCCandleChart("Step 2 HeikenAshi", bars);
			chartsControl.registerChart(step2ChartHK);
		} else {
			step2ChartHK.update(bars);
		}

		//System.out.println("TrendDirection EmaDir: " + emaDirection + " Last Bar Dir: " + bars.peekLast().getDir() + " Last Bar: " + bars.peekLast());
		//System.out.println("TrendDirection Tick: " + info.getTick());
		//System.out.println("TrendDirection Ema: " + ema);
		
		frame.setStep2EMADirection(emaDirection);
		frame.setStep2LastBarDirection(bars.peekLast().getDir());
		frame.setStep2EMA(ema);
		
		if(emaDirection == Direction.UP && bars.peekLast().getDir() == Direction.UP && info.getTick().getAsk() > ema.getCloseAsk()) {
			return Direction.UP;
		}
		if(emaDirection == Direction.DOWN && bars.peekLast().getDir() == Direction.DOWN && info.getTick().getBid() < ema.getCloseBid()) {
			return Direction.DOWN;
		}
		return Direction.NONE;
	}
	
	
	private Direction getStep3EntryTrigger(FXRateEventInfo info) throws OAException, TALibDataException {
//		Step 3: Trigger (Entry)
//		   Heiken Ashi
//		   5 Tick renko chart
//
//		   Confirm with MACD(HA(Renko(5, 1 Min(120)))) for the bars of that chart
		
		//Vector<CandlePoint> history = rateTable.getCandles(info.getPair(), FXClient.INTERVAL_5_MIN, 100);
		Vector<FXHistoryPoint> history = rateTable.getHistory(info.getPair(), FXClient.INTERVAL_30_SEC, 2400);
		LinkedList<DataBar> bars = bh.convertHistoryBars(history);
		if(step3ChartRaw == null) {
			step3ChartRaw = new OHLCCandleChart("Step 3 Raw", bars);
			chartsControl.registerChart(step3ChartRaw);
		} else {
			step3ChartRaw.update(bars);
		}
		
		double pipsize = purchasePair.getPIP();
		bars = bh.generateRenko(bars, pipsize, renkoBarSize);
		
		if(step3ChartRenko == null) {
			step3ChartRenko = new OHLCCandleChart("Step 3 Renko", bars);
			chartsControl.registerChart(step3ChartRenko);
		} else {
			step3ChartRenko.update(bars);
		}

		double[] macd = bh.getMACD(bars, 12, 26, 9);
		
		frame.setStep3MACD(macd);
		
		bars = bh.generateHeikenAshi(bars, purchasePair.getPIP());
		
		if(step3ChartHK == null) {
			step3ChartHK = new OHLCCandleChart("Step 3 HeikenAshi", bars);
			chartsControl.registerChart(step3ChartHK);
		} else {
			step3ChartHK.update(bars);
		}
		
		//bh.printBars(bars);
		frame.setStep3LastBarDirection(bars.peekLast().getDir());
		
		if(bars.peekLast().getDir() == Direction.UP && macd[0] > macd[1]) {
			return Direction.UP;
		}
		if(bars.peekLast().getDir() == Direction.DOWN && macd[3] < macd[4]) {
			return Direction.DOWN;
		}
		
		return Direction.NONE;
	}


	private boolean rule1(FXRateEventInfo info) throws OAException {
//		Rule 1 When in Profit:
//		When the last Step 3 bar changes color exit
		
		Vector<FXHistoryPoint> history = rateTable.getHistory(info.getPair(), FXClient.INTERVAL_30_SEC, 600);
		LinkedList<DataBar> bars = bh.convertHistoryBars(history);
		
		double pipsize = purchasePair.getPIP();
		
		if(rule1ChartRaw == null) {
			rule1ChartRaw = new OHLCCandleChart("Rule 1 Raw", bars);
			chartsControl.registerChart(rule1ChartRaw);
		} else {
			rule1ChartRaw.update(bars);
		}
		
		bars = bh.generateRenko(bars, pipsize, renkoBarSize);
		
		if(rule1ChartRenko == null) {
			rule1ChartRenko = new OHLCCandleChart("Rule 1 Renko", bars);
			chartsControl.registerChart(rule1ChartRenko);
		} else {
			rule1ChartRenko.update(bars);
		}
		
		bars = bh.generateHeikenAshi(bars, purchasePair.getPIP());
		
		if(rule1ChartHK == null) {
			rule1ChartHK = new OHLCCandleChart("Rule 1 HeikenAshi", bars);
			chartsControl.registerChart(rule1ChartHK);
		} else {
			rule1ChartHK.update(bars);
		}
		
		// Must look at the last bar in order to not get out on a newly painted but not full bar.
		// Changes to be the last bar it will automatically paint at average if it does change color get out
		DataBar currentBar = bars.peekLast();

		frame.setRule1LastBarDirection(bars.peekLast().getDir());
		frame.setRule1CurrentBarDirection(currentBar.getDir());
		
		if(positionUnits > 0) {
			if(info.getTick().getBid() > positionPrice && bars.peekLast().getDir() != Direction.DOWN) {
				System.out.println("Rule1: We are in Profit: " + df.format((info.getTick().getBid() - positionPrice) / pipsize));
				System.out.println("Rule1: But the bar is not DOWN yet: " + bars.peekLast().getDir());
			} else if(info.getTick().getBid() < positionPrice && bars.peekLast().getDir() == Direction.DOWN) {
				System.out.println("Rule1: We are no longer in Profit: " + df.format((info.getTick().getBid() - positionPrice) / pipsize));
				System.out.println("Rule1: But the bar has turned: " + bars.peekLast().getDir());
			}
			if(info.getTick().getBid() > positionPrice && bars.peekLast().getDir() == Direction.DOWN && currentBar.getDir() == Direction.DOWN) {
				System.out.println("Rule 1: " + df.format(info.getTick().getBid()) + " > " + positionPrice + " we are in profit");
				System.out.println("Rule 1: Last Bar: " + bars.peekLast() + " Dir: " + bars.peekLast().getDir());
				return true;
			}
		} else {
			if(info.getTick().getAsk() < positionPrice && bars.peekLast().getDir() != Direction.UP) {
				System.out.println("Rule1: We are in Profit: " + df.format((positionPrice - info.getTick().getAsk()) / pipsize));
				System.out.println("Rule1: But the bar has not turned yet: " + bars.peekLast().getDir());
			} else if(info.getTick().getAsk() > positionPrice && bars.peekLast().getDir() == Direction.UP) {
				System.out.println("Rule1: We are no longer in Profit: " + df.format((positionPrice - info.getTick().getAsk()) / pipsize));
				System.out.println("Rule1: But the bar has turned: " + bars.peekLast().getDir());
			}
			if(info.getTick().getAsk() < positionPrice && bars.peekLast().getDir() == Direction.UP && currentBar.getDir() == Direction.UP) {
				System.out.println("Rule 1: " + df.format(info.getTick().getAsk()) + " < " + positionPrice + " we are in profit");
				System.out.println("Rule 1: Last Bar: " + bars.peekLast() + " Dir: " + bars.peekLast().getDir());
				return true;
			}
		}
		
		return false;
	}
	

	private boolean rule2(FXRateEventInfo info) throws OAException, TALibDataException {
//		Step 1 and Step 2 are still in tack enter again.
//		Rule 2 When NOT in Profit (Entry rules are still in place)
//		   If entry conditions still in place
//		      enter with another 1/5
//		      only if the price is down 1/5 * ATR(14) on daily
		
		if(positionUnits > 0) {
			if(info.getTick().getAsk() < (positionPrice - (ATRZoneSize * purchasePair.getPIP())) && step1 == Direction.UP && step2 == Direction.UP && step3 == Direction.UP) {
				System.out.println("Rule 2: " + df.format(info.getTick().getAsk()) + " < " + df.format((positionPrice - (ATRZoneSize * purchasePair.getPIP()))));
				System.out.println("Rule 2: Conditions are still in place: but we are less then: " + ATRZoneSize);
				return true;
			} else {
				if(info.getTick().getAsk() < (positionPrice - (ATRZoneSize * purchasePair.getPIP()))) {
					System.out.println("Rule 2: Price Triggered however waiting on Step 3 bar to turn UP: " + step3);
				} else {
					System.out.println("Rule 2: Price Not Low Enough to trigger new Order: Price: " + df.format(info.getTick().getAsk()) + " Zone: " + (positionPrice - (ATRZoneSize * purchasePair.getPIP())));
				}
			}
		} else if(positionUnits < 0) {
			if(info.getTick().getBid() > (positionPrice + (ATRZoneSize * purchasePair.getPIP())) && step1 == Direction.DOWN && step2 == Direction.DOWN && step3 == Direction.DOWN) {
				System.out.println("Rule 2: " + df.format(info.getTick().getBid()) + " > " + df.format((positionPrice + (ATRZoneSize * purchasePair.getPIP()))));
				System.out.println("Rule 2: Conditions are still in place: but we are more then: " + ATRZoneSize);
				return true;
			} else {
				if(info.getTick().getBid() > (positionPrice + (ATRZoneSize * purchasePair.getPIP()))) {
					System.out.println("Rule 2: Price Triggered however waiting on Step 3 bar to turn DOWN: " + step3);
				} else {
					System.out.println("Rule 2: Price Not High Enough to trigger new Order: Price: " + df.format(info.getTick().getBid()) + " Zone: " + (positionPrice + (ATRZoneSize * purchasePair.getPIP())));
				}
			}
		}
		return false;
	}
	

	private boolean rule3(FXRateEventInfo info) throws OAException, TALibDataException {
//		Rule 3 When NOT in Profit (Entry rules are NO longer still in place)
//		   Use Zone Recovery
//		Kicks in ONLY when the trend changes
		
		if(positionUnits > 0) {
			return (info.getTick().getAsk() < (positionPrice - (ATRZoneSize * purchasePair.getPIP())) && (step1 == Direction.DOWN || step2 == Direction.DOWN));
		} else {
			return (info.getTick().getBid() > (positionPrice + (ATRZoneSize * purchasePair.getPIP())) && (step1 == Direction.UP || step2 == Direction.UP));
		}
	}

	private void updatePosition(FXPair pair) throws AccountException {
		Position entryPosition = account.getPosition(pair);
		if(entryPosition != null) {
			positionUnits = entryPosition.getUnits();
			positionPrice = entryPosition.getPrice();
		} else {
			positionUnits = 0;
			positionPrice = 0;
			entryCount = 0;
		}
		frame.setPositionUnits(positionUnits);
		frame.setPositionPrice(positionPrice);
		frame.setEntryCount(entryCount);
	}
	
	private void updateAtrZoneSize(FXPair pair) throws OAException, TALibDataException {
		Vector<FXHistoryPoint> candles = rateTable.getHistory(pair, FXClient.INTERVAL_1_HOUR, 30*24);
		LinkedList<DataBar> bars = bh.convertHourHistoryToDayBars(candles);
		DataBar bar = bh.getAtr(bars, 14);
		ATRZoneSize = (int)(((bar.getCloseAsk() + bar.getCloseBid()) / (2 * ATRParts)) / purchasePair.getPIP());
		
		System.out.println("Zone Edge Size: " + ATRZoneSize);
		if(positionUnits > 0) {
			ATRZoneEdgePrice = (positionPrice - (ATRZoneSize * purchasePair.getPIP()));
		} else if(positionUnits < 0) {
			ATRZoneEdgePrice = (positionPrice + (ATRZoneSize * purchasePair.getPIP()));
		} else {
			ATRZoneEdgePrice = 0;
		}
		System.out.println("Zone Edge Price: " + ATRZoneEdgePrice);
		frame.setATRZoneSize(ATRZoneSize);
		frame.setATRATRZoneEdgePrice(ATRZoneEdgePrice);
		frame.setATRParts(ATRParts);
		frame.setATR((bar.getCloseAsk() + bar.getCloseBid()) / 2);
	}
	
	private MarketOrder createOrder(Instrument pair, long amount) {
		MarketOrder m = API.createMarketOrder();
		m.setPair(pair);
		m.setUnits(amount);
		return m;
	}


}
