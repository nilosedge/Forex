package net.nilosplace.Forex.client;

import java.util.ArrayList;

public class SequenceCalculator {
	
	public static ArrayList<Double> calcMultipliersAtr(int atr) {
		int zoneSize = (int)(atr / 7);
		return SequenceCalculator.calcMultipliers(zoneSize * 3, zoneSize, 10, 0);
	}
	
	public static ArrayList<Double> calcMultipliers(long pipGain, long zoneSize, int initialPipProfit) {
		return SequenceCalculator.calcMultipliers(pipGain, zoneSize, initialPipProfit, 0);
	}
	
	public static ArrayList<Double> calcMultipliers(long pipGain, long zoneSize, double initialPipProfit, double extraPipsPerTrade) {
		
		double firstTrade = 1;
		double gainOnTrade = 0;
		ArrayList<Double> ret = new ArrayList<Double>();
		
		for(int i = 0; i < 30; i++) {
			if(i == 0) {
				gainOnTrade = firstTrade * (pipGain + zoneSize + (initialPipProfit + extraPipsPerTrade));
			} else if(i == 1) {
				firstTrade = (gainOnTrade + extraPipsPerTrade) / pipGain;
				gainOnTrade = (firstTrade * (pipGain + zoneSize)) - pipGain + initialPipProfit;
				ret.add(firstTrade);
			} else {
				firstTrade = (gainOnTrade + extraPipsPerTrade) / pipGain;
				gainOnTrade = firstTrade * (pipGain + zoneSize);
				ret.add(firstTrade);
			}
			//System.out.println("F: " + firstTrade + " G: " + gainOnTrade);
		}
		return ret;
	}
}
