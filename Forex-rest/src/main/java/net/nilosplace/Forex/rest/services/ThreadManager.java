package net.nilosplace.Forex.rest.services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.managers.CacheManager;
import net.nilosplace.Forex.ejb.oanda.OandaAPI;
import net.nilosplace.Forex.ejb.oanda.dto.OandaCandles;
import net.nilosplace.Forex.ejb.oanda.enums.OandaSystemType;
import net.nilosplace.Forex.ejb.util.FileProperty;
import net.nilosplace.Forex.rest.threads.OandaPriceStreamingThread;

import org.jboss.logging.Logger;

@ApplicationScoped
public class ThreadManager {

	private Logger log = Logger.getLogger(getClass());

	private boolean started = false;
	
	private ArrayList<String> prodList = new ArrayList<String>();
	private FileProperty config = FileProperty.OANDAPROPERTIESFILE;
	private OandaSystemType type = OandaSystemType.PRACTICE;
	
	@Inject
	CacheManager cm;
	
	@Resource(lookup="java:jboss/ee/concurrency/factory/ManagedThreadFactory")
    ManagedThreadFactory factory;
	
	OandaAPI api = new OandaAPI(type);
	
	private OandaPriceStreamingThread opst;
	//private OandaEventStreamingThread oest;
	private Thread thread1;
	//private Thread thread2;
	
	@PostConstruct
	public void init() {
		log.info("Starting ThreadManager");
		
		//prodList.add("EUR_USD");
		//prodList.add("AUD_JPY");
		//prodList.add("USD_JPY");
		//prodList.add("GBP_USD");
		//prodList.add("USD_CHF");
		//prodList.add("EUR_GBP");
		prodList.add("EUR_JPY");
		//prodList.add("EUR_CHF");
		//prodList.add("AUD_USD");
		//prodList.add("USD_CAD");
		//prodList.add("NZD_USD");
	}

	
	public void startUp() {
		log.info("Stream URL: " + config.getValue(type.getStreamUrl()));
		log.info("Api URL: " + config.getValue(type.getApiUrl()));
		started = true;
		
		updateCache();
		
		opst = new OandaPriceStreamingThread(prodList,
				config.getValue(type.getStreamUrl()),
				config.getValue(type.getAccountId()),
				config.getValue(type.getAccessToken()));
		
//		oest = new OandaEventStreamingThread(
//				config.getValue(type.getStreamUrl()),
//				config.getValue(type.getAccountId()),
//				config.getValue(type.getAccessToken()));
		

		thread1 = factory.newThread(opst);
		thread1.start();
		//thread2 = factory.newThread(oest);
		//thread2.start();

	}
	
	public void updateCache() {
		log.info("Updating Candle Cache");
		for(String instrument: prodList) {
			OandaCandles candles = api.getCandles(instrument, CacheType.MIN1, 720);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.MIN5, 576);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.MIN15, 200);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.MIN30, 96);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.HOUR1, 48);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.HOUR4, 24);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.DAY1, 24);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.WEEK1, 24);
			cm.updateCache(candles);
			candles = api.getCandles(instrument, CacheType.MONTH1, 12);
			cm.updateCache(candles);
		}
		log.info("Finished Updating Candle Cache");
	}
	
	@PreDestroy
	public void shutdown() {
		//oest.kill();
		opst.kill();
		started = false;
	}

	public void toggleThread() {
		if(started) {
			log.info("Shuting down Oanda Threads: " + started);
			shutdown();
		} else {
			log.info("Starting up Oanda Threads: " + started);
			startUp();
		}
	}
}
