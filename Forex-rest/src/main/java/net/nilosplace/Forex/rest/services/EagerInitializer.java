package net.nilosplace.Forex.rest.services;

import org.jboss.logging.Logger;

@javax.ejb.Singleton
@javax.ejb.Startup
public class EagerInitializer {
    @javax.inject.Inject
    private ThreadManager appBean;
 
    private Logger log = Logger.getLogger(getClass());
    
    @javax.annotation.PostConstruct
    public void eagerInit() {
    	log.info("This is the EagerInitializer bean");
        appBean.toString();
    }
}
