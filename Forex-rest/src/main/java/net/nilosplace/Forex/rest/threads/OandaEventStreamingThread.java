package net.nilosplace.Forex.rest.threads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.nilosplace.Forex.ejb.common.events.DataEvent;
import net.nilosplace.Forex.ejb.common.events.FillEvent;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.oanda.dto.OandaHeartBeat;
import net.nilosplace.Forex.ejb.oanda.dto.OandaTransaction;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.util.ModelMapper;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class OandaEventStreamingThread implements Runnable {

	private Logger log = Logger.getLogger(getClass());
	private ModelMapper model = new ModelMapper();
	
	private boolean running = true;
	private DefaultHttpClient httpClient = null;
	private HttpParams params = new BasicHttpParams();
	//private String domain = "https://stream-fxtrade.oanda.com";
	//https://stream-fxtrade.oanda.com/v1/events?accountIds=12345
	private String url;
	private String accountId;
	private String access_token;
	
	
	public OandaEventStreamingThread(String url, String accountId, String access_token) {
		this.url = url;
		this.accountId = accountId;
		this.access_token = access_token;
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpConnectionParams.setSoTimeout(params, 10000);
	}
	
	@Override
	public void run() {
		log.info("Running OandaEventStreamingThread");
		
		while(running) {
			
			httpClient = new DefaultHttpClient(params);
			try {

				HttpUriRequest httpGet = null;
				if(access_token != null && accountId != null) {
					httpGet = new HttpGet(url + "/v1/events?accountIds=" + accountId);
					httpGet.setHeader(new BasicHeader("Authorization", "Bearer " + access_token));
				} else if(accountId != null) {
					httpGet = new HttpGet(url + "/v1/events?accountIds=" + accountId);
				} else {
					log.error("Unable to connect to oanda events streaming");
					return;
				}
				
				log.info("Executing request: " + httpGet.getRequestLine());

				HttpResponse resp = httpClient.execute(httpGet);
				HttpEntity entity = resp.getEntity();

				if (resp.getStatusLine().getStatusCode() == 200 && entity != null) {
					InputStream stream = entity.getContent();
					String line;
					BufferedReader br = new BufferedReader(new InputStreamReader(stream));

					while ((line = br.readLine()) != null && running) {

						//log.info("Line: " + line);
						Object obj = JSONValue.parse(line);
						JSONObject json = (JSONObject) obj;

						FillEvent f = null;
						DataEvent d = null;
						if (json.containsKey("transaction")) {
							OandaTransaction t = (OandaTransaction)model.getObject(json.get("transaction").toString(), OandaTransaction.class);
							log.info("Json: " + json.toJSONString());
							f = new FillEvent(t);
							f.setSource(DataSourceType.OANDALIVETICKS);
						}
						if(json.containsKey("heartbeat")) {
							OandaHeartBeat hb = (OandaHeartBeat)model.getObject(json.get("heartbeat").toString(), OandaHeartBeat.class);
							d = new DataEvent(hb);
						}
						if(d != null) {
							//log.info("Heart Beat: " + d);
							//EventConnection.sendObject(LookupResource.DATAHANDLERQUEUE, d);
						}
						if(f != null) {
							//log.info("Sending FILL EVENT!!!");
							//EventConnection.sendObject(LookupResource.FILLEVENTQUEUE, f);
						}
					}
					httpClient.getConnectionManager().shutdown();
				} else {
					// print error message
					String responseString = EntityUtils.toString(entity, "UTF-8");
					log.error(responseString);
				}

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				log.error("Exception: " + e.getMessage());
				log.info("Shuting down Oanda Event Stream");
			} finally {
				httpClient.getConnectionManager().shutdown();
			}
			if(running) {
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	
	public void kill() {
		running = false;
		if(httpClient != null) {
			httpClient.getConnectionManager().shutdown();
		}
	}
	
	public void shutdown() {
		running = false;
	}
		
}
