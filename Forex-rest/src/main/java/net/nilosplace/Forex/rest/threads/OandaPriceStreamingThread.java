package net.nilosplace.Forex.rest.threads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.naming.InitialContext;

import net.nilosplace.Forex.ejb.common.events.DataEvent;
import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.entities.Tick;
import net.nilosplace.Forex.ejb.oanda.dto.OandaHeartBeat;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.util.ModelMapper;
import net.nilosplace.Forex.ejb.util.QueueContainer;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class OandaPriceStreamingThread implements Runnable {

	private Logger log = Logger.getLogger(getClass());
	private ModelMapper model = new ModelMapper();

	private boolean running = true;
	private String instruments = "";
	private DefaultHttpClient httpClient = null;
	private HttpParams params = new BasicHttpParams();
	//private String domain = "https://stream-fxtrade.oanda.com";
	private String url;
	private String accountId;
	private String access_token;
	private QueueContainer qc = new QueueContainer(LookupResource.DATAHANDLERQUEUE);

	public OandaPriceStreamingThread(ArrayList<String> instruments, String url, String accountId, String access_token) {
		this.instruments = StringUtils.join(instruments, "%2C");
		this.url = url;
		this.accountId = accountId;
		this.access_token = access_token;
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpConnectionParams.setSoTimeout(params, 10000);
		
	}

	public void run() {
		log.info("Running OandaPriceStreamingThread");

		while(running) {

			httpClient = new DefaultHttpClient(params);
			try {

				HttpUriRequest httpGet = null;
				if(access_token != null && accountId != null) {
					httpGet = new HttpGet(url + "/v1/prices?accountId=" + accountId + "&instruments=" + instruments);
					httpGet.setHeader(new BasicHeader("Authorization", "Bearer " + access_token));
				} else {
					httpGet = new HttpGet(url + "/v1/prices?instruments=" + instruments);
				}

				log.info("Executing request: " + httpGet.getRequestLine());

				HttpResponse resp = httpClient.execute(httpGet);
				HttpEntity entity = resp.getEntity();

				if (resp.getStatusLine().getStatusCode() == 200 && entity != null) {
					InputStream stream = entity.getContent();
					String line;
					BufferedReader br = new BufferedReader(new InputStreamReader(stream));

					while ((line = br.readLine()) != null && running) {

						//log.info("Line: " + line);
						Object obj = JSONValue.parse(line);
						JSONObject json = (JSONObject) obj;

						DataEvent d = null;
						if (json.containsKey("tick")) {
							Tick t = (Tick)model.getObject(json.get("tick").toString(), Tick.class);
							Candle c = new Candle();
							c.setCandle(DataSourceType.OANDALIVETICKS, CacheType.TICKS, t);
							d = new DataEvent(c);
						}
						if(json.containsKey("heartbeat")) {
							OandaHeartBeat hb = (OandaHeartBeat)model.getObject(json.get("heartbeat").toString(), OandaHeartBeat.class);
							d = new DataEvent(hb);
						}
						qc.sendObject(d);
					}
					httpClient.getConnectionManager().shutdown();
				} else {
					// print error message
					String responseString = EntityUtils.toString(entity, "UTF-8");
					log.error(responseString);
				}

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				log.error("Exception: " + e.getMessage());
				log.info("Shuting down Oanda Price Stream");
			} finally {
				httpClient.getConnectionManager().shutdown();
			}
			if(running) {
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void kill() {
		running = false;
		if(httpClient != null) {
			httpClient.getConnectionManager().shutdown();
		}
	}

	public void shutdown() {
		running = false;
	}
}
