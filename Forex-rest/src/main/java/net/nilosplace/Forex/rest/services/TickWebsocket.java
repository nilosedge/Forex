package net.nilosplace.Forex.rest.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.event.Observes;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import net.nilosplace.Forex.ejb.common.events.DataEvent;
import net.nilosplace.Forex.ejb.common.events.MarketEvent;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.util.ModelMapper;
import net.nilosplace.Forex.ejb.webservice.WSJMSMessage;

import org.jboss.logging.Logger;

@ServerEndpoint("/tick")
public class TickWebsocket {

	private ModelMapper m = new ModelMapper();
	private Logger log = Logger.getLogger(this.getClass());
	
	private static final Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());

	@OnOpen
	public void onOpen(final Session session) {
		try {
			session.getBasicRemote().sendText("session opened");
			sessions.add(session);

		} catch (Exception e) {
			log.warn("sender is null");
			e.printStackTrace();
		}
	}

	@OnMessage
	public void onMessage(final String message, final Session client) {
		try {
			client.getBasicRemote().sendText("client message: " + message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@OnClose
	public void onClose(final Session session) {
		try {
			session.getBasicRemote().sendText("WebSocket Session closed");
			sessions.remove(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onJMSMessage(@Observes @WSJMSMessage Message msg) {
		//log.info("Got JMS Message at WebSocket! " + msg);
		try {
			DataEvent me = (DataEvent)(((ObjectMessage)msg).getObject());
			
			if(me.getEvent() instanceof ArrayList<?>) {
				for(Object o: (ArrayList<?>)me.getEvent()) {
					Candle candle = (Candle)o;
					sendCandleToClients(candle);
				}
			} else if(me.getEvent() instanceof Candle) {
				Candle candle = (Candle)me.getEvent();
				sendCandleToClients(candle);
			}
			
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	private void sendCandleToClients(Candle tick) {
		try {
			String json = m.getString(tick);
			for (Session s : sessions) {
				s.getBasicRemote().sendText(json);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
