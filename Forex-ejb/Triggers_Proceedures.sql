CREATE LANGUAGE plpgsql;

CREATE FUNCTION create_record(ts text, index TIMESTAMP WITH TIME ZONE, bid numeric, inst text) RETURNS void AS $body$
BEGIN
	insert into candle (open, high, low, close, instrument, time, timescale) values(bid, bid, bid, bid, inst, index, ts);
END;
$body$ LANGUAGE plpgsql;

CREATE FUNCTION update_record(high numeric, low numeric, ids bigint, bid numeric) RETURNS void AS $body$
BEGIN
	IF bid > high THEN
		update candle set high = bid where id = ids;
	END IF;
	
	IF bid < low THEN
		update candle set low = bid where id = ids;
	END IF;

	update candle set close = bid where id = ids;
	
END;
$body$ LANGUAGE plpgsql;


CREATE FUNCTION update_candle(inst text, times text, ts TIMESTAMP WITH TIME ZONE, bid numeric) RETURNS void AS $body$
DECLARE
	target RECORD;
BEGIN
	select * FROM candle c where c.instrument=inst and c.timescale=times and c.time = ts INTO target;
	IF NOT FOUND THEN
		PERFORM create_record(times, ts, bid, inst);
	ELSE
		PERFORM update_record(target.high, target.low, target.id, bid);
	END IF;
END;
$body$ LANGUAGE plpgsql;


CREATE FUNCTION update_candles() RETURNS trigger AS $_$
BEGIN
	PERFORM update_candle(NEW.instrument, 'min1', date_trunc('minute', NEW.time), NEW.bid);
	PERFORM update_candle(NEW.instrument, 'min5', TIMESTAMP WITH TIME ZONE 'epoch' + (INTERVAL '1 second' * (floor(extract(EPOCH from NEW.time) / 300) * 300)), NEW.bid);
	PERFORM update_candle(NEW.instrument, 'min15', TIMESTAMP WITH TIME ZONE 'epoch' + (INTERVAL '1 second' * (floor(extract(EPOCH from NEW.time) / 900) * 900)), NEW.bid);
	PERFORM update_candle(NEW.instrument, 'min30', TIMESTAMP WITH TIME ZONE 'epoch' + (INTERVAL '1 second' * (floor(extract(EPOCH from NEW.time) / 1800) * 1800)), NEW.bid);
	PERFORM update_candle(NEW.instrument, 'hour1', date_trunc('hour', NEW.time), NEW.bid);
	PERFORM update_candle(NEW.instrument, 'hour4', TIMESTAMP WITH TIME ZONE 'epoch' + INTERVAL '1 second' * floor(extract(EPOCH from NEW.time) / 14400) * 14400, NEW.bid);
	PERFORM update_candle(NEW.instrument, 'day1', date_trunc('day', NEW.time), NEW.bid);
	PERFORM update_candle(NEW.instrument, 'week1', date_trunc('week', NEW.time), NEW.bid);
	PERFORM update_candle(NEW.instrument, 'month1', date_trunc('month', NEW.time), NEW.bid);

	RETURN NEW;
END; $_$ LANGUAGE plpgsql;

CREATE TRIGGER update_candles AFTER INSERT ON tick FOR EACH ROW EXECUTE PROCEDURE update_candles();


select create_record(text 'minute', date_trunc('minute', TIMESTAMP WITH TIME ZONE 'now'), numeric '107.9', text 'AUD_JPY');
select update_record(107.90, 107.90, 1, 107.90);
select update_candle('AUD_JPY', 'min1', date_trunc('minute', now()), 4.50);
select now(), date_trunc('minute', TIMESTAMP WITH TIME ZONE 'now') as min
,TIMESTAMP WITH TIME ZONE 'epoch' + (INTERVAL '1 second' * (floor(extract(EPOCH from now()) / 300) * 300)) as min5
,TIMESTAMP WITH TIME ZONE 'epoch' + (INTERVAL '1 second' * (floor(extract(EPOCH from now()) / 900) * 900)) as min15
, TIMESTAMP WITH TIME ZONE 'epoch' + (INTERVAL '1 second' * (floor(extract(EPOCH from now()) / 1800) * 1800)) as min30
, date_trunc('hour', TIMESTAMP WITH TIME ZONE 'now') as hour1
, TIMESTAMP WITH TIME ZONE 'epoch' + INTERVAL '1 second' * floor(extract(EPOCH from now()) / 14400) * 14400 as hour4
, date_trunc('day', TIMESTAMP WITH TIME ZONE 'now') as day1
, date_trunc('week', TIMESTAMP WITH TIME ZONE 'now') as week
, date_trunc('month', TIMESTAMP WITH TIME ZONE 'now') as month