package net.nilosplace.Forex.ejb.webservice;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import net.nilosplace.Forex.ejb.managers.CacheManager;

@Path("/cache")
@RequestScoped
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class CacheService {

	@Inject
	private CacheManager cm;
	
	@GET
	@Path("/printcache")
	public Response getAllSymbol() {
		cm.printCache();
		return Response.ok("").build();
	}
}
