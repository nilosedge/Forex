package net.nilosplace.Forex.ejb.entities;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;

import org.apache.commons.lang3.time.DateUtils;

@Entity
public class Candle implements Serializable {

	private static final long serialVersionUID = 456020886767410995L;
	//private Logger log = Logger.getLogger(getClass());
	private Long id;
	private double openBid;
	private double openAsk;
	private double highBid;
	private double highAsk;
	private double lowBid;
	private double lowAsk;
	private double closeBid;
	private double closeAsk;
	private double volume;
	private Boolean complete;
	private String instrument;
	private Date time;
	private CacheType timeScale;
	private DataSourceType source;
	
	public Candle() {
		
	}
	
	public Candle(Candle candle) {
		this.id = candle.getId();
		this.openBid = candle.getOpenBid();
		this.openAsk = candle.getOpenAsk();
		this.highBid = candle.getHighBid();
		this.highAsk = candle.getHighAsk();
		this.lowBid = candle.getLowBid();
		this.lowAsk = candle.getLowAsk();
		this.closeBid = candle.getCloseBid();
		this.closeAsk = candle.getCloseAsk();
		this.volume = candle.getVolume();
		this.complete = candle.getComplete();
		this.instrument = candle.getInstrument();
		this.time = candle.getTime();
		this.timeScale = candle.getTimeScale();
		this.source = candle.getSource();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(precision=19, scale=6)
	public double getOpenBid() {
		return openBid;
	}
	public void setOpenBid(double openBid) {
		this.openBid = openBid;
	}
	@Column(precision=19, scale=6)
	public double getOpenAsk() {
		return openAsk;
	}
	public void setOpenAsk(double openAsk) {
		this.openAsk = openAsk;
	}
	@Column(precision=19, scale=6)
	public double getHighBid() {
		return highBid;
	}
	public void setHighBid(double highBid) {
		this.highBid = highBid;
	}
	@Column(precision=19, scale=6)
	public double getHighAsk() {
		return highAsk;
	}
	public void setHighAsk(double highAsk) {
		this.highAsk = highAsk;
	}
	@Column(precision=19, scale=6)
	public double getLowBid() {
		return lowBid;
	}
	public void setLowBid(double lowBid) {
		this.lowBid = lowBid;
	}
	@Column(precision=19, scale=6)
	public double getLowAsk() {
		return lowAsk;
	}
	public void setLowAsk(double lowAsk) {
		this.lowAsk = lowAsk;
	}
	@Column(precision=19, scale=6)
	public double getCloseBid() {
		return closeBid;
	}
	public void setCloseBid(double closeBid) {
		this.closeBid = closeBid;
	}
	@Column(precision=19, scale=6)
	public double getCloseAsk() {
		return closeAsk;
	}
	public void setCloseAsk(double closeAsk) {
		this.closeAsk = closeAsk;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public Boolean getComplete() {
		return complete;
	}
	public void setComplete(Boolean complete) {
		this.complete = complete;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public CacheType getTimeScale() {
		return timeScale;
	}
	public void setTimeScale(CacheType timeScale) {
		this.timeScale = timeScale;
	}
	public DataSourceType getSource() {
		return source;
	}
	public void setSource(DataSourceType source) {
		this.source = source;
	}
	

	
	public Date toBar(CacheType type) {
		switch(type) {
			case TICKS:
				return time;
			case MIN1:
				return toMin1Bar();
			case MIN5:
				return toMin5Bar();
			case MIN15:
				return toMin15Bar();
			case MIN30:
				return toMin30Bar();
			case HOUR1:
				return toHour1Bar();
			case HOUR4:
				return toHour4Bar();
			case DAY1:
				return toDay1Bar();
			case WEEK1:
				return toWeek1Bar();
			case MONTH1:
				return toMonth1Bar();
		}
		return null;
	}
	
	
	public Date toMin1Bar() {
		return DateUtils.truncate(time, Calendar.MINUTE);
	}
	public Date toMin5Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.MINUTE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.MINUTE);
		int mod = unroundedMinutes % 5;
		calendar.add(Calendar.MINUTE, -mod);
		return calendar.getTime();
	}
	
	public Date toMin15Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.MINUTE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.MINUTE);
		int mod = unroundedMinutes % 15;
		calendar.add(Calendar.MINUTE, -mod);
		return calendar.getTime();
	}
	
	public Date toMin30Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.MINUTE);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.MINUTE);
		int mod = unroundedMinutes % 30;
		calendar.add(Calendar.MINUTE, -mod);
		return calendar.getTime();
	}
	
	public Date toHour1Bar() {
		return DateUtils.truncate(time, Calendar.HOUR);
	}
	
	public Date toHour4Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.HOUR);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.HOUR);
		int mod = unroundedMinutes % 4;
		calendar.add(Calendar.HOUR, -mod);
		return calendar.getTime();
	}
	public Date toDay1Bar() {
		return DateUtils.truncate(time, Calendar.DAY_OF_MONTH);
	}
	
	public Date toWeek1Bar() {
		Date whateverDateYouWant = DateUtils.truncate(time, Calendar.DAY_OF_MONTH);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(whateverDateYouWant);
		int unroundedMinutes = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		calendar.add(Calendar.DAY_OF_WEEK, -unroundedMinutes);
		return calendar.getTime();
	}
	
	public Date toMonth1Bar() {
		// TODO this seems wrong
		return DateUtils.truncate(time, Calendar.MONTH);
	}
	
	public void setCandle(DataSourceType source, CacheType type, Tick t) {
		this.time = t.getTime();
		this.instrument = t.getInstrument();
		this.timeScale = type;
		this.source = source;
		this.highAsk = t.getAsk();
		this.lowAsk = t.getAsk();
		this.closeAsk = t.getAsk();
		this.openAsk = t.getAsk();
		this.highBid = t.getBid();
		this.lowBid = t.getBid();
		this.closeBid = t.getBid();
		this.openBid = t.getBid();
	}
	
	public String toString() {
		return "Id: " + id + " openBid: " + openBid + " openAsk: " + openAsk + " highBid: " + highBid + " highAsk: " + highAsk + " lowBid: " + lowBid + " lowAsk: " + lowAsk + " closeBid: " + closeBid + " closeAsk: " + closeAsk + " volume: " + volume + " complete: " + complete + " instrument: " + instrument + " time: " + time + " timeScale: " + timeScale + " source: " + source;
	}

}
