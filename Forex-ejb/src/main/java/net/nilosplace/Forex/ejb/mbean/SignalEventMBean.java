package net.nilosplace.Forex.ejb.mbean;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import net.nilosplace.Forex.ejb.common.events.SignalEvent;
import net.nilosplace.Forex.ejb.managers.PortfolioManager;

/**
 * Message-Driven Bean implementation class for: SignalEventMBean
 */
@MessageDriven(
		name = "SignalEventQueue",
		description = "SignalEventQueue MBean",
		activationConfig = {
				@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
				@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/SignalEventQueue"),
				@ActivationConfigProperty(propertyName = "", propertyValue = ""),
				@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")

		})
public class SignalEventMBean implements MessageListener {

	@Inject
	private PortfolioManager pm;

	//private Logger log = Logger.getLogger(getClass());

	public SignalEventMBean() {

	}

	public void onMessage(Message message) {
		try {
			if(((ObjectMessage)message).getObject() instanceof SignalEvent) {
				SignalEvent s = (SignalEvent)(((ObjectMessage)message).getObject());
				pm.processSignalMessage(s);
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}

}
