package net.nilosplace.Forex.ejb.strategies.MAC;

import net.nilosplace.Forex.ejb.common.events.SignalEvent.SignalType;
import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;
import net.nilosplace.Forex.ejb.strategies.Strategy;

import org.jboss.logging.Logger;

public abstract class MovingAverageCrossStrategy extends Strategy {


	protected abstract void candle(Candle candle);
	private Logger log = Logger.getLogger(getClass());
	
	protected void runStrategy(CacheType cacheType, int short_window, int long_window, Candle candle) {

		double short_sma, long_sma;
		try {
			short_sma = sma(CacheType.MIN1, short_window);
			long_sma = sma(CacheType.MIN1, long_window);
		} catch (DataUnavailableException e) {
			log.trace("Data Message: " + e.getMessage());
			return;
		} catch (TALibDataException e) {
			return;
		}

		log.trace("Instrument: " + instrument + " Short SMA: " + short_sma + " Long SMA: " + long_sma);

		double strength = 1.0;

		if(short_sma > long_sma && getState() == StrategyState.OUT) {
			log.trace("short_sma > long_sma Buying In");
			signal(candle.getTime(), SignalType.LONG, strength);
		} else if(short_sma < long_sma && getState() == StrategyState.LONG) {
			log.trace("short_sma < long_sma Getting Out");
			signal(candle.getTime(), SignalType.EXIT, strength);
		}
	}

}
