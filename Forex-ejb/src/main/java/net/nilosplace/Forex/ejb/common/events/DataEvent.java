package net.nilosplace.Forex.ejb.common.events;

import java.io.Serializable;

public class DataEvent extends TradingEvent implements Serializable {

	private static final long serialVersionUID = -2358597883028932612L;

	public DataEvent(Object data) {
		super(data);
	}
}
