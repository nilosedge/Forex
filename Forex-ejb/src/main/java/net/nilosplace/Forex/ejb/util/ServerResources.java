package net.nilosplace.Forex.ejb.util;

import javax.jms.Queue;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.infinispan.manager.EmbeddedCacheManager;

public class ServerResources {

	private static InitialContext context = null;
	
	private static void setInitialContext() throws NamingException {
		if(context == null) {
			context = new InitialContext();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T lookupResource(LookupResource lookup) {

        try {
			setInitialContext();
			return (T)context.lookup(lookup.get_jndiLookupName());
		} catch (NamingException e) {
			e.printStackTrace();
			return null;
		}
    }
	
	public enum LookupResource {
//		MYSQLDATASOURCELOCAL(DataSource.class.getName(), "jdbc:mysql://localhost:3306/Database?user=&password=") {},
//		MYSQLDATASOURCE(DataSource.class.getName(), "java:jboss/datasources/MySQLXA") {},
//		MESSAGINGCONNECTION(ConnectionFactory.class.getName(), "java:/JmsXA") {},
//		NETSUITETOCACHE(Queue.class.getName(), "java:/queue/NetsuiteToCache") {},
//		TRANSACTIONPROCESSOR(Queue.class.getName(), "java:/queue/TransactionProcessor") {},
//		NETSUITEORDERAPPROVALQUEUE(Queue.class.getName(), "java:/queue/NetsuiteOrderApprovalQueue") {},
//		NETSUITETOMYSQL(Queue.class.getName(), "java:/queue/NetsuiteToMySQL") {},
//		ANALYTICSQUEUE(Queue.class.getName(), "java:/queue/AnalyticsQueue") {},
//		MYSQLTOCACHE(Queue.class.getName(), "java:/queue/MySQLToCache") {},
//		MAILSESSION(Session.class.getName(), "java:/Mail") {},
//		CACHEDATASOURCE(EmbeddedCacheManager.class.getName(), "java:jboss/infinispan/container/pbj"),
//		NETSUITETRIGGERS(Queue.class.getName(), "java:/queue/NetsuiteTriggersQueue") {}, 
//		MAILCHIMPUPDATE(Queue.class.getName(), "java:/queue/MailChimpQueue") {},
//		RECORDREFUPDATEEVENT(Queue.class.getName(), "java:/queue/RecordRefUpdateEventQueue") {},
		
		MARKETEVENTTOPIC(Topic.class.getName(), "topic/MarketEventTopic"),
		DATAHANDLERQUEUE(Queue.class.getName(), "queue/DataEventQueue"),
		SIGNALEVENTQUEUE(Queue.class.getName(), "queue/SignalEventQueue"),
		ORDEREVENTQUEUE(Queue.class.getName(), "queue/OrderEventQueue"),
		FILLEVENTQUEUE(Queue.class.getName(), "queue/FillEventQueue"),
		PRICEDATA(EmbeddedCacheManager.class.getName(), "java:jboss/infinispan/container/PriceData"),
		PORTFOLIODATA(EmbeddedCacheManager.class.getName(), "java:jboss/infinispan/container/PortfolioData"),
		;
		
		private String _returnClass;
		private String _jndiLookupName;
		
		LookupResource(String _returnClass, String _jndiLookupName) {
			this._returnClass = _returnClass;
			this._jndiLookupName = _jndiLookupName;
		}

		public String get_returnClass() {
			return _returnClass;
		}
		public String get_jndiLookupName() {
			return _jndiLookupName;
		}
	}
}
