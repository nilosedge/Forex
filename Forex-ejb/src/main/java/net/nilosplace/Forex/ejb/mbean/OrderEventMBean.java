package net.nilosplace.Forex.ejb.mbean;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import net.nilosplace.Forex.ejb.entities.Order;
import net.nilosplace.Forex.ejb.managers.ExecutionManager;
import net.nilosplace.Forex.ejb.managers.PortfolioManager;

import org.jboss.logging.Logger;

@MessageDriven(
		name = "OrderEventQueue",
		description = "OrderEventQueue MBean",
		activationConfig = {
				@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
				@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/OrderEventQueue"),
				@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")
		})
public class OrderEventMBean implements MessageListener {

	@Inject
	private ExecutionManager em;
	
	@Inject
	private PortfolioManager pm;

	private Logger log = Logger.getLogger(getClass());

	public void onMessage(Message message) {
		try {
			if(((ObjectMessage)message).getObject() instanceof Order) {
				Order o = (Order)(((ObjectMessage)message).getObject());
				Order or = em.executeOrder(o);
				pm.processOrderFill(or);
			}
		} catch (Exception e) {
			log.info("Order Failed, NOT GOING TO TRY AGAIN IN CASE IT DIDNT FILE: ");
			e.printStackTrace();
		}
	}
}
