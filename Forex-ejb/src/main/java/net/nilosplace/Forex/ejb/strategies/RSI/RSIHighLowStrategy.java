package net.nilosplace.Forex.ejb.strategies.RSI;

import net.nilosplace.Forex.ejb.common.events.SignalEvent.SignalType;
import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;
import net.nilosplace.Forex.ejb.strategies.Strategy;

import org.jboss.logging.Logger;

public class RSIHighLowStrategy extends Strategy {

	private Logger log = Logger.getLogger(getClass());
	
	/*
	 * Basic strategy
	 * Buy Signal: When all 4 rsi's drop below a certain level (30) and the SMA 200 is in an uptrend
	 * Exit Signal: Once the RSI gets back to 50
	 * 
	 * Sell Signal: When all 4 rsi's raise above a certain level (70) and the SMA 200 is in an downtrend
	 * Exit Signal: Once the RSI gets back to 50
	 */
	
	@Override
	protected void candle(Candle candle) {
		
		
		
		double rsi_min1, rsi_min5, rsi_min15, rsi_min30, ema200;
		boolean emaup;
		try {
			rsi_min1 = rsi(CacheType.MIN1, 14, true);
			rsi_min5 = rsi(CacheType.MIN5, 14, true);
			rsi_min15 = rsi(CacheType.MIN15, 14, true);
			rsi_min30 = rsi(CacheType.MIN30, 14, true);
			emaup = emaup(CacheType.MIN30, 14);
		} catch (DataUnavailableException e) {
			log.trace("Instrument: " + candle.getInstrument() + " Data Message: " + e.getMessage());
			return;
		} catch (TALibDataException e) {
			return;
		}
		
		log.trace("Candle: " + candle);
		log.trace("I: " + candle.getInstrument() + " rsi_min1: " + rsi_min1 + " rsi_min5: " + rsi_min5 + " rsi_min15: " + rsi_min15 + " rsi_min30: " + rsi_min30 + "EMAUP: " + emaup);
		
		if(rsi_min1 < 30 && rsi_min5 < 30 && rsi_min15 < 30 && rsi_min30 < 30 && emaup) {
			if(getState() == StrategyState.OUT) {
				signal(candle.getTime(), SignalType.LONG, 1.0);
				return;
			}
		}
		if(rsi_min30 > 50 && getState() == StrategyState.LONG) {
			signal(candle.getTime(), SignalType.EXIT, 1.0);
			return;
		}
		
		if(rsi_min1 > 70 && rsi_min5 > 70 && rsi_min15 > 70 && rsi_min30 > 70 && !emaup) {
			if(getState() == StrategyState.OUT) {
				signal(candle.getTime(), SignalType.SHORT, 1.0);
				return;
			}
		}
		if(rsi_min30 < 50 && getState() == StrategyState.SHORT) {
			signal(candle.getTime(), SignalType.EXIT, 1.0);
			return;
		}

		//double avg = (rsi_min1 + rsi_min5 + rsi_min15 + rsi_min30) / 4;
	}
}
