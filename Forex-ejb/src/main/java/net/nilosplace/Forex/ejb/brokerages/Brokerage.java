package net.nilosplace.Forex.ejb.brokerages;

import net.nilosplace.Forex.ejb.entities.Order;
import net.nilosplace.Forex.ejb.managers.CacheManager;

public abstract class Brokerage {
	
	protected CacheManager cm;

	public abstract Order executeOrder(Order o);

	public void setCacheManager(CacheManager cm) {
		this.cm = cm;
	}

}
