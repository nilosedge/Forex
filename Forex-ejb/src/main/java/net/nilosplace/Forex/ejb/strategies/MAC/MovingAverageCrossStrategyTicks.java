package net.nilosplace.Forex.ejb.strategies.MAC;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;

import org.jboss.logging.Logger;

public class MovingAverageCrossStrategyTicks extends MovingAverageCrossStrategy {

	private int short_window = 100;
	private int long_window = 400;

	private Logger log = Logger.getLogger(getClass());

	@Override
	public void candle(Candle candle) {
		runStrategy(CacheType.TICKS, short_window, long_window, candle);
	}

}
