package net.nilosplace.Forex.ejb.oanda;

import org.jboss.logging.Logger;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.oanda.dto.OandaAccountInfo;
import net.nilosplace.Forex.ejb.oanda.dto.OandaCandles;
import net.nilosplace.Forex.ejb.oanda.dto.OandaOrder;
import net.nilosplace.Forex.ejb.oanda.enums.OandaSystemType;
import net.nilosplace.Forex.ejb.util.FileProperty;
import si.mazi.rescu.RestProxyFactory;

public class OandaAPI {

	private Logger log = Logger.getLogger(getClass());
	private OandaApiInterface oandaInterface;
	private OandaSystemType type;
	private FileProperty config = FileProperty.OANDAPROPERTIESFILE;
	
	public OandaAPI(OandaSystemType type) {
		this.type = type;
		oandaInterface = RestProxyFactory.createProxy(OandaApiInterface.class, config.getValue(type.getApiUrl()));
		//oandaInterface.init(type);
	}
	
	public OandaOrder createOrder(OandaOrder o) {
		OandaOrder order = oandaInterface.createOrder("Bearer " + config.getValue(type.getAccessToken()), config.getValue(type.getAccountId()), o.getInstrument(), (int)o.getUnits(), o.getSide(), o.getType().toString());
		return order;
	}
	
	public OandaAccountInfo getAccountInfo(String accountId) {
		OandaAccountInfo oai = oandaInterface.getAccountInfo("Bearer " + config.getValue(type.getAccessToken()), accountId);
		return oai;
	}
	
	public OandaCandles getCandles(String instrument, CacheType cacheType, int count) {
		OandaCandles oc = oandaInterface.getCandles("Bearer " + config.getValue(type.getAccessToken()), instrument, cacheType.getOandaName(), count);
		oc.setSource(DataSourceType.OANDALIVETICKS);
		oc.setTimeScale(cacheType);
		return oc;
	}
}
