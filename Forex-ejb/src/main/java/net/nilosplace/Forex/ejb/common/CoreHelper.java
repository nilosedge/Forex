package net.nilosplace.Forex.ejb.common;

import net.nilosplace.Forex.ejb.exceptions.TALibDataException;

import org.jboss.logging.Logger;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class CoreHelper {

	private static Logger log = Logger.getLogger(CoreHelper.class);

	public static double sma(double[] prices, int window) throws TALibDataException {
		double[] out = new double[prices.length];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		RetCode retCode = c.sma(0, prices.length - 1, prices, window, begin, length, out);
		if (retCode == RetCode.Success) {
			return out[length.value - 1];
		}
		throw new TALibDataException(retCode);
	}

	public static double rsi(double[] prices, int window) throws TALibDataException {
		double[] out = new double[prices.length];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		RetCode retCode = c.rsi(0, prices.length - 1, prices, window, begin, length, out);
		if (retCode == RetCode.Success) {
			if(length.value == 0) {
				return out[length.value];
			} else {
				return out[length.value - 1];
			}
		} 
		throw new TALibDataException(retCode);
	}
	
	public static double ema(double[] prices, int window) throws TALibDataException {
		double[] out = new double[prices.length];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		RetCode retCode = c.ema(0, prices.length - 1, prices, window, begin, length, out);
		if (retCode == RetCode.Success) {
			return out[length.value - 1];
		}
		throw new TALibDataException(retCode);
	}
	
	public static double[] macd(double[] prices, int fast, int slow, int signal) throws TALibDataException {
		double[] outmacd = new double[prices.length];
		double[] outsig = new double[prices.length];
		double[] outhist = new double[prices.length];
		double[] ret = new double[3];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		
		RetCode retCode = c.macd(0, prices.length - 1, prices, fast, slow, signal, begin, length, outmacd, outsig, outhist);

		if (retCode == RetCode.Success) {
			ret[0] = outmacd[length.value - 1];
			ret[1] = outsig[length.value - 1];
			ret[2] = outhist[length.value - 1];
			return ret;
		}
		throw new TALibDataException(retCode);
	}
	
	public static boolean emaup(double[] prices, int window) throws TALibDataException {
		double[] out = new double[prices.length];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		RetCode retCode = c.ema(0, prices.length - 1, prices, window, begin, length, out);
		if (retCode == RetCode.Success) {
			if(out[length.value - 1] > out[length.value - 2]) {
				return true;
			} else {
				return false;
			}
		}
		throw new TALibDataException(retCode);
	}
	
	public static double tr(double[] highPrices, double[] lowPrices, double[] closePrices) throws TALibDataException {
		double[] out = new double[highPrices.length];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		
		RetCode retCode = c.trueRange(0, highPrices.length - 1, highPrices, lowPrices, closePrices, begin, length, out);
		if (retCode == RetCode.Success) {
			return out[length.value - 1];
		}
		throw new TALibDataException(retCode);
	}
	
	public static double atr(double[] highPrices, double[] lowPrices, double[] closePrices, int window) throws TALibDataException {
		double[] out = new double[highPrices.length];
		MInteger begin = new MInteger();
		MInteger length = new MInteger();
		Core c = new Core();
		
		RetCode retCode = c.atr(0, highPrices.length - 1, highPrices, lowPrices, closePrices, window, begin, length, out);
		if (retCode == RetCode.Success) {
			return out[length.value - 1];
		}
		throw new TALibDataException(retCode);
	}
}