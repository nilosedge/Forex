package net.nilosplace.Forex.ejb.entities.myfxbook;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MyFXBookSymbol implements Serializable {

	private static final long serialVersionUID = 3464533600250585180L;
	private Long id;
    private String name = "";
	private int shortPercentage;
	private int longPercentage;
	private BigDecimal shortVolume;
	private BigDecimal longVolume;
	private int longPositions;
	private int shortPositions;
	private int totalPositions;
	private BigDecimal avgShortPrice;
	private BigDecimal avgLongPrice;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getShortPercentage() {
		return shortPercentage;
	}
	public void setShortPercentage(int shortPercentage) {
		this.shortPercentage = shortPercentage;
	}
	public int getLongPercentage() {
		return longPercentage;
	}
	public void setLongPercentage(int longPercentage) {
		this.longPercentage = longPercentage;
	}
	public BigDecimal getShortVolume() {
		return shortVolume;
	}
	public void setShortVolume(BigDecimal shortVolume) {
		this.shortVolume = shortVolume;
	}
	public BigDecimal getLongVolume() {
		return longVolume;
	}
	public void setLongVolume(BigDecimal longVolume) {
		this.longVolume = longVolume;
	}
	public int getLongPositions() {
		return longPositions;
	}
	public void setLongPositions(int longPositions) {
		this.longPositions = longPositions;
	}
	public int getShortPositions() {
		return shortPositions;
	}
	public void setShortPositions(int shortPositions) {
		this.shortPositions = shortPositions;
	}
	public int getTotalPositions() {
		return totalPositions;
	}
	public void setTotalPositions(int totalPositions) {
		this.totalPositions = totalPositions;
	}
	public BigDecimal getAvgShortPrice() {
		return avgShortPrice;
	}
	public void setAvgShortPrice(BigDecimal avgShortPrice) {
		this.avgShortPrice = avgShortPrice;
	}
	public BigDecimal getAvgLongPrice() {
		return avgLongPrice;
	}
	public void setAvgLongPrice(BigDecimal avgLongPrice) {
		this.avgLongPrice = avgLongPrice;
	}
    
}
