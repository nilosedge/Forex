package net.nilosplace.Forex.ejb.util;

import java.util.HashMap;

import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.portfolios.Portfolio;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

import org.infinispan.Cache;
import org.infinispan.manager.CacheContainer;
import org.jboss.logging.Logger;

public class CacheConnection {
	
	private static int cacheSize = 1000;
	protected static Logger log = Logger.getLogger(CacheConnection.class);
	private static CacheContainer containerPriceData = null;
	private static CacheContainer containerProtfolioData = null;
	//private static Cache<String, HashMap<String, HashMap<String, LimitedQueue<Candle>>>> priceDataCache;
	
	public static Cache<String, HashMap<String, HashMap<String, LimitedQueue<Candle>>>> getPriceDataCache() {
		if(containerPriceData == null) {
			containerPriceData = ServerResources.lookupResource(LookupResource.PRICEDATA);
			if(containerPriceData == null) {
				log.debug("Cache lookup failed and is null");
				return null;
			}
		}
		return containerPriceData.getCache();
	}
	
	public static Cache<String, Portfolio> getPortFolioDataCache() {
		if(containerProtfolioData == null) {
			containerProtfolioData = ServerResources.lookupResource(LookupResource.PORTFOLIODATA);
			if(containerProtfolioData == null) {
				log.debug("Cache lookup failed and is null");
				return null;
			}
		}
		return containerProtfolioData.getCache();
	}
	
	public static Portfolio getPortFolioDataCache(DataSourceType source) {
		Cache<String, Portfolio> c = getPortFolioDataCache();
		Portfolio p = c.get(source.getTypeName());
		if(p == null) {
			// Each Trading Source will get its own 100K this should be (live, test, dev, etc...)
			p = new Portfolio(100000.0);
			c.put(source.getTypeName(), p);
		}
		return p;
	}
	
	public static LimitedQueue<Candle> getPriceDataCache(String key1, String key2, String key3) {
		//log.info("Getting cahce for: Key1: " + key1 + " Key2: " + key2 + " Key3: " + key3);
		
		Cache<String, HashMap<String, HashMap<String, LimitedQueue<Candle>>>> c = getPriceDataCache();
		//log.info("Cache Recieved: " + c);
		
		HashMap<String, HashMap<String, LimitedQueue<Candle>>> sourceCache = c.get(key1);
		//log.info("Source Cache: " + sourceCache);
		if(sourceCache == null) {
			//log.info("Source Cache not found:");
			sourceCache = new HashMap<String, HashMap<String, LimitedQueue<Candle>>>();
			c.put(key1, sourceCache);
		}
		
		HashMap<String, LimitedQueue<Candle>> instrumentCache = sourceCache.get(key2);
		//log.info("Instrument Cache: " + instrumentCache);
		if(instrumentCache == null) {
			//log.info("Instrument Cache not found:");
			instrumentCache = new HashMap<String, LimitedQueue<Candle>>();
			sourceCache.put(key2, instrumentCache);
		}
		
		if(!instrumentCache.containsKey(key3)) {
			//log.info("Queue<Candle> not found:");
			LimitedQueue<Candle> queue = new LimitedQueue<Candle>(cacheSize);
			instrumentCache.put(key3, queue);
			return queue;
		} else {
			LimitedQueue<Candle> queue = instrumentCache.get(key3);
			//log.info("Queue<Candle>:" + queue);
			return queue;
		}

	}
	
}
