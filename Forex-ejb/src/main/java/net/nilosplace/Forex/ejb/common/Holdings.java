package net.nilosplace.Forex.ejb.common;

import java.util.Date;
import java.util.HashMap;

import net.nilosplace.Forex.ejb.common.events.FillEvent;
import net.nilosplace.Forex.ejb.common.events.FillEvent.FillType;


public class Holdings {
	
	private Date currentDateTime;
	private HashMap<String, Double> assetPositions = new HashMap<String, Double>();
	private HashMap<String, Double> assetHoldings = new HashMap<String, Double>();
	private double commission;
	private double cash;
	private double total;
	
	public Date getCurrentDateTime() {
		return currentDateTime;
	}
	public void setCurrentDateTime(Date currentDateTime) {
		this.currentDateTime = currentDateTime;
	}
	public HashMap<String, Double> getAssetPositions() {
		return assetPositions;
	}
	public void setAssetPositions(HashMap<String, Double> assetPositions) {
		this.assetPositions = assetPositions;
	}
	public HashMap<String, Double> getAssetHoldings() {
		return assetHoldings;
	}
	public void setAssetHoldings(HashMap<String, Double> assetHoldings) {
		this.assetHoldings = assetHoldings;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}
	public double getCash() {
		return cash;
	}
	public void setCash(double cash) {
		this.cash = cash;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	public double getCurrentPositionQuantity(String instrument) {
		return assetPositions.get(instrument);
	}
	public void setCurrentPositionQuantity(String instrument, double qty) {
		double p = getCurrentPositionQuantity(instrument);
		assetPositions.put(instrument, p + qty);
	}
	
	public double getCurrentHoldingsQuantity(String instrument) {
		return assetHoldings.get(instrument);
	}
	public void setCurrentHoldingsQuantity(String instrument, double cost) {
		double d = getCurrentHoldingsQuantity(instrument);
		assetHoldings.put(instrument, d + cost);
	}
	
	public void updateCurrentCost(FillEvent f) {
		if(f.getFillType() == FillType.BUY) {
			setCurrentHoldingsQuantity(f.getInstrument(), f.getCost());
		} else if(f.getFillType() == FillType.SELL) {
			setCurrentHoldingsQuantity(f.getInstrument(), -f.getCost());
		}
		commission += f.getCommission();
		cash -= f.getCommission() + f.getCost();
		total -= f.getCommission() + f.getCost();		
	}
	
	public void updateCurrentPositionQuantity(FillEvent f) {
		if(f.getFillType() == FillType.BUY) {
			setCurrentPositionQuantity(f.getInstrument(), f.getUnits());
		} else if(f.getFillType() == FillType.SELL) {
			setCurrentPositionQuantity(f.getInstrument(), -f.getUnits());
		}
	}
}
