package net.nilosplace.Forex.ejb.singletons;

import java.io.Serializable;
import java.util.HashMap;

import javax.ejb.Singleton;

import net.nilosplace.Forex.ejb.util.QueueContainer;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

@Singleton
public class EventConnection {
	
	private HashMap<String, QueueContainer> map = new HashMap<String, QueueContainer>();
	
	public void sendObject(LookupResource resource, Serializable object) {
		QueueContainer q = getQueueContainer(resource);
		q.sendObject(object);
	}
	
	private QueueContainer getQueueContainer(LookupResource resource) {
		if(!map.containsKey(resource.get_jndiLookupName())) {
			QueueContainer qc = new QueueContainer(resource);
			map.put(resource.get_jndiLookupName(), qc);
		}
		return map.get(resource.get_jndiLookupName());
	}
}
