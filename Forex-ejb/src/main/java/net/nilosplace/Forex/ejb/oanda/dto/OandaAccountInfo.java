package net.nilosplace.Forex.ejb.oanda.dto;

import java.math.BigDecimal;

public class OandaAccountInfo {

	private int id;
	private String accountName;
	private BigDecimal balance;
	private BigDecimal unrealizedPl;
	private BigDecimal realizedPl;
	private BigDecimal marginUsed;
	private BigDecimal marginAvail;
	private int openTrades;
	private int openOrders;
	private BigDecimal marginRate;
	private String accountCurrency;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getUnrealizedPl() {
		return unrealizedPl;
	}
	public void setUnrealizedPl(BigDecimal unrealizedPl) {
		this.unrealizedPl = unrealizedPl;
	}
	public BigDecimal getRealizedPl() {
		return realizedPl;
	}
	public void setRealizedPl(BigDecimal realizedPl) {
		this.realizedPl = realizedPl;
	}
	public BigDecimal getMarginUsed() {
		return marginUsed;
	}
	public void setMarginUsed(BigDecimal marginUsed) {
		this.marginUsed = marginUsed;
	}
	public BigDecimal getMarginAvail() {
		return marginAvail;
	}
	public void setMarginAvail(BigDecimal marginAvail) {
		this.marginAvail = marginAvail;
	}
	public int getOpenTrades() {
		return openTrades;
	}
	public void setOpenTrades(int openTrades) {
		this.openTrades = openTrades;
	}
	public int getOpenOrders() {
		return openOrders;
	}
	public void setOpenOrders(int openOrders) {
		this.openOrders = openOrders;
	}
	public BigDecimal getMarginRate() {
		return marginRate;
	}
	public void setMarginRate(BigDecimal marginRate) {
		this.marginRate = marginRate;
	}
	public String getAccountCurrency() {
		return accountCurrency;
	}
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
}
