package net.nilosplace.Forex.ejb.oanda;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import net.nilosplace.Forex.ejb.oanda.dto.OandaAccountInfo;
import net.nilosplace.Forex.ejb.oanda.dto.OandaCandles;
import net.nilosplace.Forex.ejb.oanda.dto.OandaOrder;
import net.nilosplace.Forex.ejb.oanda.dto.OandaTrade;

@Path("v1")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public interface OandaApiInterface {

	@POST
	@Path("accounts/{accountId}/orders")
	public OandaOrder createOrder(
			@HeaderParam("Authorization") String token,
			@PathParam("accountId") String accountId,
			@FormParam("instrument") String instrument,
			@FormParam("units") int units,
			@FormParam("side") String side,
			@FormParam("type") String type
	);

	@GET
	@Path("accounts/{account_id}/orders/")
	public OandaOrder[] getOrders(
			@HeaderParam("Authorization") String token,
			@QueryParam("account_id") String account_id
	);
	
	@GET
	@Path("accounts/{accountId}")
	public OandaAccountInfo getAccountInfo(
			@HeaderParam("Authorization") String token,
			@PathParam("accountId") String accountId
	);

	@GET
	@Path("candles")
	public OandaCandles getCandles(
			@HeaderParam("Authorization") String token,
			@QueryParam("instrument") String instrument,
			@QueryParam("granularity") String granularity,
			@QueryParam("count") int count
	);

	@GET
	@Path("accounts/{accountId}/trades")
	public OandaTrade[] getTrades(
			@HeaderParam("Authorization") String token,
			@PathParam("accountId") String accountId
	);
	

}
