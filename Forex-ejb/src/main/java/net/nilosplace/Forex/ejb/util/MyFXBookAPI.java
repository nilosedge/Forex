package net.nilosplace.Forex.ejb.util;

import java.util.HashMap;

import net.nilosplace.Forex.ejb.entities.myfxbook.MyFXBookOutlook;
import net.nilosplace.Forex.ejb.entities.myfxbook.MyFXBookSession;

import org.jboss.logging.Logger;

public class MyFXBookAPI {

	private String email = "";
	private String password = "";
	private boolean debug;
	private ModelMapper model = new ModelMapper();
	private MyFXBookSession session;
	private Logger log = Logger.getLogger(getClass());
	private MyFXBookConnection mfbc = new MyFXBookConnection();
	
	public MyFXBookAPI(String email, String password, boolean debug) {
		this.email = email;
		this.debug = debug;
		this.password = password;
		session = connect(email, password);
	}

	public MyFXBookSession connect(String email, String password) {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("email", email);
		params.put("password", password);
		String out = mfbc.runCall("login.json", params);
		MyFXBookSession myfx = (MyFXBookSession)model.getObject(out, MyFXBookSession.class);
		if(debug) {
			log.info("MyFx Username: " + email);
			log.info("MyFx Password: " + password);
			log.info("MyFx Message: " + myfx.getMessage());
			log.info("MyFx Session: " + myfx.getSession());
			log.info("MyFx Error: " + myfx.getError());
		}
		return myfx;
	}
	
	public MyFXBookOutlook getCommunityOutlook() {
		HashMap<String, String> params = new HashMap<String, String>();
		if(session == null) {
			session = connect(email, password);
			if(session == null) {
				return null;
			}
		}
		params.put("session", session.getSession());
		String out = mfbc.runCall("get-community-outlook.json", params);
		MyFXBookOutlook mfo = (MyFXBookOutlook) model.getObject(out, MyFXBookOutlook.class);
		return mfo;
	}
	
}
