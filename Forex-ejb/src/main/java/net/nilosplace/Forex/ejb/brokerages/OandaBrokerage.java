package net.nilosplace.Forex.ejb.brokerages;

import net.nilosplace.Forex.ejb.entities.Order;
import net.nilosplace.Forex.ejb.oanda.OandaAPI;
import net.nilosplace.Forex.ejb.oanda.dto.OandaOrder;
import net.nilosplace.Forex.ejb.oanda.dto.OandaOrder.OandaOrderType;
import net.nilosplace.Forex.ejb.oanda.dto.OandaTrade;
import net.nilosplace.Forex.ejb.oanda.enums.OandaSystemType;

import org.jboss.logging.Logger;

public class OandaBrokerage extends Brokerage {

	private Logger log = Logger.getLogger(getClass());

	private OandaAPI oa;

	public OandaBrokerage() {
		init(OandaSystemType.PRACTICE);
	}

	public OandaBrokerage(OandaSystemType type) {
		oa = new OandaAPI(type);
	}

	public void init(OandaSystemType type) {
		oa = new OandaAPI(type);
	}

	@Override
	public Order executeOrder(Order o) {

		//o.print("Executing Order");
		OandaOrder oo = new OandaOrder();
		oo.setInstrument(o.getInstrument());
		oo.setType(OandaOrderType.valueOf(o.getOrderType().toString().toLowerCase()));
		oo.setSide(o.getFillType().toString().toLowerCase());

		oo.setUnits(o.getUnits() * 100);
		//oo.print("Sending Order");
		OandaOrder oo2 = oa.createOrder(oo);

		//oo2.print("Recieved Oanda Order");

		Order oret = new Order(o.getSource(), o.getStrategy(), o.getInstrument(), o.getOrderType(), 0, o.getFillType());
		
		if(oo2.getTradeOpened() != null && oo2.getTradeOpened().getUnits() > 0) {
			oret.setUnits((oo2.getTradeOpened().getUnits() / 100));
		}

		if(oo2.getTradeReduced() != null && oo2.getTradeReduced().getUnits() > 0) {
			oret.setUnits((oo2.getTradeReduced().getUnits() / 100));
		}
		
		if(oo2.getTradesClosed().size() > 0) {
			double units = 0;
			for(OandaTrade t: oo2.getTradesClosed()) {
				units += (t.getUnits() / 100);
			}
			oret.setUnits(units);
		}
		
		oret.setPrice(oo2.getPrice());
		
//		if(tradeOpened != null) return tradeOpened.getUnits();
//		if(tradeReduced != null) return -tradeReduced.getUnits();
//		if(tradesClosed != null) {
//			double ret = 0;
//			for(OandaTrade t: tradesClosed) {
//				ret += t.getUnits();
//			}
//			return - ret;
//		}
//		
//		@Override
//		public int getId() {
//			if(tradeOpened != null) {
//				return tradeOpened.getId();
//			}
//			if(tradeReduced != null) {
//				return tradeReduced.getId();
//			}
//			return 0;
//		}

		//oret.print("Returning Order");
		return oret;
	}

}