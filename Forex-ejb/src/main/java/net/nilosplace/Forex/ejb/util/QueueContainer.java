package net.nilosplace.Forex.ejb.util;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

public class QueueContainer {
	
	InitialContext context;
	ConnectionFactory conf;
	Connection connection;
	Destination destination;
	Session session;
	MessageProducer producer;
	
	public QueueContainer(LookupResource resource) {

		try {
			context = new InitialContext();
			conf = (ConnectionFactory)context.lookup("/ConnectionFactory");
			connection = conf.createConnection();
			destination = (Destination)context.lookup(resource.get_jndiLookupName());
			session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
			producer = session.createProducer(destination);
			producer.setDisableMessageID(true);
			producer.setDisableMessageTimestamp(true);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void sendObject(Serializable object) {
		try {
			ObjectMessage messageObject = session.createObjectMessage();
			messageObject.setObject(object);
			producer.send(messageObject);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
