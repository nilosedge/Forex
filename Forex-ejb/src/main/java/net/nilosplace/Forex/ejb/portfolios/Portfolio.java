package net.nilosplace.Forex.ejb.portfolios;

import java.math.BigDecimal;
import java.util.HashMap;

import net.nilosplace.Forex.ejb.entities.Position;

import org.jboss.logging.Logger;

public class Portfolio {

	private Logger log = Logger.getLogger(getClass());
	
	// Strategy, Instrument,
	private HashMap<String, HashMap<String, Position>> positions = new HashMap<String, HashMap<String, Position>>();
	private HashMap<String, Double> currentPrices = new HashMap<String, Double>();
	private BigDecimal capital;
	private BigDecimal lastCapital;

	public Portfolio(double initialCapital) {
		capital = BigDecimal.valueOf(initialCapital);
	}
	
	public HashMap<String, Double> getCurrentPrices() {
		return currentPrices;
	}
	public void setCurrentPrices(HashMap<String, Double> currentPrices) {
		this.currentPrices = currentPrices;
	}
	public HashMap<String, HashMap<String, Position>> getPositions() {
		return positions;
	}
	public void setPositions(HashMap<String, HashMap<String, Position>> positions) {
		this.positions = positions;
	}
	public BigDecimal getCapital() {
		return capital;
	}
	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}

	public Position getPosition(String strategy, String instrument) {
		HashMap<String, Position> strategyPositions = positions.get(strategy);
		if(strategyPositions == null) {
			strategyPositions = new HashMap<String, Position>();
			positions.put(strategy, strategyPositions);
		}
		
		Position p = strategyPositions.get(instrument);
		if(p == null) {
			p = new Position();
			strategyPositions.put(instrument, p);
		}
		return p;
	}

	public void printPositions() {
		PositionPrinter.print(positions);
		printCurrnetValue();
	}

	public void printCurrnetValue() {
		double tp = 0;
		for(String strategy: positions.keySet()) {
			for(String instrument: positions.get(strategy).keySet()) {
				Position p = positions.get(strategy).get(instrument);
				double cp = currentPrices.get(instrument);
				//log.info(strategy + ": " + instrument + ": " + " position value: " + (p.getUnits() * cp));
				tp += (p.getUnits() * cp);
			}
		}
		if(lastCapital == null || lastCapital.compareTo(capital.add(BigDecimal.valueOf(tp))) != 0) {
			log.info("initialCapital: " + capital + " Total Value: " + (capital.add(BigDecimal.valueOf(tp))));
			lastCapital = capital.add(BigDecimal.valueOf(tp));
		}
	}
}
