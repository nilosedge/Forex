package net.nilosplace.Forex.ejb.strategies.RSI;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;

import org.jboss.logging.Logger;

public class RSISwingStrategyMin5 extends RSISwingStrategy {
	
	private Logger log = Logger.getLogger(getClass());
	
	@Override
	protected void candle(Candle candle) {
		runStrategy(CacheType.MIN5, candle);
	}
}
