package net.nilosplace.Forex.ejb.common.events;

import java.io.Serializable;
import java.util.Date;

import net.nilosplace.Forex.ejb.common.types.DataSourceType;

import org.jboss.logging.Logger;

public class SignalEvent implements Serializable {

	private Logger log = Logger.getLogger(getClass());
	
	private static final long serialVersionUID = -1695635165087806964L;
	private DataSourceType source;
	private String strategyName;
	private String instrument;
	private Date time;
	private SignalType signalType;
	private double strength;

	public SignalEvent(DataSourceType source, String strategyName, String instrument, Date time, SignalType type, double strength) {
		this.source = source;
		this.strategyName = strategyName;
		this.instrument = instrument;
		this.time = time;
		this.signalType = type;
		this.strength = strength;
	}
	
	public DataSourceType getSource() {
		return source;
	}
	public void setSource(DataSourceType source) {
		this.source = source;
	}
	public String getStrategyName() {
		return strategyName;
	}
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public SignalType getSignalType() {
		return signalType;
	}
	public void setSignalType(SignalType signalType) {
		this.signalType = signalType;
	}
	public double getStrength() {
		return strength;
	}
	public void setStrength(double strength) {
		this.strength = strength;
	}
	
	public enum SignalType {
		LONG,
		SHORT,
		EXIT,
		;
	}
	
	public void print() {
		log.info("Source: " + source);
		log.info("strategyName: " + strategyName);
		log.info("instrument: " + instrument);
		//log.info("time: " + time);
		log.info("signalType: " + signalType);
		//log.info("strength: " + strength);
	}
}
