package net.nilosplace.Forex.ejb.strategies.RSI;

import net.nilosplace.Forex.ejb.common.events.SignalEvent.SignalType;
import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;
import net.nilosplace.Forex.ejb.strategies.Strategy;

import org.jboss.logging.Logger;

public abstract class RSISwingStrategy extends Strategy {
	
	protected abstract void candle(Candle candle);
	private Logger log = Logger.getLogger(getClass());
	
	/*
	 * Basic Strategy
	 * The main idea behind this strategy is that we want to buy the market as it has pull back
	 * extremes. We also want our trades to be short term and exit our trades as the Relative
	 * Strength Index crosses above 40.
	 * 
	 * In this strategy, we're trying to take advantage of a move to the downside that has gone
	 * too far and too fast. Sellers have panicked pushing prices down and there is no one at
	 * least in the short term left to sell. We are also saying with our rules that the return
	 * distribution is more normal above the 200 day exponential moving average
	 * 
	 * Buy Signal 1:
	 * 		Condition 1: (Close > EMA(200))
	 * 		Condition 2: (RSI < 30)
	 * 
	 * Buy Signal 2:
	 * 		Condition 1: Close < EMA(200)
	 * 		Condition 2: Close < Open
	 * 		Condition 3: RSI < 25
	 * 		Condition 4: (Open - Close) > (.7 * (High - Low))
	 * 
	 * Exit:
	 * 		Condition: RSI > 40
	 * 
	 */
	
	protected void runStrategy(CacheType cacheType, Candle candle) {
		
		Candle current = getLatestBar(cacheType);
		double open = current.getOpenBid();
		double close = current.getCloseBid();
		double high = current.getHighBid();
		double low = current.getLowBid();

		double ema200, rsi;
		try {
			ema200 = ema(cacheType, 200);
			rsi = rsi(cacheType, 5, true);
		} catch (DataUnavailableException e1) {
			return;
		} catch (TALibDataException e1) {
			return;
		}

		//log.trace("Current: " + current);
		//log.trace("Open: " + open + " High: " + high + " Low: " + low + " Close: " + close + " EMA(200): " + ema200 + " RSI(5): " + rsi);
		
		//log.trace("Buy condition 1: " + (close > ema200 && rsi < 30));
		//log.trace("Buy condition 1: (" + close + " > " + ema200 + "): " + (close > ema200));
		//log.trace("Buy condition 1: (" + rsi + " < " + 30 + "): " + (rsi < 30));
		
		// Buy condition 1
		if(close > ema200 && rsi < 30) {
			if(getState() == StrategyState.OUT) {
				signal(candle.getTime(), SignalType.LONG, 1.0);
				return;
			}
		}
		
		//log.trace("Buy condition 2: " + ((close < ema200) && close < open && rsi < 25 && ((open - close) > (.7 * (high - low)))));
		//log.trace("Buy condition 2: (" + close + " < " + ema200 + "): " + (close < ema200));
		//log.trace("Buy condition 2: (" + close + " < " + open + "): " + (close < open));
		//log.trace("Buy condition 2: (" + rsi + " < " + 25 + "): " + (rsi < 25));
		//log.trace("Buy condition 2: (" + (open - close) + " > " + (.7 * (high - low)) + "): " + ((open - close) > (.7 * (high - low))));
		
		// Buy Condition 2
		if(
				(close < ema200) &&
				close < open && 
				rsi < 25 &&
				((open - close) > (.7 * (high - low)))
		) {
			if(getState() == StrategyState.OUT) {
				signal(candle.getTime(), SignalType.LONG, 1.0);
				return;
			}
		}

		if(getState() != StrategyState.OUT) {
			//log.trace("Exit Condition: (" + rsi + " > " + 40 + "): " + (rsi > 40));
			if(rsi > 40) {
				signal(candle.getTime(), SignalType.EXIT, 1.0);
				return;
			}
		}
	}
	
}
