package net.nilosplace.Forex.ejb.portfolios;

import java.util.HashMap;

import org.jboss.logging.Logger;

import net.nilosplace.Forex.ejb.entities.Position;

public class PositionPrinter {

	private static Logger log = Logger.getLogger(PositionPrinter.class);
	
	public static void print(HashMap<String, HashMap<String, Position>> positions) {
		StringBuffer string = new StringBuffer();
		string.append("\nPositions: \n");
		for(String key1: positions.keySet()) {
			string.append("\tStrategy: " + key1 + "\n");
			for(String key2: positions.get(key1).keySet()) {
				string.append("\t\tInstrument: " + key2 + "\n");
				string.append("\t\t\tUnits: " + positions.get(key1).get(key2).getUnits() + "\n");
			}
		}
		log.info(string);
	}

}
