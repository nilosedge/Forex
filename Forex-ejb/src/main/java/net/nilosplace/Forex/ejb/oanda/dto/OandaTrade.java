package net.nilosplace.Forex.ejb.oanda.dto;

import java.io.Serializable;
import java.util.Date;

import org.jboss.logging.Logger;

public class OandaTrade implements Serializable {

	private static final long serialVersionUID = -4836148127001918287L;
	private Logger log = Logger.getLogger(getClass());
	private int id;
	private double interest;
	private double pl;
	private double units;
	private OandaSideType side;
	private String instrument;
	private Date time;
	private double price;
	private double takeProfit;
	private double stopLoss;
	private double trailingStop;
	private double trailingAmount;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public double getPl() {
		return pl;
	}
	public void setPl(double pl) {
		this.pl = pl;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public OandaSideType getSide() {
		return side;
	}
	public void setSide(OandaSideType side) {
		this.side = side;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getTakeProfit() {
		return takeProfit;
	}
	public void setTakeProfit(double takeProfit) {
		this.takeProfit = takeProfit;
	}
	public double getStopLoss() {
		return stopLoss;
	}
	public void setStopLoss(double stopLoss) {
		this.stopLoss = stopLoss;
	}
	public double getTrailingStop() {
		return trailingStop;
	}
	public void setTrailingStop(double trailingStop) {
		this.trailingStop = trailingStop;
	}
	public double getTrailingAmount() {
		return trailingAmount;
	}
	public void setTrailingAmount(double trailingAmount) {
		this.trailingAmount = trailingAmount;
	}
	
	public void print() {
		print(getClass().getName());
	}
	
	public void print(String in) {
		log.info(in + ":");
		log.info("id: " + id);
		log.info("interest: " + interest);
		log.info("pl: " + pl);
		log.info("units: " + units);
		log.info("side: " + side);
		log.info("instrument: " + instrument);
		log.info("time: " + time);
		log.info("price: " + price);
		log.info("takeProfit: " + takeProfit);
		log.info("stopLoss: " + stopLoss);
		log.info("trailingStop: " + trailingStop);
		log.info("trailingAmount: " + trailingAmount);
	}
}
