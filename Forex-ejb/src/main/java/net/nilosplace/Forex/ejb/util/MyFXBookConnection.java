package net.nilosplace.Forex.ejb.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.jboss.logging.Logger;

public class MyFXBookConnection {
	
	private HttpURLConnection con = null;
	private URL url;
	private String baseURL = "http://www.myfxbook.com/api/";
	private Logger log = Logger.getLogger(getClass());

	public String runCall(String api, HashMap<String, String> params) {
		try {
			url = buildUrl(api, params);
			con = (HttpURLConnection) url.openConnection();
            return IOUtils.toString(con.getInputStream());
		} catch (IOException e) {
			log.warn("Unable to connect to: " + url.getPath());
		}
		return null;
	}

	private URL buildUrl(String api, HashMap<String, String> params) throws MalformedURLException {
		String query = "";
		for(String key: params.keySet()) {
			if(query.length() == 0) {
				query += key + "=" + params.get(key);
			} else {
				query += "&" + key + "=" + params.get(key);
			}
		}
		return new URL(baseURL + api + "?" + query);
	}
}
