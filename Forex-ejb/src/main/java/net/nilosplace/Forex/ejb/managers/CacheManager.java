package net.nilosplace.Forex.ejb.managers;

import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;

import net.nilosplace.Forex.ejb.common.CoreHelper;
import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;
import net.nilosplace.Forex.ejb.oanda.dto.OandaCandles;
import net.nilosplace.Forex.ejb.util.CacheConnection;
import net.nilosplace.Forex.ejb.util.CandleUtil;
import net.nilosplace.Forex.ejb.util.LimitedQueue;

import org.infinispan.Cache;
import org.jboss.logging.Logger;

import com.tictactec.ta.lib.CandleSettingType;

@Stateless
public class CacheManager {
	
	private Logger log = Logger.getLogger(getClass());

	public double sma(DataSourceType source, CacheType cacheType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(source.getTypeName(), instrument, cacheType.getName());
		List<Candle> candles = queue.getElements();
		if(candles.size() >= window) {
			double[] prices = new double[candles.size()];
			for(int i = 0; i < candles.size(); i++) {
				// TODO potential bias due to always using bid price?
				prices[i] = candles.get(i).getCloseBid();
			}
			return CoreHelper.sma(prices, window);
		} else {
			throw new DataUnavailableException(cacheType, candles.size(), window, "Not enough data");
		}
	}

	public double rsi(DataSourceType source, CacheType cacheType, String instrument, int window, boolean smooth) throws DataUnavailableException, TALibDataException {
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(source.getTypeName(), instrument, cacheType.getName());
		List<Candle> candles = queue.getElements();
		if(candles.size() > window) {
			double[] prices = new double[candles.size()];
			for(int i = 0; i < candles.size(); i++) {
				// TODO potential bias due to always using bid price?
				prices[i] = candles.get(i).getCloseBid();
			}
			return CoreHelper.rsi(prices, window);
		} else {
			throw new DataUnavailableException(cacheType, candles.size(), window, "Not enough data");
		}
	}

	public double ema(DataSourceType source, CacheType cacheType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(source.getTypeName(), instrument, cacheType.getName());
		List<Candle> candles = queue.getElements();
		if(candles.size() >= window) {
			double[] prices = new double[candles.size()];
			for(int i = 0; i < candles.size(); i++) {
				// TODO potential bias due to always using bid price?
				prices[i] = candles.get(i).getCloseBid();
			}
			return CoreHelper.ema(prices, window);
		} else {
			throw new DataUnavailableException(cacheType, candles.size(), window, "Not enough data");
		}
	}
	
	public boolean emaup(DataSourceType source, CacheType cacheType, String instrument, int window) throws DataUnavailableException, TALibDataException {
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(source.getTypeName(), instrument, cacheType.getName());
		List<Candle> candles = queue.getElements();
		
		if(candles.size() > window) {
			double[] prices = new double[candles.size()];
			for(int i = 0; i < candles.size(); i++) {
				// TODO potential bias due to always using bid price?
				prices[i] = candles.get(i).getCloseBid();
			}
			return CoreHelper.emaup(prices, window);
		} else {
			throw new DataUnavailableException(cacheType, candles.size(), window, "Not enough data");
		}
	}

	public void updateCache(OandaCandles candles) {
		log.info("Source: " + candles.getSource().getTypeName());
		log.info("Instrument: " + candles.getInstrument());
		log.info("Time Scale: " + candles.getTimeScale().getName());
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(candles.getSource().getTypeName(), candles.getInstrument(), candles.getTimeScale().getName());
		updateBars(queue, candles);
	}
	
	public void updateCache(Candle candle) {

		candle.setTimeScale(CacheType.TICKS);
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.MIN1);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.MIN5);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.MIN15);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.MIN30);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.HOUR1);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.HOUR4);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.DAY1);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.WEEK1);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
		
		candle.setTimeScale(CacheType.MONTH1);
		queue = CacheConnection.getPriceDataCache(candle.getSource().getTypeName(), candle.getInstrument(), candle.getTimeScale().getName());
		updateBars(queue, candle);
		//printQueue(queue, candle);
	}	
	

	public Candle getLatestCandle(DataSourceType source, CacheType type, String instrument) {
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(source.getTypeName(), instrument, type.getName());
		return queue.peekLast();
	}
	
	public int getBarSize(DataSourceType source, CacheType type, String instrument) {
		LimitedQueue<Candle> queue = CacheConnection.getPriceDataCache(source.getTypeName(), instrument, type.getName());
		return queue.size();
	}
	
	private void updateBars(LimitedQueue<Candle> queue, OandaCandles candles) {
		for(Candle c: candles.getCandles()) {
			queue.add(c);
		}
	}

	private void updateBars(LimitedQueue<Candle> queue, Candle candle) {
		Candle last = queue.peekLast();
		if(last == null) {
			queue.add(new Candle(candle));
		} else {
			//log.info("Candle: " + candle);
			//log.info("Time: " + candle.toBar(candle.getTimeScale()));
			//log.info("Last: " + last.toBar(candle.getTimeScale()));
			if(candle.toBar(candle.getTimeScale()).equals(last.toBar(candle.getTimeScale()))) {
				updateCandle(last, candle);
			} else {
				queue.add(new Candle(candle));
			}
		}
	}

	private void updateCandle(Candle last, Candle tick) {
		if(last.getHighBid() < tick.getHighBid()) {
			last.setHighBid(tick.getHighBid());
		}
		if(last.getLowBid() > tick.getLowBid()) {
			last.setLowBid(tick.getLowBid());
		}
		if(last.getHighAsk() < tick.getHighAsk()) {
			last.setHighAsk(tick.getHighAsk());
		}
		if(last.getLowAsk() > tick.getLowAsk()) {
			last.setLowAsk(tick.getLowAsk());
		}
		last.setCloseBid(tick.getCloseBid());
		last.setCloseAsk(tick.getCloseAsk());
		last.setTime(tick.getTime());
	}

	private void printQueue(LimitedQueue<Candle> queue, Candle candle) {
		//log.info(candle.getTimeScale() + ": Updating with: " + candle);
		String outh = "";
		String outl = "";
		String outo = "";
		String outc = "";
		for(Candle c: queue) {
			outh += c.getHighBid() + " ";
			outl += c.getLowBid() + " ";
			outo += c.getOpenBid() + " ";
			outc += c.getCloseBid() + " ";
		}
//		log.info("Prices: ");
//		log.info("High: " + outh);
//		log.info("Low: " + outl);
//		log.info("Open: " + outo);
//		log.info("Close: " + outc);
//		log.info("");
	}

	public void printCache() {
		Cache<String, HashMap<String, HashMap<String, LimitedQueue<Candle>>>> cache = CacheConnection.getPriceDataCache();
		log.info("{");
		for(String key1: cache.keySet()) {
			log.info("\tkey1: " + key1 + "{");
			for(String key2: cache.get(key1).keySet()) {
				log.info("\t\tkey2: " + key2 + "{");
				for(String key3: cache.get(key1).get(key2).keySet()) {
					log.info("\t\t\tkey3: " + key3 + "{[");
					LimitedQueue<Candle> queue = cache.get(key1).get(key2).get(key3);
					for(Candle c: queue) {
						log.info("\t\t\t\t{" + c.toString() + "}");
					}
					log.info("\t\t\t]}");
				}
				log.info("\t\t}");
			}
			log.info("\t}");
		}
		log.info("}");
	}

}
