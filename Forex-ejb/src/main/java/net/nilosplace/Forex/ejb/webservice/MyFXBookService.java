package net.nilosplace.Forex.ejb.webservice;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import net.nilosplace.Forex.ejb.entities.myfxbook.MyFXBookSymbol;
import net.nilosplace.Forex.ejb.managers.DataManager;

@Path("/myfx")
@RequestScoped
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class MyFXBookService {

	//private Logger log = Logger.getLogger(getClass());

	@Inject
	private DataManager dm;
	
	@GET
	@Path("/symbol/{symbol}")
	public Response getSymbol(@PathParam("symbol") String symbol) {
		List<MyFXBookSymbol> list = dm.getDataBySymbol(symbol);
		return Response.ok(list).build();
	}

}
