package net.nilosplace.Forex.ejb.webservice;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import net.nilosplace.Forex.ejb.dao.DatabasePropertyDao;
import net.nilosplace.Forex.ejb.entities.Property;

@Path("/system")
@RequestScoped
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class DatabasePropertyService {

	//private Logger log = Logger.getLogger(getClass());

	@Inject private DatabasePropertyDao dpd;
	
	@GET
	@Path("/property")
	public Response getAllSymbol() {
		List<Property> props = dpd.getAllProperties();
		return Response.ok(props).build();
	}
	
	@GET
	@Path("/property/id/{id}")
	public Response getSymbolById(@PathParam("id") long id) {
		Property prop = dpd.getProperty(id);
		return Response.ok(prop).build();
	}
	
	@GET
	@Path("/property/{name}")
	public Response getSymbol(@PathParam("name") String name) {
		Property prop = dpd.getProperty(name);
		return Response.ok(prop).build();
	}
	
	@POST
	@Path("/property/create")
	public Response createSystemProperty(Property input) {
		Property prop = dpd.createProperty(input.getName(), input.getValue());
		return Response.ok(prop).build();
	}
	
}
