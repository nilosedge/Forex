package net.nilosplace.Forex.ejb.oanda.dto;

public enum OandaSideType {
	sell,
	buy
}
