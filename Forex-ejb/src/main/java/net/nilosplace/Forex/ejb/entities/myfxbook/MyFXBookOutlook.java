package net.nilosplace.Forex.ejb.entities.myfxbook;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class MyFXBookOutlook implements Serializable {

	private static final long serialVersionUID = -7635265580552454290L;
	private Long id;
	private String error = "";
	private String message = "";
	private List<MyFXBookSymbol> symbols;
	private MyFXBookGeneralInfo general;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@OneToMany
	@JoinColumn(name="outlook_id")
	public List<MyFXBookSymbol> getSymbols() {
		return symbols;
	}
	public void setSymbols(List<MyFXBookSymbol> symbols) {
		this.symbols = symbols;
	}
	@OneToOne
	public MyFXBookGeneralInfo getGeneral() {
		return general;
	}
	public void setGeneral(MyFXBookGeneralInfo general) {
		this.general = general;
	}
}
