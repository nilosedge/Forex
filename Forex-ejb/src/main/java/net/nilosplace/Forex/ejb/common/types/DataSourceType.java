package net.nilosplace.Forex.ejb.common.types;

import net.nilosplace.Forex.ejb.brokerages.Brokerage;
import net.nilosplace.Forex.ejb.brokerages.CVSBrokerage;
import net.nilosplace.Forex.ejb.brokerages.OandaBrokerage;

public enum DataSourceType {

	OANDALIVETICKS("LiveOandaTickThread", new OandaBrokerage()) { },
	INPUTCVSFILE("InputFromCVS", new CVSBrokerage()) { },
	;

	private String typeName;
	private Brokerage brokerageClass;

	private DataSourceType(String typeName, Brokerage brokerageClass) {
		this.typeName = typeName;
		this.brokerageClass = brokerageClass;
	}
	
	public Brokerage getBrokerageClass() {
		return brokerageClass;
	}
	
	public String getTypeName() {
		return typeName;
	}
}
