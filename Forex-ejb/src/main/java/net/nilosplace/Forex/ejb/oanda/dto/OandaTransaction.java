package net.nilosplace.Forex.ejb.oanda.dto;

import java.io.Serializable;
import java.util.Date;

public class OandaTransaction implements Serializable {

	private static final long serialVersionUID = -5203648057195589782L;
	private int id;
	private Date time;
	private double accountBalance;
	private double price;
	private int accountId;
	private int tradeId;
	private OandaSideType side;
	private double interest;
	private OandaTrade tradeOpened;
	private double pl;
	private OandaTrade tradeReduced;
	private String instrument;
	private OandaTransactionType type;
	private double units;
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public int getTradeId() {
		return tradeId;
	}
	public void setTradeId(int tradeId) {
		this.tradeId = tradeId;
	}
	public OandaSideType getSide() {
		return side;
	}
	public void setSide(OandaSideType side) {
		this.side = side;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public OandaTrade getTradeOpened() {
		return tradeOpened;
	}
	public void setTradeOpened(OandaTrade tradeOpened) {
		this.tradeOpened = tradeOpened;
	}
	public double getPl() {
		return pl;
	}
	public void setPl(double pl) {
		this.pl = pl;
	}
	public OandaTrade getTradeReduced() {
		return tradeReduced;
	}
	public void setTradeReduced(OandaTrade tradeReduced) {
		this.tradeReduced = tradeReduced;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public OandaTransactionType getType() {
		return type;
	}
	public void setType(OandaTransactionType type) {
		this.type = type;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}

	public enum OandaTransactionType {
		MARGIN_CLOSEOUT,
		ORDER_FILLED,
		MARKET_ORDER_CREATE,
		DAILY_INTEREST,
		STOP_LOSS_FILLED,
		TAKE_PROFIT_FILLED,
		TRAILING_STOP_FILLED,
		ORDER_CANCEL,
		MARGIN_CALL_ENTER,
		MARGIN_CALL_EXIT,
		TRADE_CLOSE
		;
	}
}
