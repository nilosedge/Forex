package net.nilosplace.Forex.ejb.oanda.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;

public class OandaOrder implements Serializable {

	private static final long serialVersionUID = 8266392375465979177L;
	private Logger log = Logger.getLogger(getClass());
	private String instrument;
	private Date time;
	private double price;
	private OandaTrade tradeOpened;
	private OandaTrade tradeReduced;
	private List<OandaTrade> tradesClosed;
	private double units;
	private String side;
	private OandaOrderType type;
	
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public OandaTrade getTradeOpened() {
		return tradeOpened;
	}
	public void setTradeOpened(OandaTrade tradeOpened) {
		this.tradeOpened = tradeOpened;
	}
	public OandaTrade getTradeReduced() {
		return tradeReduced;
	}
	public void setTradeReduced(OandaTrade tradeReduced) {
		this.tradeReduced = tradeReduced;
	}
	public List<OandaTrade> getTradesClosed() {
		return tradesClosed;
	}
	public void setTradesClosed(List<OandaTrade> tradesClosed) {
		this.tradesClosed = tradesClosed;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public OandaOrderType getType() {
		return type;
	}
	public void setType(OandaOrderType type) {
		this.type = type;
	}
	
	public enum OandaOrderType {
		limit,
		stop,
		marketIfTouched,
		market,
		;
	}
	
	public void print() {
		print(getClass().getName());
	}

	public void print(String in) {
		log.info(in + ":");
		log.info("instrument: " + instrument);
		log.info("time: " + time);
		log.info("price: " + price);
		if(tradeOpened != null) tradeOpened.print("Trade Opened");
		if(tradeReduced != null) tradeReduced.print("Trade Reduced");
		if(tradesClosed != null) {
			for(OandaTrade t: tradesClosed) {
				t.print("Trade Closed");
			}
		}
		log.info("units: " + units);
		log.info("side: " + side);
		log.info("type: " + type);
	}

}
