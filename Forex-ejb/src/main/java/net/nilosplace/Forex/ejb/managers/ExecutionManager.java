package net.nilosplace.Forex.ejb.managers;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import net.nilosplace.Forex.ejb.brokerages.Brokerage;
import net.nilosplace.Forex.ejb.entities.Order;

@Stateless
public class ExecutionManager {
	
	@Inject
	CacheManager cm;

	private Logger log = Logger.getLogger(getClass());

	public Order executeOrder(Order o) {
		Brokerage b = o.getSource().getBrokerageClass();
		b.setCacheManager(cm);
		Order order = b.executeOrder(o);
		return order;
	}
}
