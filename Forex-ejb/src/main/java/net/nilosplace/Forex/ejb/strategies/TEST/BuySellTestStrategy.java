package net.nilosplace.Forex.ejb.strategies.TEST;

import org.jboss.logging.Logger;

import net.nilosplace.Forex.ejb.common.events.SignalEvent.SignalType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.strategies.Strategy;
import net.nilosplace.Forex.ejb.strategies.Strategy.StrategyState;

public class BuySellTestStrategy extends Strategy {

	private Logger log = Logger.getLogger(getClass());
	
	int count = 0;
	int orderdelay = 20;
	
	@Override
	protected void candle(Candle candle) {
		if(count > 0 && (count % orderdelay) == 0) {
			if(getState() == StrategyState.SHORT) {
				signal(candle.getTime(), SignalType.EXIT, 1.0);
			} else if(getState() == StrategyState.OUT) {
				signal(candle.getTime(), SignalType.SHORT, 1.0);
			}
		}
		count++;
	}
}
