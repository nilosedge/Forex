package net.nilosplace.Forex.ejb.exceptions;

import net.nilosplace.Forex.ejb.common.types.CacheType;

public class DataUnavailableException extends Exception {

	private static final long	serialVersionUID	= 3873695134614642984L;

	public DataUnavailableException(CacheType type, int dataSize, int windowSize, String message) {
		super(type + ": " + message + " DataSize: " + dataSize + " Window Size: " + windowSize);
	}

}
