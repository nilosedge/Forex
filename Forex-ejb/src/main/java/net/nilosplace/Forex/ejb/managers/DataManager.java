package net.nilosplace.Forex.ejb.managers;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import net.nilosplace.Forex.ejb.common.events.MarketEvent;
import net.nilosplace.Forex.ejb.dao.DatabasePropertyDao;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.entities.Property;
import net.nilosplace.Forex.ejb.entities.myfxbook.MyFXBookOutlook;
import net.nilosplace.Forex.ejb.entities.myfxbook.MyFXBookSymbol;
import net.nilosplace.Forex.ejb.oanda.dto.OandaHeartBeat;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.util.FileProperty;
import net.nilosplace.Forex.ejb.util.MyFXBookAPI;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

import org.jboss.logging.Logger;

@Stateless
public class DataManager {
	
	@Inject
	DatabasePropertyDao dpdao;
	
	@PersistenceContext(unitName = "primary")
	EntityManager em;
	
	@Inject
	private EventConnection ec;
	
	private Logger log = Logger.getLogger(getClass());

	public List<MyFXBookSymbol> getDataBySymbol(String symbol) {
		Query q = em.createQuery("from MyFXBookSymbol where name = :x").setParameter("x", symbol);
		List<MyFXBookSymbol> list = q.getResultList();
		return list;
	}

	public void saveData(Candle candle) {
		em.persist(candle);
	}

	public void saveDataHeartBeat(OandaHeartBeat heartbeat) {
		em.merge(heartbeat);
	}
	
//	@TransactionAttribute()
//	@AccessTimeout(unit = TimeUnit.MINUTES, value = 1)
//	@Schedule(hour="*", minute="*", second="0", persistent=false)
	private void updateMyFXBook() {
		
		FileProperty config = FileProperty.MYFXPROPERTIESFILE;

		MyFXBookAPI api = new MyFXBookAPI(config.getValue("myfx_username"), config.getValue("myfx_password"), false);
		
		Property prop = dpdao.getProperty("myfx_update");
		if(prop == null || prop.getValue().equals("true")) {
			
			log.info("Updating My FX Book");
			MyFXBookOutlook outlook = api.getCommunityOutlook();
			
			if(outlook != null) {
				for(MyFXBookSymbol s: outlook.getSymbols()) {
					em.persist(s);
				}
				em.persist(outlook.getGeneral());
				em.persist(outlook);
			} else {
				log.warn("Outlook object is null");
			}
			MarketEvent m = new MarketEvent(outlook);
			ec.sendObject(LookupResource.MARKETEVENTTOPIC, m);
			
		} else {
			log.info("Not Updating My FX Book");
		}
	}

}
