package net.nilosplace.Forex.ejb.strategies;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jms.JMSException;
import org.jboss.logging.Logger;

import net.nilosplace.Forex.ejb.common.events.MarketEvent;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.managers.CacheManager;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.strategies.MAC.MovingAverageCrossStrategyMin1;
import net.nilosplace.Forex.ejb.strategies.MAC.MovingAverageCrossStrategyMin15;
import net.nilosplace.Forex.ejb.strategies.MAC.MovingAverageCrossStrategyMin5;
import net.nilosplace.Forex.ejb.strategies.MAC.MovingAverageCrossStrategyTicks;
import net.nilosplace.Forex.ejb.strategies.RSI.RSIHighLowStrategy;
import net.nilosplace.Forex.ejb.strategies.RSI.RSISwingStrategyMin1;
import net.nilosplace.Forex.ejb.strategies.RSI.RSISwingStrategyMin5;
import net.nilosplace.Forex.ejb.strategies.RSI.RSISwingStrategyTicks;
import net.nilosplace.Forex.ejb.strategies.RSI.RSISwingStrategyMin5;

public class StrategyManager {
	
	private Logger log = Logger.getLogger(getClass());
	
	private CacheManager cm;
	private EventConnection ec;
	
	private ArrayList<Class> classes = new ArrayList<Class>();
	private HashMap<String, ArrayList<Strategy>> strats = new HashMap<String, ArrayList<Strategy>>();

	public StrategyManager(CacheManager cm, EventConnection ec) {
		this.cm = cm;
		this.ec = ec;
		
		classes.add(MovingAverageCrossStrategyTicks.class);
		classes.add(MovingAverageCrossStrategyMin1.class);
		classes.add(MovingAverageCrossStrategyMin5.class);
		classes.add(MovingAverageCrossStrategyMin15.class);
		classes.add(RSIHighLowStrategy.class);
		classes.add(RSISwingStrategyTicks.class);
		classes.add(RSISwingStrategyMin1.class);
		classes.add(RSISwingStrategyMin5.class);
		//classes.add(BuySellTestStrategy.class);
	}

	public void processCandle(Candle tick) {
		if(!strats.containsKey(tick.getSource().getTypeName())) {
			ArrayList<Strategy> list = new ArrayList<Strategy>();
			for(Class c: classes) {
				try {
					Strategy s = (Strategy) c.newInstance();
					s.setParams(cm, ec, tick.getSource());
					list.add(s);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			strats.put(tick.getSource().getTypeName(), list);
		}
		for(Strategy s: strats.get(tick.getSource().getTypeName())) {
			s.candleUpdate(tick);
		}
	}

}
