package net.nilosplace.Forex.ejb.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class LimitedQueue<E> extends LinkedList<E> {

	private static final long	serialVersionUID	= -4463860431846133400L;
	private int limit;
	private Semaphore sem = new Semaphore(1);

    public LimitedQueue() {
		
	}
	
    public LimitedQueue(int limit) {
        this.limit = limit;
    }

	@Override
    public boolean add(E o) {
		try {
			sem.acquire();
	        boolean added = super.add(o);
	        while (added && size() > limit) {
	           super.remove();
	        }
	        sem.release();
	        return added;
		} catch (InterruptedException e) {
			sem.release();
		}
		return false;
    }

	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}

	public List<E> getElements() {
		List<E> list = null;
		try {
			sem.acquire();
			list = new ArrayList<E>();
			Iterator<E> i = iterator();
			while(i.hasNext()) {
				list.add(i.next());
			}
			sem.release();
		} catch (InterruptedException e) {
			sem.release();
		}
		return list;
	}
}
