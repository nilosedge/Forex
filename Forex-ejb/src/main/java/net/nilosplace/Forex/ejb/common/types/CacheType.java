package net.nilosplace.Forex.ejb.common.types;

public enum CacheType {

	TICKS("ticks", "S1"),
	MIN1("min1", "M1"),
	MIN5("min5", "M5"),
	MIN15("min15", "M15"),
	MIN30("min30", "M30"),
	HOUR1("hour1", "H1"),
	HOUR4("hour4", "H4"),
	DAY1("day1", "D"),
	WEEK1("week1", "W"),
	MONTH1("month1", "M"),
	;

	private String name;
	private String oandaName;
	
	public String getName() {
		return name;
	}
	public String getOandaName() {
		return oandaName;
	}
	
	private CacheType(String name, String oandaName) {
		this.name = name;
		this.oandaName = oandaName;
	}
	
}
