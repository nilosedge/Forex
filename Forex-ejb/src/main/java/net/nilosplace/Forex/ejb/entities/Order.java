package net.nilosplace.Forex.ejb.entities;

import java.io.Serializable;

import net.nilosplace.Forex.ejb.common.events.FillEvent.FillType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;

import org.jboss.logging.Logger;

public class Order implements Serializable {

	private static final long serialVersionUID = -4331872291270698179L;
	private Logger log = Logger.getLogger(getClass());
	private DataSourceType source;
	private String strategy;
	private String instrument;
	private OrderType orderType;
	private double price;
	private double units;
	private FillType fillType;
	private double cost;
	private double commission;

	public Order(DataSourceType source, String strategy, String instrument, OrderType orderType, double units, FillType fillType) {
		this.source = source;
		this.strategy = strategy;
		this.instrument = instrument;
		this.orderType = orderType;
		this.units = units;
		this.fillType = fillType;
	}
	
	public DataSourceType getSource() {
		return source;
	}
	public void setSource(DataSourceType source) {
		this.source = source;
	}
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public FillType getFillType() {
		return fillType;
	}
	public void setFillType(FillType fillType) {
		this.fillType = fillType;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}

	public enum OrderType {
		MARKET,
		LIMIT,
		;
	}
	
	public void print() {
		print(getClass().getName());
	}

	public void print(String in) {
		log.info(in + ":");
		log.info("Source :" + source);
		log.info("Strategy: " + strategy);
		log.info("Instrument: " + instrument);
		log.info("Price: " + price);
		log.info("Units: " + units);
		log.info("File Type: " + fillType);
	}
}
