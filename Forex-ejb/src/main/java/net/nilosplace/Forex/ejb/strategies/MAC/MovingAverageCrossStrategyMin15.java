package net.nilosplace.Forex.ejb.strategies.MAC;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;

import org.jboss.logging.Logger;

public class MovingAverageCrossStrategyMin15 extends MovingAverageCrossStrategy {

	private int short_window = 50;
	private int long_window = 200;

	private Logger log = Logger.getLogger(getClass());

	@Override
	public void candle(Candle candle) {
		runStrategy(CacheType.MIN15, short_window, long_window, candle);
	}


}
