package net.nilosplace.Forex.ejb.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.nilosplace.Forex.ejb.common.events.FillEvent;
import net.nilosplace.Forex.ejb.common.events.FillEvent.FillType;
import net.nilosplace.Forex.ejb.common.events.SignalEvent;
import net.nilosplace.Forex.ejb.common.events.SignalEvent.SignalType;
import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.entities.Order;
import net.nilosplace.Forex.ejb.entities.Order.OrderType;
import net.nilosplace.Forex.ejb.entities.Position;
import net.nilosplace.Forex.ejb.portfolios.Portfolio;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.util.CacheConnection;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

import org.jboss.logging.Logger;

@Stateless
public class PortfolioManager {

	private Logger log = Logger.getLogger(getClass());
	
	@Inject
	CacheManager cm;
	
	@Inject
	private EventConnection ec;
	
	private Portfolio portfolio;
	
	public void processSignalMessage(SignalEvent s) {
		portfolio = CacheConnection.getPortFolioDataCache(s.getSource());
		
		//log.info("Processing Signal Event: ");
		//s.print();
		Order o = generateOrder(s);
		if(o != null) {
			//log.info("Generated order: ");
			//oe.print();
			ec.sendObject(LookupResource.ORDEREVENTQUEUE, o);
		}
	}

	public void currnetPrice(Candle candle) {
		portfolio = CacheConnection.getPortFolioDataCache(candle.getSource());
		portfolio.getCurrentPrices().put(candle.getInstrument(), candle.getCloseBid());
	}

	public void processFillMessage(FillEvent f) {
		portfolio = CacheConnection.getPortFolioDataCache(f.getSource());

		log.info("Processing Fill Event: ");
		f.print();
		//currentHoldings.updateCurrentPositionQuantity(f);
		//currentHoldings.updateCurrentCost(f);

	}

	public void processOrderFill(Order or) {
		portfolio = CacheConnection.getPortFolioDataCache(or.getSource());
		
		//log.info("Order Filled: ");
		//or.print();
		
		portfolio.printCurrnetValue();
		portfolio.printPositions();
		
		Position p = portfolio.getPosition(or.getStrategy(), or.getInstrument());
		
		if(or.getFillType() == FillType.BUY) {
			portfolio.setCapital(portfolio.getCapital().subtract(BigDecimal.valueOf(or.getUnits() * or.getPrice())));
			p.setUnits(p.getUnits() + or.getUnits());
		} else {
			portfolio.setCapital(portfolio.getCapital().add(BigDecimal.valueOf(or.getUnits() * or.getPrice())));
			p.setUnits(p.getUnits() - or.getUnits());
		}
		
		portfolio.printPositions();
		portfolio.printCurrnetValue();
	}

	// Strategies Performance
	
	// Total return
	// return = ((final portfolio dollar value) − (initial portfolio value))/(final portfolio dollar value) × 100
	
	// Portfolie Performance
	// Equity Curve
	
	public Order generateOrder(SignalEvent s) {
		double currentPrice = cm.getLatestCandle(s.getSource(), CacheType.TICKS, s.getInstrument()).getCloseAsk();
		double qty = portfolio.getCapital().divide(BigDecimal.valueOf(10), RoundingMode.FLOOR).divide(BigDecimal.valueOf(currentPrice), RoundingMode.FLOOR).doubleValue();
		
		//log.info("Instrument: " + s.getInstrument());
		
		double currentQty = portfolio.getPosition(s.getStrategyName(), s.getInstrument()).getUnits();
		
		Order order = null;
		
		if(s.getSignalType() == SignalType.LONG && currentQty == 0) {
			order = new Order(s.getSource(), s.getStrategyName(), s.getInstrument(), OrderType.MARKET, qty, FillType.BUY);
		} else if(s.getSignalType() == SignalType.SHORT && currentQty == 0) {
			order = new Order(s.getSource(), s.getStrategyName(), s.getInstrument(), OrderType.MARKET, qty, FillType.SELL);
		} else if(s.getSignalType() == SignalType.EXIT && currentQty > 0) {
			order = new Order(s.getSource(), s.getStrategyName(), s.getInstrument(), OrderType.MARKET, Math.abs(currentQty), FillType.SELL);
		} else if(s.getSignalType() == SignalType.EXIT && currentQty < 0) {
			order = new Order(s.getSource(), s.getStrategyName(), s.getInstrument(), OrderType.MARKET, Math.abs(currentQty), FillType.BUY);
		}
		return order;
	}

	
}
