package net.nilosplace.Forex.ejb.common.events;

import java.io.Serializable;

public class MarketEvent extends TradingEvent implements Serializable {

	private static final long serialVersionUID = -2358597883028932612L;

	public MarketEvent() {
		super(null);
	}
	
	public MarketEvent(Object event) {
		super(event);
	}

}
