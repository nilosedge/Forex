package net.nilosplace.Forex.ejb.mbean;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import net.nilosplace.Forex.ejb.common.events.FillEvent;
import net.nilosplace.Forex.ejb.managers.PortfolioManager;


@MessageDriven(
		name = "FillEventQueue",
		description = "FillEventQueue MBean",
		activationConfig = {
				@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
				@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/FillEventQueue"),
				@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")
		})
public class FillEventMBean implements MessageListener {

	@Inject
	private PortfolioManager pm;
	
	@Override
	public void onMessage(Message message) {
		try {
			if(((ObjectMessage)message).getObject() instanceof FillEvent) {
				FillEvent f = (FillEvent)(((ObjectMessage)message).getObject());
				pm.processFillMessage(f);
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
		

		
	}

	

}
