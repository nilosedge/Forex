package net.nilosplace.Forex.ejb.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import net.nilosplace.Forex.ejb.entities.Property;

import org.jboss.logging.Logger;

@Stateless
public class DatabasePropertyDao {
	private Logger log = Logger.getLogger(getClass());
	
	@PersistenceContext(unitName="primary")
	EntityManager em;
	
	private String table = "Property";

	public Property getProperty(String name) {
		Query q = em.createQuery("from " + table + " where name = :x").setParameter("x", name);
		try {
			return (Property)q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Property createProperty(String name, String value) {
		log.debug("Creating: " + name + " -> " + value);
		return new Property();
	}

	public List<Property> getAllProperties() {
		Query q = em.createQuery("from " + table);
		try {
			return (List<Property>)q.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Property getProperty(long id) {
		Query q = em.createQuery("from " + table + " where id = :x").setParameter("x", id);
		try {
			return (Property)q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
