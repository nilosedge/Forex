package net.nilosplace.Forex.ejb.util;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.map.type.TypeFactory;
import org.jboss.logging.Logger;


public class ModelMapper<T> extends ObjectMapper implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private boolean debug = false;
	private Logger log = Logger.getLogger(getClass());
	
	{
		init();
	}

	public ModelMapper() { }
	
	public ModelMapper(boolean debug) {
		this.debug = debug;
	}
	
	private void init() {
		//super(new ObjectJsonFactory());
		this.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		this.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
		this.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
//		this.configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//		this.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
		
		SimpleModule module = new SimpleModule("NumberDeserializerModule", new Version(1, 0, 0, null));

		module.addDeserializer(BigDecimal.class, new StripCommasDeserializer());
		module.addDeserializer(Date.class, new ParseMicrosecondsDeserializer());

		registerModule(module);
	}
	
	public Object getObject(String data, Class<T> c) {
		//log.info("Parsing: " + data);
		try {
			if(data == null || data.equals("")) {
				log.warn("No Data to map: " + data);
				return null;
			} else {
				return readValue(data, c);
			}
		} catch (JsonParseException e) {
			log.warn("Unable to parse: " + data);
		} catch (JsonMappingException e) {
			log.warn("Unable to map: " + data);
			e.printStackTrace();
		} catch (IOException e) {
			log.error("IO Exception: " + data);
		} catch (NullPointerException e) {
			log.error("Null Pointer Exception: " + data);
		}
		return null;
	}
	
	public Object getObjects(String data, Object object) {
		//log.info("Parsing: " + data);
		try {
			TypeFactory typeFactory = TypeFactory.defaultInstance();
			readValue(data, typeFactory.constructCollectionType(ArrayList.class, object.getClass()));
			return readValue(data, object.getClass());
		} catch (JsonParseException e) {
			log.warn("Unable to parse: " + data);
		} catch (JsonMappingException e) {
			log.warn("Unable to map: " + data);
		} catch (IOException e) {
			log.error("IO Exception: " + data);
		}
		return null;
	}

	public String getString(Object object) {
		try {
			return writeValueAsString(object);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
