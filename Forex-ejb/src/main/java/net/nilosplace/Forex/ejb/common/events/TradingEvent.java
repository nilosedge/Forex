package net.nilosplace.Forex.ejb.common.events;

import java.io.Serializable;

public class TradingEvent implements Serializable {

	private static final long serialVersionUID = -5382983744265085998L;
	protected Object event;

	public TradingEvent(Object event) {
		this.event = event;
	}
	public Object getEvent() {
		return event;
	}
	public void setEvent(Object event) {
		this.event = event;
	}
}
