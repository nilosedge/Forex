package net.nilosplace.Forex.ejb.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

public class ParseMicrosecondsDeserializer extends JsonDeserializer {

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String time = jp.getText();

		
		String pattern = "^(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2}).(\\d{6})Z$";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(time);  
		m.find();

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)), Integer.parseInt(m.group(5)), Integer.parseInt(m.group(6)));
		cal.setTimeInMillis(cal.getTimeInMillis() + (Integer.parseInt(m.group(7)) / 1000));
		return cal.getTime();
	}
}
