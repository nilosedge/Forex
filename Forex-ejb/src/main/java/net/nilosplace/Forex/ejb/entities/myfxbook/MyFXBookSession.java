package net.nilosplace.Forex.ejb.entities.myfxbook;

public class MyFXBookSession {

	private String error = "";
	private String message = "";
	private String session = "";
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
}
