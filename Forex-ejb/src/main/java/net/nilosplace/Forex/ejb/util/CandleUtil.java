package net.nilosplace.Forex.ejb.util;

import java.util.Calendar;
import java.util.TimeZone;

import net.nilosplace.Forex.ejb.entities.Candle;

public class CandleUtil {

	public static Candle createCandle(String line) {
		String year, mon, day, hour, min, sec, msec;
		Candle c = new Candle();
		String[] array = line.split(",");
		c.setId(0L);
		//t.setInstrument(instrument);
		c.setHighAsk(Double.valueOf(array[2]));
		c.setLowAsk(Double.valueOf(array[2]));
		c.setCloseAsk(Double.valueOf(array[2]));
		c.setOpenAsk(Double.valueOf(array[2]));
		c.setHighBid(Double.valueOf(array[1]));
		c.setLowBid(Double.valueOf(array[1]));
		c.setCloseBid(Double.valueOf(array[1]));
		c.setOpenBid(Double.valueOf(array[1]));
		
		year = array[0].substring(0, 4);
		mon = array[0].substring(4, 6);
		day = array[0].substring(6, 8);
		
		hour = array[0].substring(9, 11);
		min = array[0].substring(11, 13);
		sec = array[0].substring(13, 15);
		
		msec = array[0].substring(15, 18);

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		cal.set(
				Integer.parseInt(year),
				Integer.parseInt(mon) - 1,
				Integer.parseInt(day),
				Integer.parseInt(hour),
				Integer.parseInt(min),
				Integer.parseInt(sec));
		
		cal.setTimeInMillis(cal.getTimeInMillis() + Integer.parseInt(msec));
		c.setTime(cal.getTime());
		
		return c;
	}
}
