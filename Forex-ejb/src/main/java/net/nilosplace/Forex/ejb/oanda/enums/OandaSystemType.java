package net.nilosplace.Forex.ejb.oanda.enums;


public enum OandaSystemType {
	
	PRODUCTION(
		"oanda_production_stream_url",
		"oanda_production_api_url",
		"oanda_production_account_id",
		"oanda_production_access_token",
		"oanda_production_username",
		"oanda_production_password",
		true
	),
	PRACTICE(
		"oanda_practice_stream_url",
		"oanda_practice_api_url",
		"oanda_practice_account_id",
		"oanda_practice_access_token",
		"oanda_practice_username",
		"oanda_practice_password",
		true
	),
	SANBOX(
		"oanda_sandbox_stream_url",
		"oanda_sandbox_api_url",
		"oanda_sandbox_account_id",
		null,
		null,
		null,
		false
	),
	;
	
	private String streamUrl;
	private String apiUrl;
	private String accountId;
	private String accessToken;
	private String username;
	private String password;
	private boolean authRequired;

	private OandaSystemType(String streamUrl, String apiUrl, String accountId, String accessToken, String username, String password, boolean authRequired) {
		this.streamUrl = streamUrl;
		this.apiUrl = apiUrl;
		this.accessToken = accessToken;
		this.username = username;
		this.password = password;
		this.authRequired = authRequired;
		this.accountId = accountId;
	}

	public String getApiUrl() {
		return apiUrl;
	}
	public String getStreamUrl() {
		return streamUrl;
	}
	public boolean isAuthRequired() {
		return authRequired;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getAccountId() {
		return accountId;
	}
}
