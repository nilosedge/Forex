package net.nilosplace.Forex.ejb.strategies;

import java.util.Date;
import java.util.HashMap;

import net.nilosplace.Forex.ejb.common.events.SignalEvent;
import net.nilosplace.Forex.ejb.common.events.SignalEvent.SignalType;
import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;
import net.nilosplace.Forex.ejb.managers.CacheManager;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

public abstract class Strategy {

	protected abstract void candle(Candle candle);
	
	private HashMap<String, StrategyState> bought = new HashMap<String, StrategyState>();
	private CacheManager cm;
	private DataSourceType source;
	private EventConnection ec;
	protected String instrument;
	protected StrategyState state;
	
//	public Strategy(HashMap<String, String> params) {
//		init(params);
//	}
	
	public void candleUpdate(Candle tick) {
		instrument = tick.getInstrument();
		state = getState();
		candle(tick);
	}
	
	public void setParams(CacheManager cm, EventConnection ec, DataSourceType source) {
		this.cm = cm;
		this.ec = ec;
		this.source = source;
		//this.state = getState();
	}
	
	// Sharpe Ratio
	// S = E(Ra − Rb) / Sqrt(􏰃Var(Ra − Rb))
	// SA = Sqrt(N) * S
	// Where N is the number of periods in a year

	
	public enum StrategyState {
		LONG,
		SHORT,
		OUT,
		;
	}

	protected double rsi(CacheType type, int i, boolean s) throws DataUnavailableException, TALibDataException {
		return cm.rsi(source, type, instrument, i, s);
	}
	protected double sma(CacheType type, int i) throws DataUnavailableException, TALibDataException {
		return cm.sma(source, type, instrument, i);
	}
	protected double ema(CacheType type, int i) throws DataUnavailableException, TALibDataException {
		return cm.ema(source, type, instrument, i);
	}
	
	protected boolean emaup(CacheType type, int i) throws DataUnavailableException, TALibDataException {
		return cm.emaup(source, type, instrument, i);
	}
	
	protected int getPriceCount(CacheType type) {
		return cm.getBarSize(source, type, instrument);
	}
	
	protected Candle getLatestBar(CacheType type) {
		return cm.getLatestCandle(source, type, instrument);
	}
	
	protected void setState(StrategyState state) {
		bought.put(instrument, state);
	}
	protected StrategyState getState() {
		if(bought.get(instrument) == null) {
			bought.put(instrument, StrategyState.OUT);
		}
		return bought.get(instrument);
	}
	
	protected void signal(Date time, SignalType type, double strength) {
		SignalEvent se = new SignalEvent(source, getClass().getName(), instrument, time, type, strength);
		ec.sendObject(LookupResource.SIGNALEVENTQUEUE, se);
		if(type == SignalType.LONG) {
			setState(StrategyState.LONG);
		} else if(type == SignalType.EXIT) {
			setState(StrategyState.OUT);
		} else if(type == SignalType.SHORT) {
			setState(StrategyState.SHORT);
		}
	}
}
