package net.nilosplace.Forex.ejb.webservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import net.nilosplace.Forex.ejb.common.events.DataEvent;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.entities.Tick;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.util.ModelMapper;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.jboss.logging.Logger;

@Path("/backtest")
@RequestScoped
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class BackTest {
	private Logger log = Logger.getLogger(getClass());
	ModelMapper m = new ModelMapper();
	
	@Inject
	private EventConnection ec;
	
	@GET
	@Path("/loaddata/{instrument}")
	public Response loadFile(@PathParam("instrument") String instrument) {

    	String fileName = instrument + ".csv.bz2";
    	
		log.info("Loading File: " + fileName);
		File f = new File("/Volumes/HD320GX/files/" + fileName);
		
		String year, mon, day, hour, min, sec, msec, time, line;
		int count = 0;
		try {
			
			FileInputStream fis = new FileInputStream(f);
			BZip2CompressorInputStream bzcis = new BZip2CompressorInputStream(fis);
			InputStreamReader isr = new InputStreamReader(bzcis);
			BufferedReader br = new BufferedReader(isr);
			
			//BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			
			ArrayList<Tick> ticks = new ArrayList<Tick>();
			
			while ((line = br.readLine()) != null) {

				Tick t = new Tick();
				String[] array = line.split(",");
				
				
				
				t.setInstrument(instrument);
				t.setAsk(Double.valueOf(array[1]));
				t.setBid(Double.valueOf(array[2]));
				
				
				year = array[0].substring(0, 4);
				mon = array[0].substring(4, 6);
				day = array[0].substring(6, 8);
				
				hour = array[0].substring(9, 11);
				min = array[0].substring(11, 13);
				sec = array[0].substring(13, 15);
				
				msec = array[0].substring(15, 18);
				


				Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
				cal.set(
						Integer.parseInt(year),
						Integer.parseInt(mon) - 1,
						Integer.parseInt(day),
						Integer.parseInt(hour),
						Integer.parseInt(min),
						Integer.parseInt(sec));
				
				cal.setTimeInMillis(cal.getTimeInMillis() + Integer.parseInt(msec));
				t.setTime(cal.getTime());
				
				//time = year + "-" + mon + "-" + day + "T" + hour + ":" + min + ":" + sec + "." + msec + "000Z";
				
				//String json = "{\"time\": \"" + time + "\", \"instrument\":\"" + instrument + "\", \"bid\":\"" + array[2] + "\", \"ask\":\"" + array[1] + "\"}";
				//log.info("Line: " + line);
				//log.info("Tick: " + t);
				//Tick t = (Tick) m.getObject(json, Tick.class);
				
				if(count % 10000 == 0 && count > 0) {
					ec.sendObject(LookupResource.DATAHANDLERQUEUE, ticks);

					ticks.clear();
				} else {
					ticks.add(t);
				}
				count++;
			}
			ec.sendObject(LookupResource.DATAHANDLERQUEUE, ticks);
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return Response.ok("{\"message\":\"" + count + " tick records loaded\"}").build();
	}
	
	@POST
	@Path("/addcandle")
	public Response sendCandle(Candle c) {
		log.info("Recieved Candle Sending to Queue: ");
		DataEvent d = new DataEvent(c);
		ec.sendObject(LookupResource.DATAHANDLERQUEUE, d);
		return Response.ok("\"Candle loaded\"").build();
	}
	
	@POST
	@Path("/addcandles")
	public Response sendCandles(ArrayList<Candle> candles) {
		log.info("Recieved Candles Sending to Queue: ");
		DataEvent d = new DataEvent(candles);
		ec.sendObject(LookupResource.DATAHANDLERQUEUE, d);
		return Response.ok("\"Candles loaded\"").build();
	}
	
}
