package net.nilosplace.Forex.ejb.oanda.dto;

import java.io.Serializable;
import java.util.List;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.entities.Candle;

import org.codehaus.jackson.annotate.JsonIgnore;

public class OandaCandles implements Serializable {

	private static final long	serialVersionUID	= -208317609831652973L;
	private String instrument;
	private String granularity;
	private List<Candle> candles;
	private CacheType timeScale;
	private DataSourceType source;
	
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public String getGranularity() {
		return granularity;
	}
	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}
	public List<Candle> getCandles() {
		return candles;
	}
	public void setCandles(List<Candle> candles) {
		this.candles = candles;
	}
	public CacheType getTimeScale() {
		return timeScale;
	}
	public void setTimeScale(CacheType timeScale) {
		this.timeScale = timeScale;
	}
	public DataSourceType getSource() {
		return source;
	}
	public void setSource(DataSourceType source) {
		this.source = source;
	}
	
	@JsonIgnore
	public void setData(DataSourceType source, CacheType timeScale) {
		for(Candle c: candles) {
			c.setInstrument(instrument);
		}
		this.source = source;
		for(Candle c: candles) {
			c.setSource(source);
		}
		this.timeScale = timeScale;
		for(Candle c: candles) {
			c.setTimeScale(timeScale);
		}
	}
}
