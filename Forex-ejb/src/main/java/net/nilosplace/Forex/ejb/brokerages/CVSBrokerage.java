package net.nilosplace.Forex.ejb.brokerages;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.entities.Order;

public class CVSBrokerage extends Brokerage {

	@Override
	public Order executeOrder(Order o) {

		Candle latest = cm.getLatestCandle(o.getSource(), CacheType.TICKS, o.getInstrument());

		o.setPrice(latest.getCloseBid());
		
		return o;
	}
}
