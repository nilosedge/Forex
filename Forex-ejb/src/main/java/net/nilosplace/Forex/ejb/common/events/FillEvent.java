package net.nilosplace.Forex.ejb.common.events;

import java.io.Serializable;
import java.util.Date;

import net.nilosplace.Forex.ejb.common.events.FillEvent.FillType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.entities.Order;
import net.nilosplace.Forex.ejb.oanda.dto.OandaSideType;
import net.nilosplace.Forex.ejb.oanda.dto.OandaTransaction;

import org.jboss.logging.Logger;

public class FillEvent implements Serializable {

	private Logger log = Logger.getLogger(getClass());
	
	private static final long	serialVersionUID	= -7079889054987301264L;
	private DataSourceType source;
	private Date fillDate;
	private String instrument;
	private double units;
	private FillType fillType;
	private double cost;
	private double commission;
	
	public FillEvent(DataSourceType source, Date fillDate, String instrument, double units, FillType fillType, double cost, double commission) {
		this.source = source;
		this.fillDate = fillDate;
		this.instrument = instrument;
		this.units = units;
		this.fillType = fillType;
		this.cost = cost;
		this.commission = commission;
	}

	public FillEvent(Order o) {
		this.source = o.getSource();
		this.fillDate = new Date();
		this.instrument = o.getInstrument();
		this.units = o.getUnits();
		this.fillType = o.getFillType();
		this.cost = 0;
		this.commission = 0;
	}

	public FillEvent(OandaTransaction t) {
		this.fillDate = t.getTime();
		this.instrument = t.getInstrument();
		this.units = t.getUnits();
		this.fillType = FillType.valueOf(t.getSide().name().toUpperCase());
		this.cost = t.getPrice() * t.getUnits();
	}

	public DataSourceType getSource() {
		return source;
	}
	public void setSource(DataSourceType source) {
		this.source = source;
	}
	public Date getFillDate() {
		return fillDate;
	}
	public void setFillDate(Date fillDate) {
		this.fillDate = fillDate;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	public double getUnits() {
		return units;
	}
	public void setUnits(double units) {
		this.units = units;
	}
	public FillType getFillType() {
		return fillType;
	}
	public void setFillType(FillType fillType) {
		this.fillType = fillType;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}

	public enum FillType {
		BUY,
		SELL,
		;
	}

	public void print() {
		//log.info("DataSourceType: " + source);
		//log.info("Date: " + fillDate);
		log.info("Instrument: " + instrument);
		log.info("Units: " + units);
		log.info("Fill Type: " + fillType);
		log.info("Cost: " + cost);
		//log.info("Commission: " + commission);
	}
}
