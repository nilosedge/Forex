package net.nilosplace.Forex.ejb.mbean;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import net.nilosplace.Forex.ejb.common.events.DataEvent;
import net.nilosplace.Forex.ejb.common.events.MarketEvent;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.managers.CacheManager;
import net.nilosplace.Forex.ejb.managers.DataManager;
import net.nilosplace.Forex.ejb.managers.PortfolioManager;
import net.nilosplace.Forex.ejb.oanda.dto.OandaCandles;
import net.nilosplace.Forex.ejb.oanda.dto.OandaHeartBeat;
import net.nilosplace.Forex.ejb.singletons.EventConnection;
import net.nilosplace.Forex.ejb.strategies.StrategyManager;
import net.nilosplace.Forex.ejb.util.ServerResources.LookupResource;
import net.nilosplace.Forex.ejb.webservice.WSJMSMessage;

import org.jboss.logging.Logger;

@MessageDriven(
		name = "DataEventQueue",
		description = "DataEventQueue MBean",
		activationConfig = {
				@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
				@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/DataEventQueue"),
				@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")
		})
public class DataEventMBean implements MessageListener {

	@Inject
	@WSJMSMessage
	Event<Message> jmsEvent;
	
	@Inject
	private DataManager dm;
	
	@Inject
	private CacheManager cm;
	
	@Inject
	private PortfolioManager pm;
	
	@Inject
	private EventConnection ec;

	private Logger log = Logger.getLogger(getClass());
	private StrategyManager sm;
	
	@PostConstruct
	public void init() {
		sm = new StrategyManager(cm, ec);
	}

	public void onMessage(Message message) {
		log.info("Recieved Data Event");
		try {
			if(((ObjectMessage)message).getObject() instanceof DataEvent) {
				DataEvent d = (DataEvent)(((ObjectMessage)message).getObject());
				
				if(d.getEvent() instanceof ArrayList<?>) {
					log.info("Starting Cache Update");
					for(Object o: (ArrayList<?>)d.getEvent()) {
						Candle candle = (Candle)o;
						cm.updateCache(candle);
						pm.currnetPrice(candle);
						sm.processCandle(candle);
					}
					log.info("Finished Cache Update");
					jmsEvent.fire(message);

				} else if(d.getEvent() instanceof Candle) { 
					Candle candle = (Candle)d.getEvent();
					//log.info("Incoming tick: " + tick.getInstrument());
					//dm.saveData(candle);
					cm.updateCache(candle);
					pm.currnetPrice(candle);
					sm.processCandle(candle);
					jmsEvent.fire(message);
				} else if(d.getEvent() instanceof OandaHeartBeat) {
					OandaHeartBeat heartbeat = (OandaHeartBeat)d.getEvent();
					heartbeat.setId(1L);
					dm.saveDataHeartBeat(heartbeat);
				} else if(d.getEvent() instanceof OandaCandles) {
					OandaCandles candles = (OandaCandles)d.getEvent();
					cm.updateCache(candles);
				} else {
					log.error("Unknown object type to data manager: " + d.getEvent().getClass());
				}
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
