package net.nilosplace.Forex.ejb.entities.myfxbook;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MyFXBookGeneralInfo implements Serializable {

	private static final long serialVersionUID = -8569289100902171311L;
	private Long id;
    private int demoAccountsPercentage;
	private int realAccountsPercentage;
	private int profitablePercentage;
	private int nonProfitablePercentage;
	private BigDecimal fundsWon;
	private BigDecimal fundsLost;
	private BigDecimal averageDeposit;
	private BigDecimal averageAccountProfit;
	private BigDecimal averageAccountLoss;
	private BigDecimal totalFunds;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getDemoAccountsPercentage() {
		return demoAccountsPercentage;
	}
	public void setDemoAccountsPercentage(int demoAccountsPercentage) {
		this.demoAccountsPercentage = demoAccountsPercentage;
	}
	public int getRealAccountsPercentage() {
		return realAccountsPercentage;
	}
	public void setRealAccountsPercentage(int realAccountsPercentage) {
		this.realAccountsPercentage = realAccountsPercentage;
	}
	public int getProfitablePercentage() {
		return profitablePercentage;
	}
	public void setProfitablePercentage(int profitablePercentage) {
		this.profitablePercentage = profitablePercentage;
	}
	public int getNonProfitablePercentage() {
		return nonProfitablePercentage;
	}
	public void setNonProfitablePercentage(int nonProfitablePercentage) {
		this.nonProfitablePercentage = nonProfitablePercentage;
	}
	public BigDecimal getFundsWon() {
		return fundsWon;
	}
	public void setFundsWon(BigDecimal fundsWon) {
		this.fundsWon = fundsWon;
	}
	public BigDecimal getFundsLost() {
		return fundsLost;
	}
	public void setFundsLost(BigDecimal fundsLost) {
		this.fundsLost = fundsLost;
	}
	public BigDecimal getAverageDeposit() {
		return averageDeposit;
	}
	public void setAverageDeposit(BigDecimal averageDeposit) {
		this.averageDeposit = averageDeposit;
	}
	public BigDecimal getAverageAccountProfit() {
		return averageAccountProfit;
	}
	public void setAverageAccountProfit(BigDecimal averageAccountProfit) {
		this.averageAccountProfit = averageAccountProfit;
	}
	public BigDecimal getAverageAccountLoss() {
		return averageAccountLoss;
	}
	public void setAverageAccountLoss(BigDecimal averageAccountLoss) {
		this.averageAccountLoss = averageAccountLoss;
	}
	public BigDecimal getTotalFunds() {
		return totalFunds;
	}
	public void setTotalFunds(BigDecimal totalFunds) {
		this.totalFunds = totalFunds;
	}
	
	
}
