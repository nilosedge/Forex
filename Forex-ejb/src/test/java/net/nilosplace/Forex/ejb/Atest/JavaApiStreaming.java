package net.nilosplace.Forex.ejb.Atest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.nilosplace.Forex.ejb.entities.Tick;
import net.nilosplace.Forex.ejb.oanda.dto.OandaHeartBeat;
import net.nilosplace.Forex.ejb.util.ModelMapper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.BasicConfigurator;
import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JavaApiStreaming {
	
    public static void main (String[]args) throws IOException {
    	
    	Logger log = Logger.getLogger(JavaApiStreaming.class);
    	BasicConfigurator.configure();
    	ModelMapper model = new ModelMapper();
    	
    	DefaultHttpClient httpClient = new DefaultHttpClient();
    	//https://stream-fxtrade.oanda.com/v1/prices?accountId=738891&instruments=EUR_USD%2CUSD_JPY%2CGBP_USD%2CUSD_CHF%2CEUR_GBP%2CEUR_JPY%2CEUR_CHF%2CAUD_USD%2CUSD_CAD%2CNZD_USD%2CAUD_JPY
    	HttpUriRequest httpGet = new HttpGet("http://stream-sandbox.oanda.com/v1/prices?instruments=EUR_USD%2CAUD_JPY");
		//httpGet.setHeader(new BasicHeader("Authorization", "Bearer 'DO NOT PUT TOKEN HERE AND COMMIT IT!!!!'"));

    	log.info("Executing request: " + httpGet.getRequestLine());
		
		HttpResponse resp = httpClient.execute(httpGet);
		HttpEntity entity = resp.getEntity();

		if (resp.getStatusLine().getStatusCode() == 200 && entity != null) {
			InputStream stream = entity.getContent();
			String line;
			BufferedReader br = new BufferedReader(new InputStreamReader(stream));
			while ((line = br.readLine()) != null) {

				log.info("Line: " + line);
				Object obj = JSONValue.parse(line);
				JSONObject json = (JSONObject) obj;

				// unwrap if necessary
				if (json.containsKey("tick")) {
					Tick t = (Tick)model.getObject(json.get("tick").toString(), Tick.class);
					log.info(t.getInstrument());
					log.info(json.get("tick").toString());
				}
				if(json.containsKey("heartbeat")) {
					OandaHeartBeat hb = (OandaHeartBeat)model.getObject(json.get("heartbeat").toString(), OandaHeartBeat.class);
					log.info(hb);
				}
			}
			httpClient.getConnectionManager().shutdown();
		} else {
			// print error message
			String responseString = EntityUtils.toString(entity, "UTF-8");
			log.info(responseString);
		}
    }
}