package net.nilosplace.Forex.ejb.Btest;

import java.util.ArrayList;

import net.nilosplace.Forex.ejb.entities.Candle;
import si.mazi.rescu.RestProxyFactory;

public class LocalRestAPI implements LocalRestAPIInterface {

	private LocalRestAPIInterface localRestApi;
	
	public LocalRestAPI() {
		localRestApi = RestProxyFactory.createProxy(LocalRestAPIInterface.class, "http://localhost:8080/rest");
	}

	public String sendCandle(Candle candle) {
		return localRestApi.sendCandle(candle);
	}
	
	public String sendCandles(ArrayList<Candle> candles) {
		return localRestApi.sendCandles(candles);
	}

}
