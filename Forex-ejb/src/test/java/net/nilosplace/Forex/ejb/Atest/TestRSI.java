package net.nilosplace.Forex.ejb.Atest;

import java.util.ArrayList;
import java.util.List;

import net.nilosplace.Forex.ejb.common.CoreHelper;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;

import org.apache.log4j.BasicConfigurator;
import org.jboss.logging.Logger;

public class TestRSI {

	public static void main(String[] args) throws DataUnavailableException, TALibDataException {
		//Logger log = Logger.getLogger(TestRSI.class);
		BasicConfigurator.configure();

		ArrayList<Double> list = new ArrayList<Double>();

		list.add(100.783);
		list.add(100.846);
		list.add(100.984);
		list.add(100.975);
		list.add(100.919);
		list.add(100.888);
		list.add(100.954);
		list.add(100.977);
		list.add(101.031);
		list.add(101.034);
		list.add(101.028);
		list.add(101.019);
		list.add(100.985);
		list.add(100.935);
		list.add(100.940);
		list.add(100.976);
		list.add(100.958);
		
		list.add(100.949);
		list.add(100.964);
		list.add(100.972);
		list.add(100.973);
		list.add(100.974);
		list.add(100.996);
		
		
		
		
		
		//11:25:09,584 INFO  [stdout] (Thread-5 (HornetQ-client-global-threads-950347416)) Window: 14 Prices: 7.455 7.455 7.455 7.4552 7.4552 7.455 7.4549 7.4549 7.455 7.455 7.455 7.4549 7.4549 7.4551 7.455 
		//11:25:09,585 INFO  [stdout] (Thread-5 (HornetQ-client-global-threads-950347416)) Window: 14 Prices: 7.4549 7.455 7.455 7.455 7.455 7.4552 7.4552 7.455 7.4549 7.455 7.455 7.455 7.4549 7.4551 7.455 

		//		list.add(45.15);
		//		list.add(46.26);
		//		list.add(46.50);
		//		list.add(46.23);
		//		list.add(46.08);
		//		list.add(46.03);
		//		list.add(46.83);
		//		list.add(47.69);
		//		list.add(47.54);
		//		list.add(49.25);
		//		list.add(49.23);
		//		list.add(48.20);
		//		list.add(47.57);
		//		list.add(47.61);
		//		list.add(48.08);
		//		list.add(47.21);
		//		list.add(46.76);
		//		list.add(46.68);
		//		list.add(46.21);
		//		list.add(47.47);
		//		list.add(47.98);
		//		list.add(47.13);
		//		list.add(46.58);
		//		list.add(46.03);

		//int size = 17;

		//check(list, 14);
		//check(list.subList(1, 16), 14);
		//check(list.subList(2, 17), 14);
		//check(list.subList(3, 18), 14);
		//check(list.subList(4, 19), 14);
		//check(list.subList(5, 20), 14);
		//check(list.subList(6, 21), 14);
		//check(list.subList(7, 22), 14);
		//check(list.subList(8, 23), 14);
		//check(list.subList(9, 24), 14);


	}

	public static void check(List<Double> list) throws DataUnavailableException, TALibDataException {
		Logger log = Logger.getLogger(TestRSI.class);
		double[] prices = new double[list.size()];
		for(int i = 0; i < list.size(); i++) {
			//System.out.println("I: " + i);
			//System.out.println("X: " + (i - (list.size() - size)));
			prices[i] = list.get(i);
		}

		double rsi = CoreHelper.rsi(prices, 14);
		log.info("RSI: " + rsi);
	}

}
