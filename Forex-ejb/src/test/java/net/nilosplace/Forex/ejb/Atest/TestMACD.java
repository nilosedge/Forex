package net.nilosplace.Forex.ejb.Atest;

import net.nilosplace.Forex.ejb.common.CoreHelper;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;

import org.apache.log4j.BasicConfigurator;
import org.jboss.logging.Logger;

public class TestMACD {

	public static void main(String[] args) throws DataUnavailableException, TALibDataException {
		Logger log = Logger.getLogger(TestMACD.class);
    	BasicConfigurator.configure();
    	
		int size = 500;
		double[] prices = new double[size];
		
		for(int i = 0; i < size; i++) {
			prices[i] = Math.random() * i;
		}
		
		double macd[] = CoreHelper.macd(prices, 12, 26, 9);
		log.info("MACD: " + macd[0] + " Sig: " + macd[1] + " Hist: " + macd[2]);
	}

}
