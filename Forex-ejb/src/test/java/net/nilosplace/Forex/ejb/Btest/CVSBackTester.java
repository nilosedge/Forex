package net.nilosplace.Forex.ejb.Btest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import net.nilosplace.Forex.ejb.common.types.CacheType;
import net.nilosplace.Forex.ejb.common.types.DataSourceType;
import net.nilosplace.Forex.ejb.entities.Candle;
import net.nilosplace.Forex.ejb.util.CandleUtil;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.jboss.logging.Logger;

public class CVSBackTester extends JFrame {

	private static final long serialVersionUID = 2744026689086735554L;
	private static final Logger log = Logger.getLogger(CVSBackTester.class);
	
	private LocalRestAPI api = new LocalRestAPI();
	private BufferedReader br;
	private JPanel p = new JPanel();
	private JButton pm = new JButton("Press Me");
	private JButton lf = new JButton("Load File");
	private JComboBox<Integer> amountBox = new JComboBox<Integer>();
	private JComboBox<String> filesBox = new JComboBox<String>();
	private String selectedInstrument;
	
	public CVSBackTester() {
		setSize(350, 100);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setupComponants();
		setupFiles();
		setVisible(true);
	}
	
	private void setupFiles() {
		ArrayList<String> dirs = new ArrayList<String>();
		dirs.add("/Users/oblod/Desktop/FX/");
		dirs.add("/Volumes/HD320GX/files/");
		//dirs.add("/Users/oblodgett/Desktop/");
		
		for(String dir: dirs) {
			File d = new File(dir);
			if(d.list() != null) {
				ArrayList<String> filenames = new ArrayList<String>(Arrays.asList(d.list()));
				for(String filename: filenames) {
					filesBox.addItem(dir + filename);
				}
			}
		}
	}

	private void setupComponants() {

		pm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(br == null) {
					openFile(filesBox.getSelectedItem().toString());
				}
				pm.setEnabled(false);
				loadCandles();
				pm.setEnabled(true);
			}
		});
		
		lf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openFile(filesBox.getSelectedItem().toString());
			}
		});
		
		add(p);
		p.add(pm);
		
		for(int i = 1; i <= 1000000; i *= 10) {
			amountBox.addItem(new Integer(i));
		}
		
		p.add(amountBox);

		p.add(lf);
		
		p.add(filesBox);
	}

	private void openFile(String filename) {
		String[] array = filename.split("/");
		String[] array2 = array[array.length - 1].split("\\.");
		selectedInstrument = array2[0];
		log.info("Loading File: " + filename);
		try {
			if(br != null) br.close();
			File f = new File(filename);
			FileInputStream fis = new FileInputStream(f);
			BZip2CompressorInputStream bzcis = new BZip2CompressorInputStream(fis);
			InputStreamReader isr = new InputStreamReader(bzcis);
			br = new BufferedReader(isr);
			log.info("File Loaded: " + filename);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private void loadCandles() {
		int counter = 0;
		Date start = new Date();
		Date end = new Date();
		
		String temp = null;
		start = new Date();
		try {
			System.out.println("Reading: " + amountBox.getSelectedItem() + " lines");
			int count = 0;
			
			ArrayList<Candle> candles = new ArrayList<>();
			while(count < (int)amountBox.getSelectedItem() && ((temp = br.readLine()) != null)) {
				Candle c = CandleUtil.createCandle(temp);
				c.setId((long)(counter));
				c.setSource(DataSourceType.INPUTCVSFILE);
				c.setTimeScale(CacheType.TICKS);
				c.setInstrument(selectedInstrument);
				candles.add(c);
				count++;
				counter++;
			}
			api.sendCandles(candles);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		end = new Date();
		double lps = 0;
		System.out.println((end.getTime() - start.getTime()));
		if((end.getTime() - start.getTime()) != 0) {
			lps = ((int)amountBox.getSelectedItem() / (double)(end.getTime() - start.getTime())) * 1000;
		}
		System.out.println(lps + " l/s");
		if(temp == null) System.exit(0);
	}
	
}
