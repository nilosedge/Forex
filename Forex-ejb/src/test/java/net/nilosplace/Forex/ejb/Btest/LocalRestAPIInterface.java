package net.nilosplace.Forex.ejb.Btest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.nilosplace.Forex.ejb.entities.Candle;

@Path("backtest")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface LocalRestAPIInterface {

	@POST
	@Path("addcandle")
	public String sendCandle(Candle candle);
	
	@POST
	@Path("addcandles")
	public String sendCandles(ArrayList<Candle> candles);

}
