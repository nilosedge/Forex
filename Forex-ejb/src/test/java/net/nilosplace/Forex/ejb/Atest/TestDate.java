package net.nilosplace.Forex.ejb.Atest;

import java.util.Date;

import org.apache.log4j.BasicConfigurator;
import org.jboss.logging.Logger;

import net.nilosplace.Forex.ejb.entities.Candle;

public class TestDate {

	public static void main(String[] args) {
		Logger log = Logger.getLogger(TestDate.class);
    	BasicConfigurator.configure();
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd--HH-mm-ss");
//		Date today = Calendar.getInstance().getTime();
//		String reportDate = df.format(today);
//		log.info("Report Date: " + reportDate);
//
//		BigDecimal low = new BigDecimal("2.0001");
//		BigDecimal tick = new BigDecimal("2.00001");
//		
//		log.info(low.compareTo(tick) > 0);
		
		Candle c = new Candle();
		c.setTime(new Date());
		
		log.info("Min1:   " + c.toMin1Bar()   + " - " + c.toMin1Bar().getTime());
		log.info("Min5:   " + c.toMin5Bar()   + " - " + c.toMin5Bar().getTime());
		log.info("Min15:  " + c.toMin15Bar()  + " - " + c.toMin15Bar().getTime());
		log.info("Min30:  " + c.toMin30Bar()  + " - " + c.toMin30Bar().getTime());
		log.info("Hour1:  " + c.toHour1Bar()  + " - " + c.toHour1Bar().getTime());
		log.info("Hour4:  " + c.toHour4Bar()  + " - " + c.toHour4Bar().getTime());
		log.info("Day1:   " + c.toDay1Bar()   + " - " + c.toDay1Bar().getTime());
		log.info("Week1:  " + c.toWeek1Bar()  + " - " + c.toWeek1Bar().getTime());
		log.info("Month1: " + c.toMonth1Bar() + " - " + c.toMonth1Bar().getTime());
	}

}
