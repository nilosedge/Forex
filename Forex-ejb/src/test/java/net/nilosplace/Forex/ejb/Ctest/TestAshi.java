package net.nilosplace.Forex.ejb.Ctest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;

public class TestAshi {

	public static void main(String[] args) throws Exception {
		File f = new File("EURJPY1440.csv");

		File fo = new File("EURJPY1440-2.csv");
		BufferedWriter out = new BufferedWriter(new FileWriter(fo));
		BufferedReader buf = new BufferedReader(new FileReader(f));
		
		double prevOpen = 0;
		//double prevHigh = 0;
		///double prevLow = 0;
		double prevClose = 0;
		
		double curOpen = 0;
		double curHigh = 0;
		double curLow = 0;
		double curClose = 0;
		
		double barOpen = 0;
		double barHigh = 0;
		double barLow = 0;
		double barClose = 0;
		DecimalFormat df = new DecimalFormat("#.000");
		
		
		
		//Date,Time,Open,High,Low,Close,Volumn
		String s;
		while((s = buf.readLine()) != null) {
			String[] array = s.split(",");
			curOpen = Double.parseDouble(array[2]);
			curHigh = Double.parseDouble(array[3]);
			curLow = Double.parseDouble(array[4]);
			curClose = Double.parseDouble(array[5]);
			
//			System.out.println("Open: " + curOpen);
//			System.out.println("High: " + curHigh);
//			System.out.println("Low: " + curLow);
//			System.out.println("Close: " + curClose);
//			System.out.println();
			
			barClose = (curOpen + curHigh + curLow + curClose)/4;
			barOpen = (prevOpen + prevClose) / 2;
			barHigh = Math.max(Math.max(curHigh, barOpen), barClose);
			barLow = Math.min(Math.min(curLow, barOpen), barClose);
			
//			System.out.println("BarOpen: " + barOpen);
//			System.out.println("BarHigh: " + barHigh);
//			System.out.println("BarLow: " + barLow);
//			System.out.println("BarClose: " + barClose);
//			System.out.println();
			
			
			
			
			
			
			
			out.write(array[0] + "," + array[1] + "," + df.format(barOpen) + "," + df.format(barHigh) + "," + df.format(barLow) + "," + df.format(barClose) + "," + array[6] + "\n");
			
			prevClose = curClose;
			//prevHigh = curHigh;
			//prevLow = curLow;
			prevOpen = curOpen;

		}
		
		out.close();
		buf.close();
		
	}

}
