package net.nilosplace.Forex.ejb.Atest;

import net.nilosplace.Forex.ejb.common.CoreHelper;
import net.nilosplace.Forex.ejb.exceptions.DataUnavailableException;
import net.nilosplace.Forex.ejb.exceptions.TALibDataException;

import org.apache.log4j.BasicConfigurator;
import org.jboss.logging.Logger;

public class TestSMA {

	public static void main(String[] args) throws DataUnavailableException, TALibDataException {
		Logger log = Logger.getLogger(TestSMA.class);
    	BasicConfigurator.configure();
    	
		int size = 500;
		double[] prices = new double[size];
		
		for(int i = 0; i < size; i++) {
			prices[i] = i;
		}
		
		double sma = CoreHelper.sma(prices, 20);
		log.info("SMA: " + sma);
	}

}
